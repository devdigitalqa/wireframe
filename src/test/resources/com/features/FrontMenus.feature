Feature: Front menus scenarios

  @FrontMenus
  Scenario: About front menu scenario
    Given Login as Admin and Go to "Front Menus" Module
    When Changes for "About" menu as "Active with Banner Image"
    And Click on "Save" button in "Front Menus"
    Then Open Front site and Go to "Home" Module
    And Verify at front side for "About with Banner Image"
    Given Login as Admin and Go to "Front Menus" Module
    When Changes for "About" menu as "Active without Banner Image"
    And Click on "Save" button in "Front Menus"
    Then Open Front site and Go to "Home" Module
    And Verify at front side for "About without Banner Image"
    Given Login as Admin and Go to "Front Menus" Module
    When Changes for "About" menu as "Inactive"
    And Click on "Save" button in "Front Menus"
    Then Open Front site and Go to "Home" Module
    And Verify at front side for "About"

  @FrontMenus
  Scenario: About front menu Banner Image Alt text verification scenario
    Given Login as Admin and Go to "Front Menus" Module
    When Changes for "About" menu as "Active with Banner Image"
    And "Set" Banner image alt text
    And Click on "Save" button in "Front Menus"
    Then Open Front site and Go to "Home" Module
    And Verify at front side for "About with Banner Image"
    When Login as Admin and Go to "Front Menus" Module
    When Changes for "About" menu as "Active with Banner Image"
    And "Do not set" Banner image alt text
    And Click on "Save" button in "Front Menus"
    Then Open Front site and Go to "Home" Module
    And Verify at front side for "About with Banner Image"

  @FrontMenus
  Scenario: Contact front menu scenario
    Given Login as Admin and Go to "Front Menus" Module
    When Changes for "Contact" menu as "Active with Banner Image"
    And Click on "Save" button in "Front Menus"
    Then Open Front site and Go to "Home" Module
    And Verify at front side for "Contact with Banner Image"
    Given Login as Admin and Go to "Front Menus" Module
    When Changes for "Contact" menu as "Active without Banner Image"
    And Click on "Save" button in "Front Menus"
    Then Open Front site and Go to "Home" Module
    And Verify at front side for "Contact without Banner Image"
    Given Login as Admin and Go to "Front Menus" Module
    When Changes for "Contact" menu as "Inactive"
    And Click on "Save" button in "Front Menus"
    Then Open Front site and Go to "Home" Module
    And Verify at front side for "Contact"

  @FrontMenus
  Scenario: Contact front menu Banner Image Alt text verification scenario
    Given Login as Admin and Go to "Front Menus" Module
    When Changes for "Contact" menu as "Active with Banner Image"
    And "Set" Banner image alt text
    And Click on "Save" button in "Front Menus"
    Then Open Front site and Go to "Home" Module
    And Verify at front side for "Contact with Banner Image"
    When Login as Admin and Go to "Front Menus" Module
    When Changes for "Contact" menu as "Active with Banner Image"
    And "Do not set" Banner image alt text
    And Click on "Save" button in "Front Menus"
    Then Open Front site and Go to "Home" Module
    And Verify at front side for "Contact with Banner Image"

  @FrontMenus
  Scenario: Blog front menu scenario
    Given Login as Admin and Go to "Front Menus" Module
    When Changes for "Blog" menu as "Active with Banner Image"
    And Click on "Save" button in "Front Menus"
    Then Open Front site and Go to "Home" Module
    And Verify at front side for "Blog with Banner Image"
    When Login as Admin and Go to "Front Menus" Module
    When Changes for "Blog" menu as "Active without Banner Image"
    And Click on "Save" button in "Front Menus"
    Then Open Front site and Go to "Home" Module
    And Verify at front side for "Blog without Banner Image"
    When Login as Admin and Go to "Front Menus" Module
    When Changes for "Blog" menu as "Inactive"
    And Click on "Save" button in "Front Menus"
    Then Open Front site and Go to "Home" Module
    And Verify at front side for "Blog"

  @FrontMenus
  Scenario: Blog front menu Banner Image Alt text verification scenario
    Given Login as Admin and Go to "Front Menus" Module
    When Changes for "Blog" menu as "Active with Banner Image"
    And "Set" Banner image alt text
    And Click on "Save" button in "Front Menus"
    Then Open Front site and Go to "Home" Module
    And Verify at front side for "Blog with Banner Image"
    When Login as Admin and Go to "Front Menus" Module
    When Changes for "Blog" menu as "Active with Banner Image"
    And "Do not set" Banner image alt text
    And Click on "Save" button in "Front Menus"
    Then Open Front site and Go to "Home" Module
    And Verify at front side for "Blog with Banner Image"

  @FrontMenus
  Scenario: Gallery front menu scenario
    Given Login as Admin and Go to "Front Menus" Module
    When Changes for "Gallery" menu as "Active with Banner Image"
    And Click on "Save" button in "Front Menus"
    Then Open Front site and Go to "Home" Module
    And Verify at front side for "Gallery with Banner Image"
    When Login as Admin and Go to "Front Menus" Module
    When Changes for "Gallery" menu as "Active without Banner Image"
    And Click on "Save" button in "Front Menus"
    Then Open Front site and Go to "Home" Module
    And Verify at front side for "Gallery without Banner Image"
    When Login as Admin and Go to "Front Menus" Module
    When Changes for "Gallery" menu as "Inactive"
    And Click on "Save" button in "Front Menus"
    Then Open Front site and Go to "Home" Module
    And Verify at front side for "Gallery"

  @FrontMenus
  Scenario: Blog front menu Banner Image Alt text verification scenario
    Given Login as Admin and Go to "Front Menus" Module
    When Changes for "Gallery" menu as "Active with Banner Image"
    And "Set" Banner image alt text
    And Click on "Save" button in "Front Menus"
    Then Open Front site and Go to "Home" Module
    And Verify at front side for "Gallery with Banner Image"
    When Login as Admin and Go to "Front Menus" Module
    When Changes for "Gallery" menu as "Active with Banner Image"
    And "Do not set" Banner image alt text
    And Click on "Save" button in "Front Menus"
    Then Open Front site and Go to "Home" Module
    And Verify at front side for "Gallery with Banner Image"

  @FrontMenus
  Scenario: Services front menu scenario
    Given Login as Admin and Go to "Front Menus" Module
    When Changes for "Services" menu as "Active with Banner Image"
    And Click on "Save" button in "Front Menus"
    Then Open Front site and Go to "Home" Module
    And Verify at front side for "Services with Banner Image"
    When Login as Admin and Go to "Front Menus" Module
    When Changes for "Services" menu as "Active without Banner Image"
    And Click on "Save" button in "Front Menus"
    Then Open Front site and Go to "Home" Module
    And Verify at front side for "Services without Banner Image"
    When Login as Admin and Go to "Front Menus" Module
    When Changes for "Services" menu as "Inactive"
    And Click on "Save" button in "Front Menus"
    Then Open Front site and Go to "Home" Module
    And Verify at front side for "Services"

  @FrontMenus
  Scenario: Services front menu Banner Image Alt text verification scenario
    Given Login as Admin and Go to "Front Menus" Module
    When Changes for "Services" menu as "Active with Banner Image"
    And "Set" Banner image alt text
    And Click on "Save" button in "Front Menus"
    Then Open Front site and Go to "Home" Module
    And Verify at front side for "Services with Banner Image"
    When Login as Admin and Go to "Front Menus" Module
    When Changes for "Services" menu as "Active with Banner Image"
    And "Do not set" Banner image alt text
    And Click on "Save" button in "Front Menus"
    Then Open Front site and Go to "Home" Module
    And Verify at front side for "Services with Banner Image"
