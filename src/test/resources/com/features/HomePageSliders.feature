Feature: Home Page Sliders module scenario

  @HomePageSliderAdd
  Scenario: Create Home Page Sliders and verify details
    Given Login as Admin and Go to "Home Page Sliders" Module
    When I enter all mandatory fields for "add" Home Page Sliders
    And Click on "Upload" button in "Home Page Sliders"
    Then I should get "added successfully." message on "Home Page Sliders"

  @HomePageSliderEdit
  Scenario: Edit Home Page Sliders and verify details
    Given Login as Admin and Go to "Home Page Sliders" Module
    And Click on "Edit" button in "Home Page Sliders"
    Then "Slider" "Edit" page gets open
    When I enter all mandatory fields for "edit" Home Page Sliders
    And Click on "Save" button in "Home Page Sliders"
    Then I should get "updated successfully." message on "Home Page Sliders"

  @HomePageSliderEditSaveContinue
  Scenario: Edit Home Page Sliders and verify details
    Given Login as Admin and Go to "Home Page Sliders" Module
    And Click on "Edit" button in "Home Page Sliders"
    Then "Slider" "Edit" page gets open
    When I enter all mandatory fields for "edit" Home Page Sliders
    And Click on "Save and Continue" button in "Home Page Sliders"
    Then I should get "updated successfully." message on "Home Page Sliders"

  @HomePageSliderDelete
  Scenario: Delete Home Page Sliders and verify details
    Given Login as Admin and Go to "Home Page Sliders" Module
    And Click on Delete button in Home Page Sliders
    Then I should get "deleted successfully." message on "Home Page Sliders"

  @HomePageSliderStatus
  Scenario: Home Page Sliders Active Inactive
    Given Login as Admin and Go to "Home Page Sliders" Module
    Then Make Home Page Sliders status and verify "success message"
