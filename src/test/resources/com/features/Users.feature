Feature: Users module scenario

  @UserAdd
  Scenario: Create user and verify details
    Given Login as Admin and Go to "Users" Module
    #When Verify table column in each grid "Users" page
    When Click on "Add" button in "Users"
    Then "User" "Add" page gets open
    And Click on "Save" button in "Users"
    And Verify validation message for "Users"
    When I enter all mandatory fields for "add" User
    And Click on "Save" button in "Users"
    Then I should get "account created successfully" message on "Users"
    Then Verify details in "Users"

  @UserEdit
  Scenario: Edit user and verify details
    Given Login as Admin and Go to "Users" Module
    When Verify details in "Users"
    And Click on "Edit" button in "Users"
    Then "User" "Edit" page gets open
    #And Verify test data with proper validation message for "Users"
    When I enter all mandatory fields for "edit" User
    And Click on "Save" button in "Users"
    Then I should get "account updated successfully" message on "Users"
    Then Verify details in "Users"

  @UserStatus
  Scenario: User Active Inactive
    Given Login as Admin and Go to "Users" Module
    When Verify details in "Users"
    And "User" is "Inactive"
    Then Make "User" "Active" and verify "error message"

  @UserAddSC
  Scenario: Create user and verify details
    Given Login as Admin and Go to "Users" Module
    When Click on "Add" button in "Users"
    Then "User" "Add" page gets open
    When I enter all mandatory fields for "add" User
    And Click on "Save and Continue" button in "Users"
    Then I should get "account created successfully." message on "Users"
    Then Verify details in "Users"

  @UserEditSC
  Scenario: Edit user and verify details
    Given Login as Admin and Go to "Users" Module
    When Verify details in "Users"
    And Click on "Edit" button in "Users"
    Then "User" "Edit" page gets open
    When I enter all mandatory fields for "edit" User
    And Click on "Save and Continue" button in "Users"
    Then I should get "account updated successfully." message on "Users"
    Then Verify details in "Users"

  @UserDelete
  Scenario: Delete user and verify details
    Given Login as Admin and Go to "Users" Module
    When Verify details in "Users"
    And Click on "Delete" button in "Users"
    Then I should get "account has been deleted successfully." message on "Users"
    Then Verify details in "Users"

  @UserTestingDataDelete
  Scenario: Delete user and verify details
    Given Login as Admin and Go to "Users" Module
    When Verify details in "Users" for Testing Data
    Then Delete Data "Users"

  @userLogin
  Scenario: Delete user and verify details
    Given Login as Admin and Go to "Users" Module
    #When Verify details in "Users"
    Then Verify checkbox for login user "Users" Module

  @multipleuserAdd
  Scenario: Add multiple user and verify details
    Given Login as Admin and Go to "Users" Module
    When Click on "Add" button in "Users"
    Then "User" "Add" page gets open
    When I enter all fields for "add" "Users"

  @multipleuserDelete
  Scenario: Delete multiple user and verify details
    Given Login as Admin and Go to "Users" Module
    And Select all record and Click on "Delete" button in "Users"
    Then I should get "acccount has been deleted successfully." message on "Users"

  @userPagination
  Scenario: user Pagination record verification
    Given Login as Admin and Go to "Settings" Module
    And value get in Settings Module
    And Verify Pagination count in "Users"

  @usersorting
  Scenario: user Sorting record verification
    Given Login as Admin and Go to "Users" Module
    And Verify Sorting record in "Users"
