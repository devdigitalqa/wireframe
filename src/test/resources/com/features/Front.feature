Feature: Front module scenario

  @FrontHeader
  Scenario: Create Front and verify Header details
    Given Open Front site and Go to "Home" Module
    #And Verify the header details
    Then Login as Admin and Go to "Settings" Module
    When Open "Social Networking Links" accordion
    Then Verify validation for maximum selection Social Networking Links
    And Verify required validation message
    #Then Set social media icons "Facebook, Instagram, LinkedIn, Pinterest"
    #Given Open Front site and Go to "Home" Module
    #And Verify social media icons at front side "Facebook, Instagram, LinkedIn, Pinterest"
    #And Verify required validation message
    Then Set social media icons "Instagram, Snapchat, LinkedIn, Facebook"
    Given Open Front site and Go to "Home" Module
    And Verify social media icons at front side

  @FrontBannerSlider
  Scenario: Create Front and verify Banner details
    Given Login as Admin and Go to "Home Page Sliders" Module
    And Get count of Active Home Page Sliders
    When Click on "Home Page Sliders" Settings menu
    And Home page slider setting "Yes"
    Then Open Front site and Go to "Home" Module
    Then Verify the banner title for front if "Yes"
    And Verify sliders count as per CMS
    Then Login as Admin and Go to "Home Page Sliders" Module
    When Click on "Home Page Sliders" Settings menu
    And Home page slider setting "No"
    Then Open Front site and Go to "Home" Module
    Then Verify the banner title for front if "No"

  @FrontAboutUsHomePage
  Scenario: Create Front and verify Header details
    Given Open Front site and Go to "About" Module
    And Verify the Front detail

  @FrontAboutUsDetailPage
  Scenario: Verify front about us detail page
    Given Login as Admin and Go to "About Us Settings" Module
    Then "Add" mission statement text
    Given Open Front site and Go to "About" Module
    And Verify "About" banner text
    And Verify About Us content
    And Verify About Us image
    And Verify About Us mission statement is "available"
    Given Login as Admin and Go to "About Us Settings" Module
    Then "Remove" mission statement text
    And Verify About Us mission statement is "not available"

  @FrontServicesHomePage
  Scenario: Verify front services home page
    Given Login as Admin and Go to "Services" Module
    And Get count of Active Featured Services
    Then Open Front site and scroll down "Services" Module
    Then Verify Service Listing on "Home Page" as per CMS

  @FrontServicesDetailPage
  Scenario: Verify front services detail page
    Given Login as Admin and Go to "Services" Module
    And Get count of Active "Services"
    Then Open Front site and Go to "Services" Module
    And Verify "Services" banner text
    Then Verify Service Listing on "Detail Page" as per CMS

  @FrontGalleryHomePage
  Scenario: Verify front work gallery home page
    Given Login as Admin and Go to "Galleries" Module
    And Get count of Active Galleries
    Then Open Front site and scroll down "Work Gallery" Module
    Then Verify Work Gallery on "Home Page" as per CMS

  @FrontGalleryDetailPage
  Scenario: Verify front work gallery detail page
    Given Login as Admin and Go to "Galleries" Module
    And Get count of Active Galleries
    Then Open Front site and Go to "Work Gallery" Module
    And Verify "Work Gallery" banner text
    Then Verify Work Gallery on "Detail Page" as per CMS

  @FrontPartnersHomePage
  Scenario: Verify front work partners home page
    Given Login as Admin and Go to "Partners" Module
    And Get count of Active Partners
    Then Open Front site and scroll down "Partners" Module
    Then Verify Partners on "Home Page" as per CMS

  @FrontBlogsHomePage
  Scenario: Verify front blogs home page
    Given Open Front site and scroll down "Latest Blogs" Module
    Then Verify Blogs on Home Page

  @FrontBlogsDetailPage
  Scenario: Verify front blogs detail page
    Given Login as Admin and Go to "Blogs" Module
    And Get count of Active "Blogs"
    Then Click on "Blogs" Settings menu
    And Get value of record per page on blog listing page
    Then Open Front site and Go to "Blogs" Module
    And Verify Blogs on "Detail Page" as per CMS
    Then Verify Blogs Content

  @FrontContactUsHomePage
  Scenario: Create Front and verify Header details
    Given Open Front site and scroll down "Contact Us" Module
    Then Verify the element in contact us

  @FrontContactUsDetailPage
  Scenario: Create Front and verify Header details
    Given Open Front site and Go to "Contact Us" Module
    Then Verify the element in contact us

  @FrontFooter
  Scenario: Verify Front Footer content
    Given Open Front site and scroll down "Footer" Module
    And Login as Admin and Go to "Settings" Module
    Then Get CopyRight Content from Settings
    And Open Front site and scroll down "Footer" Module
    Then Verify content of footer
