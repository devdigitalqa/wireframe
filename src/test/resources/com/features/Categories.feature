Feature: Blog Category module scenario

  @BlogCategoryAdd
  Scenario: Create Blog Category and verify details
    Given Login as Admin and Go to "Blogs" Module
    When Click on "Blog Categories" left side menu
    #When Verify table column in each grid "Blog Categories" page
    When Click on "Add" button in "Blog Categories"
    Then "Blog Category" "Add" page gets open
    And Click on "Save" button in "Blog Categories"
    And Verify validation message for "Blog Categories"
    When I enter all mandatory fields for "add" Blog Category
    And Click on "Save" button in "Blog Category"
    Then I should get "blog category added successfully" message on "Blog Category"
    Then Verify details in "Blog Categories"

  @BlogCategoryEdit
  Scenario: Edit Blog Category and verify details
    Given Login as Admin and Go to "Blogs" Module
    When Verify details in "Blog Categories"
    And Click on "Edit" button in "Blog Categories"
    Then "Blog Category" "Edit" page gets open
    #And Verify test data with proper validation message for "Categories"
    When I enter all mandatory fields for "edit" Blog Category
    And Click on "Save" button in "Blog Category"
    Then I should get "blog category updated successfully" message on "Blog Category"
    Then Verify details in "Blog Categories"

  @BlogCategoryStatus
  Scenario: Category Active Inactive
    Given Login as Admin and Go to "Blogs" Module
    When Verify details in "Blog Categories"
    And "Blog Categories" is "Active"
    Then Make "Blog Categories" "Inactive" and verify "success message"
    #Then Verify details in "Categories grid with other filters"
    When Verify details in "Blog Categories"
    And "Blog Categories" is "Inactive"
    Then Make "Blog Categories" "Active" and verify "success message"

  #Then Verify details in "Categories grid with other filters"
  @BlogCategoryDelete
  Scenario: Delete Blog Category and verify details
    Given Login as Admin and Go to "Blogs" Module
    When Verify details in "Blog Categories"
    And Click on "Delete" button in "Blog Categories"
    Then I should get "Selected blog category(s) have been deleted successfully." message on "Categories"
    Then Verify details in "Blog Categories"

  @BlogCategoryAddSaveContinue
  Scenario: Create Blog Category and verify details
    Given Login as Admin and Go to "Blogs" Module
    When Click on "Add" button in "Blog Categories"
    Then "Blog Category" "Add" page gets open
    When I enter all mandatory fields for "add" Blog Category
    And Click on "Save and Continue" button in "Blog Category"
    Then I should get "blog category added successfully" message on "Blog Category"
    Then Verify details in "Blog Categories"

  @BlogCategoryEditSaveContinue
  Scenario: Edit Blog Category and verify details
    Given Login as Admin and Go to "Blogs" Module
    When Verify details in "Blog Categories"
    And Click on "Edit" button in "Blog Categories"
    Then "Blog Category" "Edit" page gets open
    When I enter all mandatory fields for "edit" Blog Category
    And Click on "Save and Continue" button in "Blog Category"
    Then I should get "blog category updated successfully" message on "Blog Category"
    Then Verify details in "Blog Categories"

  @BlogCategoryTestingDataDelete
  Scenario: Delete user and verify details
    Given Login as Admin and Go to "Blogs" Module
    Then Delete Data "Blog Categories"

  #@multiplecategoriesAdd
  #Scenario: Add multiple page and verify details
  #Given Login as Admin and Go to "Categories" Module
  #When Click on "Add" button in "Categories"
  #Then "Category" "Add" page gets open
  #When I enter all fields for "add" "Categories"
  @multiplecategoriesDelete
  Scenario: Delete multiple Blog and verify details
    Given Login as Admin and Go to "Blogs" Module
    And Select all record and Click on "Delete" button in "Blog Categories"
    Then I should get "Selected blog category(s) have been deleted successfully." message on "Blog Categories"
  #@categoriessorting
  #Scenario: Blog Sorting record verification
    #Given Login as Admin and Go to "Categories" Module
    #And Verify Sorting record in "Categories"
