Feature: Services module scenario

  @ServicesAdd
  Scenario: Create Service and verify details
    Given Login as Admin and Go to "Services" Module
    #When Verify table column in each grid "Services" page
    When Click on "Add" button in "Services"
    Then "Service" "Add" page gets open
    And Click on "Save" button in "Services"
    And Verify validation message for "Services"
    When I enter all mandatory fields for "add" Service
    And Click on "Save" button in "Services"
    Then I should get "service created successfully" message on "Service"
    Then Verify details in "Services"

  @ServicesEdit
  Scenario: Edit Service and verify details
    Given Login as Admin and Go to "Services" Module
    When Verify details in "Services"
    And Click on "Edit" button in "Services"
    Then "Service" "Edit" page gets open
    #And Verify test data with proper validation message for "Categories"
    When I enter all mandatory fields for "edit" Service
    And Click on "Save" button in "Service"
    Then I should get "updated successfully" message on "Service"
    Then Verify details in "Services"

  @ServicesActiveInactive
  Scenario: Service Active Inactive
    Given Login as Admin and Go to "Services" Module
    When Verify details in "Services"
    And "Services" is "Active"
    Then Make "Services" "Inactive" and verify "success message"
    #Then Verify details in "Categories grid with other filters"
    When Verify details in "Services"
    And "Services" is "Inactive"
    Then Make "Services" "Active" and verify "success message"

  #Then Verify details in "Categories grid with other filters"
  @ServicesDelete
  Scenario: Delete Service and verify details
    Given Login as Admin and Go to "Services" Module
    When Verify details in "Services"
    And Click on "Delete" button in "Services"
    Then I should get "deleted successfully." message on "Services"
    Then Verify details in "Services"

  @ServicesAddSaveContinue
  Scenario: Create Service and verify details
    Given Login as Admin and Go to "Services" Module
    When Click on "Add" button in "Services"
    Then "Service" "Add" page gets open
    When I enter all mandatory fields for "add" Service
    And Click on "Save and Continue" button in "Service"
    Then I should get "service created successfully" message on "Service"
    Then Verify details in "Categories"

  @ServicesEditSaveContinue
  Scenario: Edit Service and verify details
    Given Login as Admin and Go to "Services" Module
    When Verify details in "Services"
    And Click on "Edit" button in "Services"
    Then "Service" "Edit" page gets open
    When I enter all mandatory fields for "edit" Service
    And Click on "Save and Continue" button in "Service"
    Then I should get "updated successfully" message on "Service"
    Then Verify details in "Services"

  @ServicesTestingDataDelete
  Scenario: Delete Service and verify details
    Given Login as Admin and Go to "Services" Module
    Then Delete Data "Services"
  #@ServicesPagination
  #Scenario: Categories Pagination record verification
    #Given Login as Admin and Go to "Settings" Module
    #And value get in Settings Module
    #And Verify Pagination count in "Categories"
#
  #@ServicesSorting
  #Scenario: Blog Sorting record verification
    #Given Login as Admin and Go to "Categories" Module
    #And Verify Sorting record in "Categories"
