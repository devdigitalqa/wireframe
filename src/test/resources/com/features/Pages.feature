Feature: Pages module scenario

  @PageAdd
  Scenario: Create Page and verify details
    Given Login as Admin and Go to "Pages" Module
    When Verify table column in each grid "Pages" page
    When Click on "Add" button in "Pages"
    Then "Page" "Add" page gets open
    And Click on "Save" button in "Page"
    And Verify validation message for "Pages"
    When I enter all mandatory fields for "add" Page
    And Click on "Save" button in "Page"
    Then I should get "page created successfully." message on "Pages"
    Then Verify details in "Pages"

  @PageEdit
  Scenario: Edit Page and verify details
    Given Login as Admin and Go to "Pages" Module
    When Verify details in "Pages"
    And Click on "Edit" button in "Pages"
    Then "Page" "Edit" page gets open
    When I enter all mandatory fields for "edit" Page
    And Click on "Save" button in "Page"
    Then I should get "page updated successfully." message on "Pages"
    Then Verify details in "Pages"

  @PageStatus
  Scenario: Page Active Inactive
    Given Login as Admin and Go to "Pages" Module
    When Verify details in "Pages"
    And "Pages" is "Active"
    Then Make "Pages" "Inactive" and verify "success message"
    #Then Verify details in "Pages grid with other filters"
    When Verify details in "Pages"
    And "Pages" is "Inactive"
    Then Make "Pages" "Active" and verify "success message"
    Then Verify details in "Pages"

  @PageAddSaveContinue
  Scenario: Create Page and verify details
    Given Login as Admin and Go to "Pages" Module
    When Click on "Add" button in "Pages"
    Then "Page" "Add" page gets open
    When I enter all mandatory fields for "add" Page
    And Click on "Save and Continue" button in "Page"
    Then I should get "page created successfully." message on "Pages"
    Then Verify details in "Pages"

  @PageEditSaveContinue
  Scenario: Edit Page and verify details
    Given Login as Admin and Go to "Pages" Module
    When Verify details in "Pages"
    And Click on "Edit" button in "Pages"
    Then "Page" "Edit" page gets open
    When I enter all mandatory fields for "edit" Page
    And Click on "Save and Continue" button in "Page"
    Then I should get "page updated successfully." message on "Pages"
    Then Verify details in "Pages"

  @MultiplePageAdd
  Scenario: Add multiple page and verify details
    Given Login as Admin and Go to "Pages" Module
    When Click on "Add" button in "Pages"
    Then "Page" "Add" page gets open
    When I enter all fields for "add" "Pages"

  #And Click on "Save" button in "Users"
  #Then I should get "account created successfully." message on "Users"
  #Then Verify details in "Users"
  @PagesSorting
  Scenario: Page Sorting record verification
    Given Login as Admin and Go to "Pages" Module
    And Verify Sorting record in "Pages"
