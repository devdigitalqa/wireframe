Feature: Partners module scenario

  @PartnersAdd
  Scenario: Create Partners and verify details
  	Given Login as Admin and Get Module Title
    Given Login as Admin and Go to "Partners" Module
    When Click on "Add" button in "Partners"
    When I enter all mandatory fields for "add" Partner
    And Click on "Upload" button in "Partners"
    Then I should get "added successfully" message on "Partners"

  @PartnersEdit
  Scenario: Edit Partners and verify details
    Given Login as Admin and Get Module Title
    Given Login as Admin and Go to "Partners" Module
    And Click on "Edit" button in "Partners"
    Then "Partners" "Edit" page gets open
    When I enter all mandatory fields for "edit" Partner
    And Click on "Save" button in "Partners"
    Then I should get "updated successfully" message on "Partners"

  @PartnersEditSaveContinue
  Scenario: Edit Partners and verify details
    Given Login as Admin and Get Module Title
    Given Login as Admin and Go to "Partners" Module
    And Click on "Edit" button in "Partners"
    Then "Partners" "Edit" page gets open
    When I enter all mandatory fields for "edit" Partner
    And Click on "Save and Continue" button in "Partners"
    Then I should get "updated successfully" message on "Partners"

  @PartnersDelete
  Scenario: Delete Partners and verify details
    Given Login as Admin and Get Module Title
    Given Login as Admin and Go to "Partners" Module
    When Select Partner
    And Click on Delete button in Partner
    Then I should get "deleted successfully" message on "Partners"

  @PartnersStatus
  Scenario: Partners Active Inactive
    Given Login as Admin and Get Module Title
    Given Login as Admin and Go to "Partners" Module
    When Select Partner
    Then Make Partner status and verify "success message"
