package com.basicactions;

import java.io.File;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import com.utilities.ReadPropFile;

public class MailSend {
	ReadPropFile readPropFile = new ReadPropFile();
	String smtpServer;
	String email;
	String password;
	String emailFrom;
	String emailTo;
	String emailToCC;
	String emailSubject;
	String emailBodyContent;
	Message msg;
	Session session;
	Multipart multipart;
	BodyPart messageBodyPart;

	public void mailSend(String gmailEmail, String gmailPassword, String emailTo, String emailSubject,
			String emailBodyContent) {
		mailConfig(gmailEmail, gmailPassword, emailTo, emailSubject, emailBodyContent);
		try {
			mailBody();
			mailEnclose();
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}

	public void mailConfig(String gmailEmail, String gmailPassword, String emailTo, String emailSubject,
			String emailBodyContent) {
		// for example, smtp.mailgun.org
		smtpServer = "smtp.gmail.com";

		email = gmailEmail;
		password = gmailPassword;

		this.emailFrom = gmailEmail;
		this.emailTo = emailTo;
		emailToCC = "";

		this.emailSubject= emailSubject;

		this.emailBodyContent = emailBodyContent;

		Properties prop = System.getProperties();
		prop.put("mail.smtp.host", smtpServer);
		prop.put("mail.smtp.auth", "true");
		prop.put("mail.smtp.port", "587");
		prop.put("mail.smtp.starttls.enable", "true");

		session = Session.getInstance(prop, null);
		msg = new MimeMessage(session);
	}

	public void mailBody() throws MessagingException {
		// from
		msg.setFrom(new InternetAddress(emailFrom));

		// to
		msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emailTo, false));

		// cc
		msg.setRecipients(Message.RecipientType.CC, InternetAddress.parse(emailToCC, false));

		// subject
		msg.setSubject(emailSubject);

		// Create the message part
		messageBodyPart = new MimeBodyPart();

		// Now set the actual message
		messageBodyPart.setText(emailBodyContent);

		// Create a multipart message
		multipart = new MimeMultipart();

		// Set text message part
		multipart.addBodyPart(messageBodyPart);
	}

	public void mailEnclose() throws MessagingException {
		// Send the complete message parts
		msg.setContent(multipart);

		// Get SMTPTransport
		Transport t = session.getTransport("smtp");

		// connect
		t.connect(smtpServer, email, password);

		// send
		t.sendMessage(msg, msg.getAllRecipients());

		t.close();
	}

	public void mailSend(String gmailEmail, String gmailPassword, String emailTo, String emailSubject,
			String emailBodyContent, String[] attachmentNameWithExtension) {
		mailConfig(gmailEmail, gmailPassword, emailTo, emailSubject, emailBodyContent);
		try {
			mailBody();

			// Add Attachments
			String[] filename = attachmentNameWithExtension;
			for (int i = 0; i < filename.length; i++) {
				addAttachment(multipart, filename[i]);
			}

			mailEnclose();

		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}

	public void addAttachment(Multipart multipart, String filename) throws MessagingException {
		File file = new File(filename);
		if (file.exists()) {
			messageBodyPart = new MimeBodyPart();
			DataSource source = new FileDataSource(filename);
			messageBodyPart.setDataHandler(new DataHandler(source));
			messageBodyPart.setFileName(filename);
			multipart.addBodyPart(messageBodyPart);
		}
	}

}
