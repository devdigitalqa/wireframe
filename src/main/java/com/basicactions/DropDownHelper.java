package com.basicactions;

import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class DropDownHelper {

	@SuppressWarnings("unused")
	private WebDriver driver;
	String deselectionDropdownText = "Deselection using in dropdown by ";
	String selectionDropdownText = "Selection using in dropdown by ";
	private Logger log = LogHelper.getLogger(DropDownHelper.class);

	public DropDownHelper(WebDriver driver) {
		this.driver = driver;
	}

	public void selectByVaule(WebElement element, String value) {
		Select select = new Select(element);
		log.info(selectionDropdownText + value + " value");
		select.selectByValue(value);
	}

	public void selectByIndex(WebElement element, int index) {
		Select select = new Select(element);
		log.info(selectionDropdownText + index + " index");
		select.selectByIndex(index);
	}

	public void selectVisibleText(WebElement element, String visibleText) {
		Select select = new Select(element);
		log.info(selectionDropdownText + visibleText);
		select.selectByVisibleText(visibleText);
	}

	public void deselectByVaule(WebElement element, String value) {
		Select select = new Select(element);
		log.info(deselectionDropdownText + value + " value");
		select.deselectByValue(value);
	}

	public void deselectByIndex(WebElement element, int index) {
		Select select = new Select(element);
		log.info(deselectionDropdownText + index + " index");
		select.deselectByIndex(index);
	}

	public void deselectVisibleText(WebElement element, String visibleText) {
		Select select = new Select(element);
		log.info(deselectionDropdownText + visibleText);
		select.deselectByVisibleText(visibleText);
	}

	public boolean isMultiple(WebElement element) {
		Select select = new Select(element);
		return select.isMultiple();
	}

	public void deselectAll(WebElement element) {
		Select select = new Select(element);
		select.deselectAll();
	}

	public WebElement getFirstSelectedOption(WebElement element) {
		Select select = new Select(element);
		return select.getFirstSelectedOption();
	}

	public List<String> getAllDropDownData(WebElement element) {
		Select select = new Select(element);
		List<WebElement> elementList = select.getOptions();
		List<String> valueList = new LinkedList<>();
		for (WebElement ele : elementList) {
			log.info("List of options: " + ele.getText());
			valueList.add(ele.getText());
		}
		return valueList;
	}

	public List<String> getAllSelectData(WebElement element) {
		Select select = new Select(element);
		List<WebElement> elementList = select.getAllSelectedOptions();
		List<String> valueList = new LinkedList<>();
		for (WebElement ele : elementList) {
			log.info("List of all selected data: " + ele.getText());
			valueList.add(ele.getText());
		}
		return valueList;
	}
}
