package com.utilities;

import org.openqa.selenium.WebDriver;

public class CommonVariables {

	public static final String USERS = "Users";
	public static final String BLOGS = "Blogs";
	public static final String PAGES = "Pages";
	public static final String PARTNERS = "Partners";
	public static final String IP_TRACKER = "IP Tracker";
	public static final String SETTINGS = "Settings";
	public static final String BlogSETTINGS = "Settings";
	public static final String BlogCategories = "Blog Categories";
	public static final String CHANGEPWD = "Changepwd";
	public static final String STYLE = "style";
	public static final String DISPLAY_NONE = "display: none;";
	public static final String CATEGORIES = "Categories";
	public static final String SERVICES = "Services";
	public static final String CONTACT_SETTINGS = "Contactsetting";
	public static final String HOME_PAGE_SLIDERS = "Home Page Sliders";
	public static final String GALLERIES = "Galleries";
	public static final String HEADER = "Header";

	public static final String ALLSTRINGS = "All Strings";
	public static final String TABLE_COLUMNS = "Table Columns";
	public static final String CONTACTUS = "Contact Us";
	public static final String CONTACT_INQUIRIES = "Contact inquiries";
	public static final String FRONT = "Front";

	String numRecord = "";

	public String getNumRecord() {
		return numRecord;
	}

	public void setNumRecord(String numRecord) {
		this.numRecord = numRecord;
	}

	String blogRecordPage = "";

	public String getBlogRecordPage() {
		return blogRecordPage;
	}

	public void setBlogRecordPage(String blogRecordPage) {
		this.blogRecordPage = blogRecordPage;
	}

	String blogCategoryCount = "";

	public String getBlogCategoryCount() {
		return blogCategoryCount;
	}

	public void setBlogCategoryCount(String blogCategoryCount) {
		this.blogCategoryCount = blogCategoryCount;
	}

	String email = "";

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	String publishDate = "";

	public String getPublishDate() {
		return publishDate;
	}

	public void setPublishDate(String publishDate) {
		this.publishDate = publishDate;
	}

	String txtSearchCmnVar = "";

	public String getTxtSearchCmnVar() {
		return txtSearchCmnVar;
	}

	public void setTxtSearchCmnVar(String txtSearchCmnVar) {
		this.txtSearchCmnVar = txtSearchCmnVar;
	}

	String pagetitle = "";
	String changename = "";

	public String getChangename() {
		return changename;
	}

	public void setChangename(String changename) {
		this.changename = changename;
	}

	String adminauthor = "";

	public String getAdminauthor() {
		return adminauthor;
	}

	public void setAdminauthor(String adminauthor) {
		this.adminauthor = adminauthor;
	}

	String frontURL = "";

	public String getFrontURL() {
		return frontURL;
	}

	public void setFrontURL(String frontURL) {
		this.frontURL = frontURL;
	}

	String pageContentTitle = "";

	public String getPageContentTitle() {
		return pageContentTitle;
	}

	public void setPageContentTitle(String pageContentTitle) {
		this.pageContentTitle = pageContentTitle;
	}

	String adminURL = "";
	public String loginEmail = "";
	public String loginPwd = "";
	boolean deleteRecord = false;

	public boolean isDeleteRecord() {
		return deleteRecord;
	}

	public void setDeleteRecord(boolean deleteRecord) {
		this.deleteRecord = deleteRecord;
	}

	boolean saveandcontinue = false;

	public boolean isSaveandcontinue() {
		return saveandcontinue;
	}

	public void setSaveandcontinue(boolean saveandcontinue) {
		this.saveandcontinue = saveandcontinue;
	}

	String inactive = "";

	public String getInactive() {
		return inactive;
	}

	public void setInactive(String inactive) {
		this.inactive = inactive;
	}

	ReadPropFile readPropFile;
	String browserName = "";

	public String getBrowserName() {
		return browserName;
	}

	public void setBrowserName(String browserName) {
		this.browserName = browserName;
	}

	WebDriver driver;

	public CommonVariables() {
		readPropFile = new ReadPropFile();
		readPropFile.readProp();
		setBrowserName(ReadPropFile.getProp().getProperty("browser"));
	}

	public String getAdminURL() {
		return adminURL;
	}

	public void setAdminURL(String url) {
		this.adminURL = url;
	}

	boolean testingData = false;

	public boolean isTestingData() {
		return testingData;
	}

	public void setTestingData(boolean testingData) {
		this.testingData = testingData;
	}

	String contactUsTitleCMS = "";

	public String getContactUsTitleCMS() {
		return contactUsTitleCMS;
	}

	public void setContactUsTitleCMS(String contactUsTitleCMS) {
		this.contactUsTitleCMS = contactUsTitleCMS;
	}

	String copyRightText = "";

	public String getCopyRightText() {
		return copyRightText;
	}

	public void setCopyRightText(String copyRightText) {
		this.copyRightText = copyRightText;
	}

	boolean socialMediaLinksFront = true;

	public boolean isSocialMediaLinksFront() {
		return socialMediaLinksFront;
	}

	public void setSocialMediaLinksFront(boolean socialMediaLinksFront) {
		this.socialMediaLinksFront = socialMediaLinksFront;
	}

	String status = "";

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}