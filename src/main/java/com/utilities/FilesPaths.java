package com.utilities;

public class FilesPaths {

	private FilesPaths() {
	}

	static String userDir = "user.dir";

	public static final String CONFIG_PROPERTIES_FILE = System.getProperty(userDir)
			+ "\\src\\main\\resources\\com\\configfiles\\config.properties";

	public static final String XPATH_PROPERTIES_FILE = System.getProperty(userDir)
			+ "\\src\\main\\resources\\com\\configfiles\\xpath.properties";

	public static final String LOG_PROPERTIES_FILE = System.getProperty(userDir)
			+ "\\src\\main\\resources\\com\\configfiles\\log4j.properties";

	public static final String CHROME_DRIVER = System.getProperty(userDir)
			+ "\\src\\main\\resources\\com\\drivers\\chromedriver.exe";

	public static final String GECKO_DRIVER_FF = System.getProperty(userDir)
			+ "\\src\\main\\resources\\com\\drivers\\geckodriver.exe";

	public static final String IE_DRIVER = System.getProperty(userDir)
			+ "\\src\\main\\resources\\com\\drivers\\IEDriverServer.exe";

	public static final String TEST_DATA = System.getProperty(userDir) + "\\src\\test\\resources\\com\\testdata\\";

	public static final String EXTRA_FILES_FOLDER = System.getProperty(userDir)
			+ "\\src\\main\\resources\\com\\extrafiles\\";

	public static final String PROJECT_FOLDER = System.getProperty(userDir) + "\\";

	public static final String TEMPLATE9WIREFRAME = EXTRA_FILES_FOLDER + "Template9WireFrame.xls";

	public static final String TEMPLATE9SETTINGSBACKUP = EXTRA_FILES_FOLDER + "Template9SettingsBackup.xls";

	public static final String JSON_DATA_LARAVEL_FILE = EXTRA_FILES_FOLDER + "laravel.json";

	public static final String JSON_DATA_CREDENTIALS_FILE = EXTRA_FILES_FOLDER + "credentials.json";

	public static final String CONTACT_INQUIRIES_EXCEL = EXTRA_FILES_FOLDER + "Contact inquiries.xlsx";

}