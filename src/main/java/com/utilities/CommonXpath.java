package com.utilities;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CommonXpath {

	@FindBy(xpath = "//*[@id='carouselCaptions']/div[1]")
	public WebElement bannerImage;

	@FindBy(xpath = "//*[@id='carouselCaptions']/div[1]/div[1]/div[1]/div/h3")
	public WebElement bannerTitle;

	@FindBy(xpath = "//*[@id='carouselCaptions']/div[1]/div[1]/div[1]/div/div/a")
	public WebElement button;

	@FindBy(xpath = "//*[@id='about_us_for_mission_statement']")
	public WebElement about_us_mission_statement;

	@FindBy(xpath = "/html/body/main/div[2]/div/div/div/div/div[1]/div/h2")
	public WebElement aboutusHomepageTitle;

	@FindBy(xpath = "/html/body/main/section/div[2]/div/div[1]/div/div/div/div/div[1]/div/div/p[1]")
	public List<WebElement> aboutus_Content;

	@FindBy(xpath = "/html/body/main/section/div[2]/div/div[2]/div/div/div/div/p")
	public List<WebElement> aboutus_Mission;

	@FindBy(xpath = "//*[@id='carouselCaptions']/div[1]/div[2]/div[1]/div/p")
	public WebElement description;

	@FindBy(xpath = "//*[@id='carouselExampleControls']/div/div/div[1]/picture/img")
	public WebElement banner2Image;

	@FindBy(xpath = "//*[@id='carouselExampleControls']/div/div/div[2]/div[1]/h1")
	public WebElement banner2Title;

	@FindBy(xpath = "//*[@id='carouselExampleControls']/div/div/div[2]/div[1]/p")
	public WebElement banner2Description;

	@FindBy(xpath = "//*[@id='carouselExampleControls']/div/div/div[2]/div[2]/a[1]/img")
	public WebElement googlePaybutton;

	@FindBy(xpath = "/html/body/main/section/div[2]/div/div[1]/div/div/div/div/div[2]/div/div/picture/img")
	public List<WebElement> aboutImage;

	@FindBy(xpath = "//*[@id='carouselExampleControls']/div/div/div[2]/div[2]/a[2]/img")
	public WebElement appStorebutton;

	@FindBy(xpath = "/html/body/main/div[2]/div/div/div/div/div[2]/div/picture/img")
	public WebElement aboutusImg;

	@FindBy(xpath = "/html/body/main/div[2]/div/div/div/div/div[1]/div/div[1]/p[1]")
	public WebElement aboutusTitle;

	@FindBy(xpath = "/html/body/main/div[2]/div/div/div/div/div[1]/div/div[2]/a")
	public WebElement aboutusCalltoAction;

	@FindBy(xpath = "/html/body/main/div[4]/div/div[2]/div/div/div[7]/div/div/div/picture/img")
	public WebElement galleryImg;

	@FindBy(xpath = "/html/body/main/div[3]/div/div/div/div[2]/div[1]/div/div[1]")
	public WebElement serviceIcon;

	@FindBy(xpath = "/html/body/main/div[3]/div/div/div/div[2]/div[1]/div/div[2]/p")
	public WebElement serviceDescription;

	@FindBy(xpath = "/html/body/main/div[3]/div/div/div/div[3]/div/a")
	public WebElement serviceViewMoreButton;

	@FindBy(xpath = "/html/body/main/div[6]/div/div/div/div[1]/div/h2")
	public WebElement blogTitle;

	@FindBy(xpath = "/html/body/main/div[6]/div/div/div/div[2]/div[1]/div/div[1]")
	public WebElement blogImg;

	@FindBy(xpath = "//*[@id='navbarNavAltMarkup']/ul/li[6]/a")
	public WebElement conatctMenu;

	@FindBy(xpath = "/html/body/main/div/div[2]/div/div/div/div/div/div[1]/div[1]/div[1]/div/div/a[1]/div/span")
	public WebElement blogAuthorDateTime;

	@FindBy(xpath = "/html/body/main/div/div[2]/div/div/div/div/div/div[1]/div[2]/a")
	public WebElement blogLoadMoreButton;

	@FindBy(xpath = "/html/body/main/div/div[2]/div/div/div/div/div/div[2]/div[1]/div/form/div/input")
	public WebElement blogListSearchbox;

	@FindBy(xpath = "/html/body/main/div/div[2]/div/div/div/div/div/div[2]/div[1]/div/form/div/div/button/img")
	public WebElement blogListSearchIcon;

	@FindBy(xpath = "//*[@id='navbarNavAltMarkup']/ul/li[5]/a")
	public WebElement blogMenu;

	@FindBy(xpath = "/html/body/main/div/div[2]/div/div/div/div/div/div[1]/div[1]/div[1]/div/div/a[1]/div")
	public WebElement blogTitleDateTime;

	@FindBy(xpath = "/html/body/main/div/div[2]/div/div/div/div/div/div[1]/div[1]/div[1]/div/div/a[2]")
	public WebElement readMoreButton;

	@FindBy(xpath = "/html/body/main/div/div[2]/div/div/div/div/div/div[2]/div[2]/h5")
	public WebElement postedCategory;

	@FindBy(xpath = "/html/body/main/div/div[2]/div/div/div/div/div/div[2]/div[2]/div/ul/li[1]/a")
	public WebElement testCategory;

	@FindBy(xpath = "/html/body/main/div/div[2]/div/div/div/div/div/div[1]/div[1]/div[1]/h2")
	public WebElement blogDetailTitle;

	@FindBy(xpath = "/html/body/main/div/div[2]/div/div/div/div/div/div[1]/div[1]/div[1]/ul/li[1]")
	public WebElement blogDetailAuthor;

	@FindBy(xpath = "/html/body/main/div/div[2]/div/div/div/div/div/div[1]/div[1]/div[1]/ul/li[2]")
	public WebElement blogDetailDate;

	@FindBy(xpath = "/html/body/main/div/div[2]/div/div/div/div/div/div[1]/div[1]/div[2]/div[1]/div/picture/img")
	public WebElement blogDetailImage;

	@FindBy(xpath = "/html/body/main/div/div[2]/div/div/div/div/div/div[1]/div[1]/div[2]/div[2]/p[1]")
	public WebElement blogDetailDescription;

	@FindBy(xpath = "/html/body/main/div/div[2]/div/div/div/div/div/div[1]/div[2]/div[1]/div/a")
	public WebElement blogDetailPrevLink;

	@FindBy(xpath = "/html/body/main/div/div[2]/div/div/div/div/div/div[1]/div[2]/div[2]/div/a")
	public WebElement blogDetailNextLink;

	@FindBy(xpath = "/html/body/main/div/div[2]/div/div/div/div/div[1]/div[1]/div/div/div/div/div/div/form/div[2]/h2")
	public WebElement contactTitle;

	@FindBy(xpath = "/html/body/main/div/div[2]/div/div/div/div/div[1]/div[1]/div/div/div/div/div/div/form/div[3]/div[1]/div/input")
	public WebElement contactFirstname;

	@FindBy(xpath = "/html/body/main/div/div[2]/div/div/div/div/div[1]/div[1]/div/div/div/div/div/div/form/div[3]/div[2]/div/input")
	public WebElement contactLastName;

	@FindBy(xpath = "/html/body/main/div/div[2]/div/div/div/div/div[1]/div[1]/div/div/div/div/div/div/form/div[3]/div[3]/div/input")
	public WebElement contactPhone1;

	@FindBy(xpath = "/html/body/main/div/div[2]/div/div/div/div/div[1]/div[1]/div/div/div/div/div/div/form/div[3]/div[4]/div/input")
	public WebElement contactEmail1;

	@FindBy(xpath = "/html/body/main/div/div[2]/div/div/div/div/div[1]/div[1]/div/div/div/div/div/div/form/div[3]/div[5]/div/textarea")
	public WebElement contactMessage;

	@FindBy(xpath = "/html/body/main/div/div[2]/div/div/div/div/div[1]/div[1]/div/div/div/div/div/div/form/div[3]/div[6]/div/img")
	public WebElement contactCaptcha;

	@FindBy(xpath = "/html/body/main/div/div[2]/div/div/div/div/div[1]/div[1]/div/div/div/div/div/div/form/div[3]/div[7]/div/input")
	public WebElement submit;

	@FindBy(xpath = "/html/body/main/div/div[2]/div/div/div/div/div[1]/div[2]/div/div[1]/address")
	public WebElement contactAddress;

	@FindBy(xpath = "/html/body/main/div/div[2]/div/div/div/div/div[1]/div[2]/div/div[2]/span/a")
	public WebElement contactEmail;

	@FindBy(xpath = "/html/body/main/div/div[2]/div/div/div/div/div[1]/div[2]/div/div[3]/span/a")
	public WebElement contactPhone;

	@FindBy(xpath = "/html/body/main/div/div[2]/div/div/div/div/div[2]/div/iframe")
	public WebElement contactGoogleMap;

	@FindBy(xpath = "/html/body/main/div/div[2]/div/div/div/div/div/div[1]/div[1]/div[1]/div")
	public WebElement menuOption;

	@FindBy(xpath = "//div[@class='loader']")
	public List<WebElement> elementList;

	@FindBy(id = "first_name")
	public WebElement userFirstName;

	@FindBy(id = "title")
	public WebElement title;

	@FindBy(xpath = "//*[@id='status']")
	public WebElement status;

	@FindBy(xpath = "//*[@id='featured']")
	public WebElement featruedDropdown;

	@FindBy(xpath = "//*[@id='DataTables_Table_0']/tbody/tr[1]/td[6]/a")
	public WebElement categorieseditbutton;

	@FindBy(xpath = "//input[@type='file']")
	public WebElement image;

	@FindBy(xpath = "//*[@id='image_alt']")
	public WebElement imageAlt;

	@FindBy(xpath = "//*[@id='alt_image_text']")
	public WebElement galleryImageAlt;

	@FindBy(xpath = "//*[@id='meta_description']")
	public WebElement categoriesMetadesc;

	@FindBy(xpath = "//div[@class='loader']")
	public WebElement element;

	@FindBy(xpath = "//*[@id='main']/div[2]/div/div/div[2]/a")
	public WebElement usersMenu;

	@FindBy(xpath = "//button[@id='dropdownMenu2']")
	public WebElement profileMainMenu;

	@FindBy(xpath = "//div[@aria-labelledby='dropdownMenu2']/a[1]")
	public WebElement myProfileMenu;

	@FindBy(xpath = "//div[@aria-labelledby='dropdownMenu2']/a[2]")
	public WebElement changePasswordMenu;

	@FindBy(xpath = "//input[@id='email']")
	public WebElement username;

	@FindBy(xpath = "//input[@id='email']")
	public List<WebElement> usernameList;

	@FindBy(xpath = "//input[@id='password']")
	public WebElement password;

	@FindBy(xpath = "//button[@type='submit']")
	public WebElement login;

	@FindBy(id = "first_name")
	public WebElement firstName;

	@FindBy(id = "last_name")
	public WebElement lastName;

	@FindBy(id = "email")
	public WebElement userEmail;

	@FindBy(xpath = "//*[@id='DataTables_Table_0']/thead/tr/th[2]/div")
	public WebElement selectAllCheckbox;

	@FindBy(xpath = "//*[@id='frmaddedit']/div[1]/div[1]/h1")
	public WebElement addTitle;

	@FindBy(xpath = "//*[@id='main']/div[1]/span")
	public WebElement successmsg;

	@FindBy(xpath = "//table[@id='DataTables_Table_0']/tbody/tr[1]/td[1]/a[1]/span[1]")
	public WebElement statuscolumn;

	@FindBy(xpath = "//*[@id='action_btns']/div/a[3]")
	public WebElement deletebutton;

	@FindBy(xpath = "//*[@id='confirm-action-submit']")
	public WebElement confirmyesbutton;

	@FindBy(xpath = "//*[@id='search']")
	public WebElement searchTextbox;

	@FindBy(xpath = "//*[@id='addimagebtn']")
	public WebElement homeSliderAddButton;

	@FindBy(xpath = "//*[@for='selectAll']")
	public WebElement selectCheckBox;

	@FindBy(xpath = "//*[@id='action_btns']/div/a[2]")
	public WebElement inactiveButton;

	@FindBy(xpath = "//*[@id='confirm-action-submit']")
	public WebElement submitButton;

	@FindBy(xpath = "//*[@id='action_btns']/div/a[1]")
	public WebElement activeButton;

	@FindBy(xpath = "//*[@id='main']/div[1]/span")
	public WebElement inactiveMsgGallery;

	@FindBy(xpath = "//*[@id='confirm-gallery-delete']")
	public WebElement gallerySubmit;

	@FindBy(xpath = "//*[@id='btnsave']")
	public WebElement displaySliderSave;

	@FindBy(xpath = "//*[@id='main']/div[3]/form/div/div[2]/div[1]/div/div/div[1]/div/a[3]")
	public WebElement sliderDelete;

	@FindBy(xpath = "//*[@id='confirm-slider-delete']")
	public WebElement sliderSubmit;

	@FindBy(xpath = "//*[@id='search']")
	public WebElement sliderSearchTextBox;

	@FindBy(xpath = "//*[@id='search-btn']")
	public WebElement searchButton;

	@FindBy(xpath = "//*[@id='btnsearch']")
	public WebElement sliderSearchButton;

	@FindBy(xpath = "//*[@id='sortable']/div")
	public List<WebElement> imagesCMS;

	@FindBy(xpath = "//*[@id='basic-uploader']/div[3]/table/tbody/tr/td[4]/button[1]")
	public WebElement uploadButton;

	@FindBy(xpath = "//*[@id='startall']")
	public WebElement uploadAllButton;

	@FindBy(xpath = "//*[@id='records_per_page']")
	public WebElement noOfRecordsPerPage;

	@FindBy(xpath = "//*[@id='dropdownMenu2']")
	public WebElement profileMenu;

	@FindBy(xpath = "/html/body/header/div/div/div[2]/div/div/a[4]")
	public WebElement logoutProfile;

	@FindBy(xpath = "//*[@id='btnsave']")
	public WebElement settingSave;

	@FindBy(xpath = "//*[@id='navbarDropdown' and @title='Home Page Sliders']//parent::li[1]/div[1]/a[2]")
	public WebElement homeSettingMenu;

	@FindBy(xpath = "//*[@id='navbarDropdown' and @title='Contact Us']//parent::li[1]/div[1]/a[2]")
	public WebElement contactSetting;

	@FindBy(xpath = "//*[@id='frmaddedit']/div[1]/h1")
	public WebElement contactSettingTitle;

	@FindBy(xpath = "//*[@id='site-config-heading-12']/h5/a")
	public WebElement blogSettingTitle;

	@FindBy(id = "meta_title")
	public WebElement metaTitle;

	@FindBy(id = "meta_desc")
	public WebElement metaDescription;

	@FindBy(xpath = "//*[@id='DataTables_Table_0']/tbody/tr/td[4]")
	public WebElement tableRow;

	@FindBy(xpath = "//*[@id='DataTables_Table_0']/tbody/tr/td[4]")
	public List<WebElement> tableRowList;

	@FindBy(xpath = "//*[@id='DataTables_Table_0']/tbody/tr/td[4]/a")
	public WebElement tableRowATag;

	@FindBy(xpath = "//*[@id='DataTables_Table_0']/tbody/tr/td[4]/a")
	public List<WebElement> tableRowListATag;

	@FindBy(xpath = "//*[@id='about_us_for_mission_statement']")
	public WebElement aboutUsMissionStatementTextbox;

	@FindBy(xpath = "//*[@id='tinymce']/p")
	public WebElement descriptionEditor;

	// Front

	// About Us
	@FindBy(xpath = "//div[contains(@class,'banner-text-box')]/h1[1]")
	public WebElement subPageBannerText;

	@FindBy(xpath = "//div[contains(@class,'about-content')]//p")
	public WebElement aboutUsContent;

	@FindBy(xpath = "//div[contains(@class,'about-content')]//p")
	public List<WebElement> aboutUsContentList;

	@FindBy(xpath = "//div[contains(@class,'about-img')]//img")
	public WebElement aboutUsImage;

	@FindBy(xpath = "//div[contains(@class,'about-img')]//img")
	public List<WebElement> aboutUsImageList;

	@FindBy(xpath = "//div[contains(@class,'about-mission')]//h2[1]")
	public WebElement aboutUsMissionTitle;

	@FindBy(xpath = "//div[contains(@class,'about-mission')]//h2[1]")
	public List<WebElement> aboutUsMissionTitleList;

	@FindBy(xpath = "//div[contains(@class,'about-mission-content')]//p[1]")
	public WebElement aboutUsMissionContent;

	@FindBy(xpath = "//div[contains(@class,'about-mission-content')]//p[1]")
	public List<WebElement> aboutUsMissionContentList;

	// Services
	@FindBy(xpath = "//div[contains(@class,'service-listing')]/div[1]/div[1]/div")
	public List<WebElement> servicesListingList;

	@FindBy(xpath = "//div[contains(@class,'service-listing')]/div[1]/div[1]/div/h3[1]")
	public WebElement servicesDetailPageNoServicesFoundText;

	@FindBy(xpath = "//div[contains(@class,'hm-service')]")
	public WebElement servicesSection;

	@FindBy(xpath = "//div[contains(@class,'hm-service')]")
	public List<WebElement> servicesSectionList;

	@FindBy(xpath = "//div[contains(@class,'hm-service')]/div[1]/div[1]/div[1]/div[1]/div[1]/h2[1]")
	public WebElement servicesTitle;

	@FindBy(xpath = "//div[contains(@class,'hm-service')]/div[1]/div[1]/div[1]/div[2]/div")
	public List<WebElement> servicesListCountFront;

	// Work Gallery
	@FindBy(xpath = "//div[contains(@class,'hm-gallery')]")
	public WebElement gallerySection;

	@FindBy(xpath = "//div[contains(@class,'hm-gallery')]")
	public List<WebElement> gallerySectionList;

	@FindBy(xpath = "//div[contains(@class,'hm-gallery')]/div[1]/div[1]/div[1]/h2[1]")
	public WebElement galleryTitle;

	@FindBy(xpath = "//div[contains(@class,'hm-gallery')]/div[1]/div[2]/button[1]")
	public WebElement galleryPreviousButton;

	@FindBy(xpath = "//div[contains(@class,'hm-gallery')]/div[1]/div[2]/button[2]")
	public WebElement galleryNextButton;

	@FindBy(xpath = "//div[contains(@class,'hm-gallery')]/div[1]/div[2]/div[1]/div[1]/div[@role='option']")
	public List<WebElement> galleryImagesCount;

	@FindBy(xpath = "//div[contains(@class,'gallery-list-wrap')]/div[1]/div")
	public List<WebElement> galleryImagesDetailPageCount;

	@FindBy(xpath = "//div[contains(@class,'gallery-list-wrap')]/div[1]/div/h3[1]")
	public WebElement galleryImagesDetailPageNoGalleryFoundText;

	// Partners
	@FindBy(xpath = "//div[contains(@class,'hm-partner')]")
	public WebElement partnersSection;

	@FindBy(xpath = "//div[contains(@class,'hm-partner')]")
	public List<WebElement> partnersSectionList;

	@FindBy(xpath = "//div[contains(@class,'hm-partner')]//h2[1]")
	public WebElement partnersTitle;

	@FindBy(xpath = "//*[@id='partner']/div/div/div/ul/li/img")
	public List<WebElement> partnersImagesCount;

	// Contact Us
	@FindBy(xpath = "//div[contains(@class,'contact-container')]")
	public WebElement contactUsSection;

	@FindBy(xpath = "//form[contains(@class,'contact-form')]/div[2]/h2[1]")
	public WebElement contactUsTitle;

	// Blogs
	@FindBy(xpath = "//div[contains(@class,'hm-blog')]")
	public WebElement blogsSection;

	@FindBy(xpath = "//div[contains(@class,'hm-blog')]//h2[1]")
	public WebElement blogsTitle;

	@FindBy(xpath = "//*[@id='carouselCaptions']/div[1]/div/div[1]/div/h3")
	public List<WebElement> bannerFrontTitle;

	@FindBy(xpath = "//div[contains(@class,'hm-blog')]/div/div/div/div[2]/div")
	public List<WebElement> blogsListHomePage;

	@FindBy(xpath = "//div[contains(@class,'hm-blog')]/div/div/div/div[3]/a")
	public List<WebElement> blogsHomePageViewMore;

	@FindBy(xpath = "//div[contains(@class,'blog-listing-wrapper')]/div[1]/div[1]/div[1]/div[1]/div")
	public List<WebElement> blogsDetailPageCount;

	@FindBy(xpath = "//div[contains(@class,'blog-listing-wrapper')]/div[1]/div[1]/div[1]/div[1]/div[1]")
	public WebElement blogsDetailPageFirstBlog;

	@FindBy(xpath = "//input[@name='search']")
	public List<WebElement> blogsSearchTextBox;

	@FindBy(xpath = "//div[contains(@class,'blog-listing-wrapper')]//h2[1]")
	public List<WebElement> blogsDetailPageTitle;

	@FindBy(xpath = "//div[contains(@class,'blog-listing-wrapper')]/div[1]/div[1]/div[1]/div[3]//a[1]")
	public List<WebElement> blogsDetailPageBackToBlogListButtonList;

	@FindBy(xpath = "//div[contains(@class,'blog-listing-wrapper')]/div[1]/div[1]/div[1]/div[3]//a[1]")
	public WebElement blogsDetailPageBackToBlogListButton;

	@FindBy(xpath = "//div[contains(@class,'blog-listing-wrapper')]/div[1]/div[1]/div[1]/div[2]//div[contains(@class,'next')]/a[1]")
	public WebElement blogsDetailPageNextBlogLink;

	@FindBy(xpath = "//div[contains(@class,'blog-listing-wrapper')]/div[1]/div[1]/div[1]/div[2]//div[contains(@class,'next')]")
	public List<WebElement> blogsDetailPageNextBlogLinkList;

	@FindBy(xpath = "//div[contains(@class,'blog-listing-wrapper')]/div[1]/div[1]/div[1]/div[2]//div[contains(@class,'prev')]")
	public WebElement blogsDetailPagePrevBlogLink;

	@FindBy(xpath = "//div[contains(@class,'blog-listing-wrapper')]/div[1]/div[1]/div[1]/div[2]//div[contains(@class,'prev')]")
	public List<WebElement> blogsDetailPagePrevBlogLinkList;

	@FindBy(xpath = "//div[contains(@class,'social-share')]")
	public WebElement blogsDetailPageSocialMediaSection;

	@FindBy(xpath = "//div[contains(@class,'social-share')]")
	public List<WebElement> blogsDetailPageSocialMediaSectionList;

	@FindBy(xpath = "//div[contains(@class,'social-share')]/div[1]/div")
	public List<WebElement> blogsDetailPageSocialMediaLinks;

	@FindBy(xpath = "//*[@id='load_more_btn']")
	public WebElement blogsLoadMore;

	// Footer
	@FindBy(xpath = "/html/body/footer")
	public WebElement footerSection;

	@FindBy(xpath = "//footer/div/div/div/div/div[1]")
	public WebElement frontCopyRightText;

	@FindBy(xpath = "//footer/div/div/div/div/div[2]/div/ul/li[1]/a")
	public WebElement frontFooterHomeLink;

	@FindBy(xpath = "//footer/div/div/div/div/div[2]/div/ul/li[2]/a")
	public WebElement frontFooterSitemapLink;

	@FindBy(xpath = "//footer/div/div/div/div/div[3]")
	public WebElement frontFooterPowerdByText;

	@FindBy(xpath = "//footer/div/div/div/div/div[3]/a[1]")
	public WebElement frontFooterPowerdByLink;

	WebDriver driver;

	public CommonXpath(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

}
