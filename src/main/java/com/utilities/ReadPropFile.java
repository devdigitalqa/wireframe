package com.utilities;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.basicactions.LogHelper;

public class ReadPropFile {

	private static final Properties PROP = new Properties();
	Logger log = LogHelper.getLogger(ReadPropFile.class);

	public Properties readProp() {
		log.info("********************Initialization of properties file********************");
		try {
			FileInputStream fi = new FileInputStream(FilesPaths.CONFIG_PROPERTIES_FILE);
			getProp().load(fi);
			log.info("********************Loading of config properties file********************");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return getProp();
	}

	public Properties readProp(String propFilePath) {
		log.info("********************Initialization of properties file********************");
		try {
			FileInputStream fi = new FileInputStream(propFilePath);
			getProp().load(fi);
			log.info("********************Loading of PropFilePath properties file********************");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return getProp();
	}

	public static Properties getProp() {
		return PROP;
	}
}
