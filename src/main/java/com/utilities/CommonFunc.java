package com.utilities;

import static org.testng.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.base.Frame;
import com.basicactions.DropDownHelper;
import com.basicactions.ExcelHelper;
import com.basicactions.LogHelper;
import com.pages.adminpages.blogs.Blogs;
import com.pages.adminpages.categories.Categories;
import com.pages.adminpages.galleries.Galleries;
import com.pages.adminpages.homepagesliders.HomePageSliders;
import com.pages.adminpages.pages.Pages;
import com.pages.adminpages.partners.Partners;
import com.pages.adminpages.services.Services;
import com.pages.adminpages.users.Users;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

public class CommonFunc {

	WebDriver driver;
	CommonVariables commonVariables;
	CommonXpath commonXpath;
	DropDownHelper dropDownHelper;
	Users users;
	Categories categories;
	Services services;
	Pages pages;
	Blogs blogs;
	Galleries galleries;
	Partners partners;
	HomePageSliders homepagesliders;

	private Logger log = LogHelper.getLogger(CommonFunc.class);

	String testcaseText = "testcase";

	@FindBy(xpath = "//*[@id='add-btn']")
	WebElement addNewButton;

	@FindBy(xpath = "//*[@id='addimagebtn']")
	WebElement addNewGalleryButton;

	@FindBy(xpath = "//button[@value='save']")
	public WebElement saveButton;

	@FindBy(xpath = "//*[@id='DataTables_Table_0']/tbody/tr/td[9]/a")
	WebElement editButton;

	@FindBy(xpath = "/html/body/div[2]/div[3]/div/div[2]/section/form/div/table/tbody/tr[1]/td[2]/div/input")
	WebElement checkbox;

	@FindBy(xpath = "//*[@id='DataTables_Table_0']/tbody/tr[1]/td[2]/div")
	WebElement selectcheckbox;

	@FindBy(xpath = "//*[@id='confirm-action-submit']")
	WebElement confirmyesbutton;

	@FindBy(xpath = "//*[@id='action_btns']/div/a[3]")
	WebElement deletebutton;

	@FindBy(xpath = "//button[@value='savecontinue']")
	WebElement savecontinueButton;

	@FindBy(xpath = "//*[@id='frmaddedit']/div[1]/div[2]/div/div/a")
	WebElement cancelButton;

	@FindBy(xpath = "//table[@id='DataTables_Table_0']/tbody/tr[1]/td[1]/a[1]/span[1]")
	WebElement statuscolumn;

	@FindBy(xpath = "//*[@id='main']/div[1]/span")
	WebElement successmsg;

	@FindBy(xpath = "//*[@id='startall']")
	WebElement uploadButton;

	public CommonFunc(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		commonXpath = new CommonXpath(driver);
		dropDownHelper = new DropDownHelper(driver);
	}

	public void clickOnAddNewButton(String modulename) {
		log.info("********************Click on add new user button********************");
		if (modulename.equalsIgnoreCase(CommonVariables.HOME_PAGE_SLIDERS)
				|| (modulename.contains(Frame.commonVariables.getPageContentTitle())
						&& !Frame.commonVariables.getPageContentTitle().isEmpty())
				|| modulename.equalsIgnoreCase(CommonVariables.GALLERIES)) {
			commonXpath.homeSliderAddButton.click();
		} else {
			addNewButton.click();
		}
	}

	public void checkElementAvailable(List<WebElement> xpathListElement) {
		int tmp = 1;
		long t = System.currentTimeMillis();
		long end = t + 40000;

		do {
			if (System.currentTimeMillis() > end) {
				log.info("timeout");
			}

			int size = xpathListElement.size();
			if (size > 0) {
				tmp = 0;
			} else {
				tmp = 1;
			}

		} while (tmp == 1);
	}

	public void checkElementAvailableWithAttributeCompare(List<WebElement> xpathListElement, WebElement xpath,
			String attributeName, String attributeValue) {
		int tmp = 1;
		long t = System.currentTimeMillis();
		long end = t + 40000;

		do {
			if (System.currentTimeMillis() > end) {
				log.info("timeout");
			}

			int size = xpathListElement.size();
			if (size > 0 && xpath.getAttribute(attributeName).equals(attributeValue)) {
				tmp = 0;
			} else {
				tmp = 1;
			}

		} while (tmp == 1);
	}

	public void clickMenuOption(String menuTitle) {
		checkElementAvailableWithAttributeCompare(commonXpath.elementList, commonXpath.element, CommonVariables.STYLE,
				CommonVariables.DISPLAY_NONE);

		if (menuTitle.equalsIgnoreCase("my profile")) {
			commonXpath.profileMainMenu.click();
			commonXpath.myProfileMenu.click();
		} else if (menuTitle.equalsIgnoreCase("change password")) {
			commonXpath.profileMainMenu.click();
			commonXpath.changePasswordMenu.click();

		} else {
			List<WebElement> menu = driver.findElements(By.xpath("//*[@id='main']/div[2]/div/div/div/a"));

			int count = menu.size();
			for (int i = 1; i <= count; i++) {
				String m1 = driver.findElement(By.xpath("//*[@id='main']/div[2]/div/div/div[" + i + "]/a")).getText();
				if (m1.equals(menuTitle)) {
					driver.findElement(By.xpath("//*[@id='main']/div[2]/div/div/div[" + i + "]/a")).click();
					break;
				}
			}
		}
	}

	public void verifythesheetname(String moduleName) {

		if (moduleName.equals(CommonVariables.USERS)) {
			ExcelHelper.readDataFromXLS(FilesPaths.TEMPLATE9WIREFRAME, CommonVariables.USERS);
		} else if (moduleName.equals(CommonVariables.BLOGS)) {
			ExcelHelper.readDataFromXLS(FilesPaths.TEMPLATE9WIREFRAME, CommonVariables.BLOGS);
		} else if (moduleName.equals(CommonVariables.CATEGORIES)) {
			ExcelHelper.readDataFromXLS(FilesPaths.TEMPLATE9WIREFRAME, CommonVariables.CATEGORIES);
		} else if (moduleName.equals(CommonVariables.SERVICES)) {
			ExcelHelper.readDataFromXLS(FilesPaths.TEMPLATE9WIREFRAME, CommonVariables.SERVICES);
		} else if (moduleName.equals(Frame.commonVariables.getPageContentTitle())) {
			ExcelHelper.readDataFromXLS(FilesPaths.TEMPLATE9WIREFRAME, CommonVariables.PARTNERS);
		} else if (moduleName.equals(CommonVariables.PAGES)) {
			ExcelHelper.readDataFromXLS(FilesPaths.TEMPLATE9WIREFRAME, CommonVariables.PAGES);
		} else if (moduleName.equals(CommonVariables.IP_TRACKER)) {
			ExcelHelper.readDataFromXLS(FilesPaths.TEMPLATE9WIREFRAME, CommonVariables.IP_TRACKER);
		} else if (moduleName.equals(CommonVariables.SETTINGS)) {
			ExcelHelper.readDataFromXLS(FilesPaths.TEMPLATE9WIREFRAME, CommonVariables.SETTINGS);
		} else if (moduleName.equals(CommonVariables.CONTACTUS)) {
			ExcelHelper.readDataFromXLS(FilesPaths.TEMPLATE9WIREFRAME, CommonVariables.CONTACTUS);
		} else if (moduleName.equals(CommonVariables.GALLERIES)) {
			ExcelHelper.readDataFromXLS(FilesPaths.TEMPLATE9WIREFRAME, CommonVariables.GALLERIES);
		} else if (moduleName.equals(CommonVariables.HOME_PAGE_SLIDERS)) {
			ExcelHelper.readDataFromXLS(FilesPaths.TEMPLATE9WIREFRAME, CommonVariables.HOME_PAGE_SLIDERS);
		} else if (moduleName.equalsIgnoreCase(CommonVariables.CHANGEPWD)
				|| moduleName.equalsIgnoreCase("About Us Settings") || moduleName.equalsIgnoreCase("Front Menus")
				|| moduleName.equalsIgnoreCase("my profile")) {
			assert true;
		} else {
			assertTrue(false);
		}
	}

	public void searchRecord(String searchText, String verifyTableText, String xpath, String moduleName,
			String testingData) {
		checkElementAvailableWithAttributeCompare(commonXpath.elementList, commonXpath.element, CommonVariables.STYLE,
				CommonVariables.DISPLAY_NONE);

		driver.findElement(By.xpath("//*[@id='search-btn']")).click();

		commonXpath.searchTextbox.clear();
		commonXpath.searchTextbox.sendKeys(searchText);
		driver.findElement(By.xpath("//*[@id='btnsearch']")).click();

		checkElementAvailableWithAttributeCompare(commonXpath.elementList, commonXpath.element, CommonVariables.STYLE,
				CommonVariables.DISPLAY_NONE);

		if (testingData.isEmpty()) {
			if (Frame.commonVariables.isDeleteRecord()) {
				assertTrue(moduleName.equals(CommonVariables.USERS) || moduleName.equals(CommonVariables.BLOGS)
						|| moduleName.equals(CommonVariables.CATEGORIES) || moduleName.equals(CommonVariables.PAGES)
						|| moduleName.equals(CommonVariables.SERVICES));
			} else {
				searchNotTestingData(verifyTableText, xpath, moduleName);
			}
		} else {
			List<WebElement> eleNotFoundText = driver.findElements(By.xpath("//*[@id='frmlist']/table/tbody/tr/td"));
			int size = eleNotFoundText.size();
			Frame.commonVariables.setTestingData(size <= 0);
		}
	}

	public void searchNotTestingData(String verifyTableText, String xpath, String moduleName) {
		if (driver.findElement(By.xpath(xpath)).getText().trim().equalsIgnoreCase(verifyTableText.trim())) {
			if (moduleName.equals(CommonVariables.BLOGS) && !Frame.commonVariables.isDeleteRecord()) {
				blogs = new Blogs(driver);
				Frame.commonVariables.setPublishDate(blogs.getPublishdate());
			}
			assertTrue(true);
		} else {
			if (!moduleName.equals(CommonVariables.PAGES)) {
				assertTrue(false);
			}
		}
	}

	public void checkSuccessMessage(String inactiveMsg, String activeMsg, String moduleName, WebElement statuscolumn,
			WebElement successmsg) {

		String msg = "";

		String msg2 = "";

		if (Frame.commonVariables.getInactive().equals("false")) {
			msg = inactiveMsg;
		} else if (Frame.commonVariables.getInactive().equals("true")) {
			msg2 = activeMsg;
		} else {
			log.error(moduleName + " Message is not match");
			assertTrue(false);
		}

		statuscolumn.click();

		checkElementAvailableWithAttributeCompare(commonXpath.elementList, commonXpath.element, CommonVariables.STYLE,
				CommonVariables.DISPLAY_NONE);

		if (successmsg.getText().equals(msg)) {
			log.info("Messgae for active = " + moduleName + " > " + msg);
		} else if (successmsg.getText().equals(msg2)) {
			log.info("Messgae for inactive = " + moduleName + " > " + msg2);
		} else {
			log.error(moduleName + " Message is not match:");
			assertTrue(false);
		}
	}

	public void clickOnEditButton(String moduleName) throws InterruptedException {
		log.info("********************Click on edit button********************");
		Thread.sleep(2000);
		if (moduleName.equals(CommonVariables.USERS)) {
			users = new Users(driver);
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].click();", editButton);
		} else if (moduleName.equals(CommonVariables.BLOGS)) {
			blogs = new Blogs(driver);
			blogs.clickonEditbutton();
		} else if (moduleName.equals(CommonVariables.CATEGORIES)) {
			categories = new Categories(driver);
			categories.clickonEditbutton();
		} else if (moduleName.equals(CommonVariables.SERVICES)) {
			services = new Services(driver);
			services.clickonEditbutton();
		} else if (moduleName.equals(CommonVariables.PAGES)) {
			pages = new Pages(driver);
			pages.clickonEditbutton();
		} else if (moduleName.equals(CommonVariables.HOME_PAGE_SLIDERS)) {
			homepagesliders = new HomePageSliders(driver);
			homepagesliders.clickonEditbutton();
		} else if (moduleName.equals(CommonVariables.GALLERIES)) {
			galleries = new Galleries(driver);
			galleries.clickonEditbutton();
		} else if (moduleName.contains(Frame.commonVariables.getPageContentTitle())) {
			partners = new Partners(driver);
			partners.clickonEditbutton();
		} else {
			assertTrue(false);
		}

	}

	public void clickJSButton(WebElement ele) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", ele);
	}

	public void clickOnUploadButton() {
		uploadButton.click();
		checkElementAvailableWithAttributeCompare(commonXpath.elementList, commonXpath.element, CommonVariables.STYLE,
				CommonVariables.DISPLAY_NONE);

		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView();", driver.findElement(By.xpath("//*[@id='normal_btns']")));

	}

	public void scroll(WebElement ele) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView();", ele);

	}

	public void clickOnselectcheckbox() throws InterruptedException {
		log.info("********************Click on select checkbox button********************");
		selectcheckbox.click();
		Thread.sleep(1500);
	}

	public void clickOnDeleteButton() throws InterruptedException {
		log.info("********************Click on delete button********************");
		deletebutton.click();
		Thread.sleep(1500);
	}

	public void clickOnconfirmyesbutton() {
		log.info("********************Click on confirm yes button********************");
		confirmyesbutton.click();
	}

	public void clickOnCancelbutton() {
		log.info("********************Click on confirm yes button********************");
		cancelButton.click();
	}

	public void clickOnsavecontinuebutton() {
		log.info("********************Click on save and continue button********************");
		savecontinueButton.click();
		checkElementAvailableWithAttributeCompare(commonXpath.elementList, commonXpath.element, CommonVariables.STYLE,
				CommonVariables.DISPLAY_NONE);
	}

	public void clickOnSave(String moduleName) {
		log.info("********************Click on " + moduleName + " submit button********************");
		saveButton.click();
		checkElementAvailableWithAttributeCompare(commonXpath.elementList, commonXpath.element, CommonVariables.STYLE,
				CommonVariables.DISPLAY_NONE);
	}

	public void verifyTestEmailData(String excelSheetName, String xpathTextbox, String xpathErrorMessage)
			throws InterruptedException {
		ExcelHelper.readDataFromXLS(FilesPaths.TEMPLATE9WIREFRAME, excelSheetName);

		for (int i = 0; i < ExcelHelper.getTotalRowsCount(); i++) {
			driver.findElement(By.xpath(xpathTextbox)).clear();
			driver.findElement(By.xpath(xpathTextbox)).sendKeys(ExcelHelper.getData(0, i));
			driver.findElement(By.xpath(xpathTextbox)).sendKeys(Keys.TAB);
			Thread.sleep(1500);

			if (driver.findElement(By.xpath(xpathErrorMessage)).getText().isEmpty()) {
				String msgtext = "Error not show";
				if (msgtext.equals(ExcelHelper.getData(1, i))) {
					assert true;
				}
			} else {
				String text = "Error show";

				if (text.equals(ExcelHelper.getData(1, i))) {
					String emailError = driver.findElement(By.xpath(xpathErrorMessage)).getText();
					if (emailError.contains(ExcelHelper.getData(2, i))) {
						assert true;
					}
				} else {
					assertTrue(false);
				}
			}
		}
	}

	public void clickOnstatuscolumn(String moduleName) throws InterruptedException {
		log.info("********************Click on status********************");
		Thread.sleep(2000);
		String msg = "";
		String msg2 = "";
		Thread.sleep(3000);

		if (Frame.commonVariables.getInactive().equals("false")) {
			msg = "The " + moduleName + " successfully inactivated.";
		} else if (Frame.commonVariables.getInactive().equals("true")) {
			msg2 = "The " + moduleName + " successfully activated.";
		} else {
			log.error("Inactive variable val = " + Frame.commonVariables.getInactive());
			assertTrue(false);
		}

		statuscolumn.click();
		Thread.sleep(3000);
		log.error("Actual Message 1 = " + msg);
		log.error("Actual Message 2 = " + msg2);
		log.error("Expected Message = " + successmsg.getText());

		if (successmsg.getText().equals(msg)) {
			log.info("Messgae for inactive  = " + successmsg.getText());
		} else if (successmsg.getText().equals(msg2)) {
			log.info("Messgae for active  = " + successmsg.getText());
		} else {
			assertTrue(false);
		}
	}

	public void verifyTestAllData(String excelSheetName, String xpathTextbox, String xpathErrorMessage)
			throws InterruptedException {
		ExcelHelper.readDataFromXLS(FilesPaths.TEMPLATE9WIREFRAME, excelSheetName);

		for (int i = 0; i < 1; i++) {
			driver.findElement(By.xpath(xpathTextbox)).clear();
			driver.findElement(By.xpath(xpathTextbox)).sendKeys(ExcelHelper.getData(0, i));
			driver.findElement(By.xpath(xpathTextbox)).sendKeys(Keys.TAB);
			Thread.sleep(1500);

			if (driver.findElement(By.xpath(xpathErrorMessage)).getText().isEmpty()) {
				String msgtext = "Error not show";
				if (msgtext.equals(ExcelHelper.getData(1, i))) {
					assert true;
				}
			} else {
				assertTrue(false);
			}
		}
	}

	public void validationField(String validationType, String fieldName, WebElement xpathTextbox,
			WebElement xpathErrorMessage, String errorMessage) throws InterruptedException {
		switch (validationType) {
		case "Required Validation":
			if (fieldName.contains("textbox")) {
				xpathTextbox.clear();
				xpathTextbox.sendKeys(Keys.TAB);
			}
			Thread.sleep(1500);
			break;

		case "Email Validation":
			xpathTextbox.clear();
			xpathTextbox.sendKeys("abc");
			xpathTextbox.sendKeys(Keys.TAB);
			Thread.sleep(1500);
			break;

		case "Number Validation":
			xpathTextbox.clear();
			xpathTextbox.sendKeys("abc");
			Thread.sleep(1500);
			xpathTextbox.sendKeys(Keys.TAB);
			Thread.sleep(1500);
			break;

		default:
			break;
		}

		String errorMessageCMS = xpathErrorMessage.getText().trim();
		if (errorMessageCMS.isEmpty()) {
			assertTrue(false);
		} else {
			Assert.assertEquals(errorMessageCMS, errorMessage);
		}
	}

	public void verifytablegridData(String excelValue, String moduleName) {

		List<WebElement> ele = driver.findElements(By.xpath("//*[@id='DataTables_Table_0']/thead/tr/th"));
		int count = ele.size();
		int j = 0;

		String[] values = excelValue.split(",");
		int i = 0;
		if (moduleName.equals("IP Tracker")) {
			i = 2;
		} else {
			i = 4;
		}

		for (; i <= count; i++) {
			String val1 = driver.findElement(By.xpath("//*[@id='DataTables_Table_0']/thead/tr/th[" + i + "]")).getText()
					.trim();
			String val2 = values[j].trim();
			if (val1.equals(val2)) {
				assertTrue(true);
			} else {
				log.error("Actual Text = " + val1);
				log.error("Expected Text = " + val2);
				assertTrue(false);
			}
			j++;
		}
	}

	public void verifypaginationcount() {
		List<WebElement> numofrecord = driver.findElements(By.xpath(" //*[@id='DataTables_Table_0']/tbody/tr/td[4]"));
		int rcount = numofrecord.size();

		Select select = new Select(driver.findElement(By.xpath("//*[@id='DataTables_Table_0_length']/label/select")));
		WebElement option = select.getFirstSelectedOption();
		String defaultItem = option.getText();

		if (defaultItem.equals(Frame.commonVariables.numRecord)) {
			if (Integer.parseInt(defaultItem) >= rcount) {
				assertTrue(true);
			}
		} else {
			log.error("Actual Text = " + defaultItem);
			log.error("Expected Text = " + Frame.commonVariables.numRecord);
			assertTrue(false);
		}
	}

	public void urlGet() throws org.json.simple.parser.ParseException {
		String[] credentials = null;
		JSONParser jsonParser = new JSONParser();
		String casesText = "cases";
		try (FileReader reader = new FileReader(FilesPaths.JSON_DATA_LARAVEL_FILE)) {
			// Read JSON file
			Object obj = jsonParser.parse(reader);

			JSONObject jobj = new JSONObject(obj.toString());
			int numOfTestCase = jobj.getJSONArray(casesText).length();
			credentials = new String[numOfTestCase];
			JSONObject jb;
			for (int i = 0; i < jobj.getJSONArray(casesText).length(); i++) {
				jb = (JSONObject) jobj.getJSONArray(casesText).get(i);
				if (jb.has(testcaseText)) {
					credentials[i] = jb.getString(testcaseText);
				}
			}

			List<String> list = Arrays.asList(credentials);
			jb = (JSONObject) jobj.getJSONArray(casesText).get(list.indexOf("stageURL"));

			if (Frame.commonVariables.getFrontURL().isEmpty()) {
				Frame.commonVariables.setFrontURL(jb.has("frontURL") ? jb.getString("frontURL") : "nofrontURL");
			}

			if (Frame.commonVariables.getAdminURL().isEmpty()) {
				Frame.commonVariables.setAdminURL(jb.has("adminURL") ? jb.getString("adminURL") : "noadminURL");
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void credentialGet() throws org.json.simple.parser.ParseException {
		String[] credentials = null;
		JSONParser jsonParser = new JSONParser();
		String credentialsText = "credentials";

		try (FileReader reader = new FileReader(FilesPaths.JSON_DATA_CREDENTIALS_FILE)) {
			// Read JSON file
			Object obj = jsonParser.parse(reader);

			JSONObject jobj = new JSONObject(obj.toString());
			int numOfTestCase = jobj.getJSONArray(credentialsText).length();
			credentials = new String[numOfTestCase];
			JSONObject jb;
			for (int i = 0; i < jobj.getJSONArray(credentialsText).length(); i++) {
				jb = (JSONObject) jobj.getJSONArray(credentialsText).get(i);
				if (jb.has(testcaseText)) {
					credentials[i] = jb.getString(testcaseText);
				}
			}
			List<String> list = Arrays.asList(credentials);
			jb = (JSONObject) jobj.getJSONArray(credentialsText).get(list.indexOf("stageURL"));

			String loginPwd = "loginPwd";
			String loginEmail = "loginEmail";

			if (Frame.commonVariables.loginEmail.isEmpty()) {
				Frame.commonVariables.loginEmail = jb.has(loginEmail) ? jb.getString(loginEmail) : "nologinEmail";
			}
			
			if (Frame.commonVariables.loginPwd.isEmpty()) {
				Frame.commonVariables.loginPwd = jb.has(loginPwd) ? jb.getString(loginPwd) : "nologinPwd";
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void clickFrontMenuOption(String menuTitle) {

		List<WebElement> menu = driver.findElements(By.xpath("//*[@id='navbarNavAltMarkup']/ul/li/a"));
		int count = menu.size();
		for (int i = 1; i <= count; i++) {
			String m1 = driver.findElement(By.xpath("//*[@id='navbarNavAltMarkup']/ul/li[" + i + "]/a")).getText();
			if (m1.equalsIgnoreCase(menuTitle)) {
				driver.findElement(By.xpath("//*[@id='navbarNavAltMarkup']/ul/li[" + i + "]/a")).click();
				break;
			}
		}
	}

	public void settingsBackup(String accordion, String fieldName, String fieldValue)
			throws IOException, WriteException {
		File outputWorkbook;
		WritableWorkbook workbook1;
		WritableSheet sheet1;
		outputWorkbook = new File(FilesPaths.TEMPLATE9SETTINGSBACKUP);

		// BugTracking excel header content

		String[] headerContent = { "Accordion", "Field Name", "Field Value" };

		if (!outputWorkbook.exists()) {
			workbook1 = Workbook.createWorkbook(outputWorkbook);
			sheet1 = workbook1.createSheet("Template9Settings", 0);

			for (int headerContentData = 0; headerContentData < headerContent.length; headerContentData++) {
				Label label = new Label(headerContentData, 0, headerContent[headerContentData]);
				sheet1.addCell(label);
			}

			workbook1.write();
			workbook1.close();
		}

		FileInputStream fsIP = new FileInputStream(new File(FilesPaths.TEMPLATE9SETTINGSBACKUP));
		HSSFWorkbook wb = new HSSFWorkbook(fsIP);
		HSSFSheet worksheet = wb.getSheetAt(0);

		int rowCount = worksheet.getLastRowNum() - worksheet.getFirstRowNum();
		Row row = worksheet.getRow(0);

		// Create a loop over the cell of newly created Row

		Row newRow = worksheet.createRow(rowCount + 1);

		String[] data = { accordion, fieldName, fieldValue };

		for (int j = 0; j < row.getLastCellNum(); j++) {
			Cell cell = newRow.createCell(j);
			cell.setCellValue(data[j]);
		}

		fsIP.close();

		FileOutputStream outputStream = new FileOutputStream(FilesPaths.TEMPLATE9SETTINGSBACKUP);
		wb.write(outputStream);
		outputStream.close();
	}
}
