package com.runners;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import com.base.TestBase;
import com.basicactions.LogHelper;
import com.basicactions.WaitHelper;
import com.utilities.ReadPropFile;
import com.utilities.TimeOutWait;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;

public class HookHelper {

	WebDriver driver;
	ReadPropFile readPropFile;
	WaitHelper waitHelper;
	Logger log = LogHelper.getLogger(HookHelper.class);

	@Before
	public void setUp() {
		readPropFile = new ReadPropFile();
		readPropFile.readProp();

		log.info("Open " + ReadPropFile.getProp().getProperty("browser") + " browser from reading properties file");
		driver = TestBase.launchBrowser(ReadPropFile.getProp().getProperty("browser"));
		log.info("********************Delete all cookies of browser********************");
		driver.manage().deleteAllCookies();
		driver.manage().window().maximize();
		waitHelper = new WaitHelper(driver);
		waitHelper.pageLoadTime(TimeOutWait.PAGELOAD_TIMEOUT, TimeUnit.SECONDS);
		waitHelper.setImplicitWait(TimeOutWait.IMPLICIT_TIMEOUT, TimeUnit.SECONDS);
	}

	@After
	public void tearDown(Scenario scenario) {
		if (scenario.isFailed()) {
			log.info("********************Take a screenshot of failed scenario********************");
			final byte[] screenshot = ((TakesScreenshot) TestBase.getDriver()).getScreenshotAs(OutputType.BYTES);
			scenario.embed(screenshot, "image/png", "Fail Image"); // ... and embed it in the report.
		}
		log.info("********************Closing the browser********************");
		TestBase.getDriver().close();
		TestBase.getDriver().quit();
	}
}
