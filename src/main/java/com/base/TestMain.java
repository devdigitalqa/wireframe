package com.base;

import java.util.ArrayList;
import java.util.List;

import org.testng.TestNG;

public class TestMain {
	TestNG runner;
	List<String> suitefiles;

	public void getAndVerifyTestNgSuiteName() {
		runner = new TestNG();
		suitefiles = new ArrayList<>();

		// Users
		getUserAE();

		getUserStatus();

		getUserDelete();

		getUserAESC();

		// Pages
		getPageAE();
		getPageStatus();
		getPageAESC();

		// Categories
		getCategoryAE();

		getCategoryStatus();

		getCategoryDelete();

		getCategoryAESC();

		getCategoryDelete();

		// Services
		getServicesAddEditSaveButton();
		getServicesActiveInactive();
		getServicesDelete();
		getServicesAddEditSaveContinue();
		getServicesDelete();

		// Partners

		if (FrameVal.getPartnersAddEditSaveButton()) {
			suitefiles.add("Suites/Partners/PartnersAE.xml");
		}

		if (FrameVal.getPartnersActiveInactive()) {
			suitefiles.add("Suites/Partners/PartnersAI.xml");
		}

		if (FrameVal.getPartnersDelete()) {
			suitefiles.add("Suites/Partners/PartnersDelete.xml");
		}

		if (FrameVal.getPartnersAddEditSaveContinue()) {
			suitefiles.add("Suites/Partners/PartnersAESaveContinue.xml");
		}

		// Galleries

		getGalleryAE();
		getGalleryStatus();
		getGalleryDelete();
		getGalleryAESC();
		getGalleryDelete();

		// Home Page Sliders

		getHomePageSliderAE();
		getHomePageSliderStatus();
		getHomePageSliderDelete();
		getHomePageSliderAESC();
		getHomePageSliderDelete();

		// Blogs
		getBlogAES();
		getBlogStatus();
		getBlogFront();
		getBlogDelete();
		getBlogAESC();

		getSettings();

		// Delete All
		getUserTestingDataDelete();

		getCategoryTestingDataDelete();

		getBlogTestingDataDelete();

		getServiceTestingDataDelete();

		getContactInquiries();

		getIPTracker();

		getFrontMenus();

		frontCheck();

		runner.setTestSuites(suitefiles);
		runner.run();
	}

	// Users
	public void getUserAE() {
		if (FrameVal.getUserAE()) {
			suitefiles.add("Suites/Users/UserAE.xml");
		}
	}

	public void getUserStatus() {
		if (FrameVal.getUserStatus()) {
			suitefiles.add("Suites/Users/UserAI.xml");
		}
	}

	public void getUserDelete() {
		if (FrameVal.getUserDelete()) {
			suitefiles.add("Suites/Users/UserDelete.xml");
		}
	}

	public void getUserAESC() {
		if (FrameVal.getUserAESC()) {
			suitefiles.add("Suites/Users/UserAESC.xml");
		}
	}

	// Categories
	public void getCategoryAE() {
		if (FrameVal.getCategoryAE()) {
			suitefiles.add("Suites/Categories/CategoryAE.xml");
		}
	}

	public void getCategoryStatus() {
		if (FrameVal.getCategoryStatus()) {
			suitefiles.add("Suites/Categories/CategoryStatus.xml");
		}
	}

	public void getCategoryDelete() {
		if (FrameVal.getCategoryDelete()) {
			suitefiles.add("Suites/Categories/CategoryDelete.xml");
		}
	}

	public void getCategoryAESC() {
		if (FrameVal.getCategoryAESC()) {
			suitefiles.add("Suites/Categories/CategoryAESC.xml");
		}
	}

	// Galleries
	public void getGalleryAE() {
		if (FrameVal.getGalleryAE()) {
			suitefiles.add("Suites/Galleries/GalleryAE.xml");
		}
	}

	public void getGalleryStatus() {
		if (FrameVal.getGalleryStatus()) {
			suitefiles.add("Suites/Galleries/GalleryStatus.xml");
		}
	}

	public void getGalleryDelete() {
		if (FrameVal.getGalleryDelete()) {
			suitefiles.add("Suites/Galleries/GalleryDelete.xml");
		}
	}

	public void getGalleryAESC() {
		if (FrameVal.getGalleryAESC()) {
			suitefiles.add("Suites/Galleries/GalleryAESC.xml");
		}
	}

	// Home Page Sliders
	public void getHomePageSliderAE() {
		if (FrameVal.getHomePageSliderAE()) {
			suitefiles.add("Suites/HomePageSliders/HomePageSliderAE.xml");
		}
	}

	public void getHomePageSliderStatus() {
		if (FrameVal.getHomePageSliderStatus()) {
			suitefiles.add("Suites/HomePageSliders/HomePageSliderStatus.xml");
		}
	}

	public void getHomePageSliderDelete() {
		if (FrameVal.getHomePageSliderDelete()) {
			suitefiles.add("Suites/HomePageSliders/HomePageSliderDelete.xml");
		}
	}

	public void getHomePageSliderAESC() {
		if (FrameVal.getHomePageSliderAESC()) {
			suitefiles.add("Suites/HomePageSliders/HomePageSliderAESC.xml");
		}
	}

	// Blogs
	public void getBlogAES() {
		if (FrameVal.getBlogAddEditSaveButton()) {
			suitefiles.add("Suites/Blogs/BlogAE.xml");
		}
	}

	public void getBlogStatus() {
		if (FrameVal.getBlogStatus()) {
			suitefiles.add("Suites/Blogs/BlogAI.xml");
		}
	}

	public void getBlogFront() {
		if (FrameVal.getBlogFront()) {
			suitefiles.add("Suites/Blogs/BlogFront.xml");
		}
	}

	public void getBlogDelete() {
		if (FrameVal.getBlogDelete()) {
			suitefiles.add("Suites/Blogs/BlogDelete.xml");
		}
	}

	public void getBlogAESC() {
		if (FrameVal.getBlogAESC()) {
			suitefiles.add("Suites/Blogs/BlogAESC.xml");
		}
	}

	public void getBlogSettings() {
		if (FrameVal.getBlogSettings()) {
			suitefiles.add("Suites/Blogs/BlogSettings.xml");
		}
	}

	public void getSettings() {
		if (FrameVal.getSettings()) {
			suitefiles.add("Suites/Settings.xml");
		}
	}

	public void frontCheck() {
		getHeader();

		getBannerSlider();

		getAboutUsHomePage();
		getAboutUsDetailPage();

		getServicesHomePage();
		getServicesDetailPage();

		getGalleryHomePage();
		getGalleryDetailPage();

		getPartnersHomePage();

		getBlogsHomePage();
		getBlogsDetailPage();

		getContactUsHomePage();
		getContactUsDetailPage();

		getFrontFooter();
	}

	public void getHeader() {
		if (FrameValFront.getHeader()) {
			suitefiles.add("Suites/Front/Header.xml");
		}
	}

	public void getBannerSlider() {
		if (FrameValFront.getBannerSlider()) {
			suitefiles.add("Suites/Front/BannerSlider.xml");
		}
	}

	public void getAboutUsHomePage() {
		if (FrameValFront.getAboutUsHomePage()) {
			suitefiles.add("Suites/Front/AboutUsHomePage.xml");
		}
	}

	public void getAboutUsDetailPage() {
		if (FrameValFront.getAboutUsDetailPage()) {
			suitefiles.add("Suites/Front/AboutUsDetailPage.xml");
		}
	}

	public void getServicesHomePage() {
		if (FrameValFront.getServicesHomePage()) {
			suitefiles.add("Suites/Front/ServicesHomePage.xml");
		}
	}

	public void getServicesDetailPage() {
		if (FrameValFront.getServicesDetailPage()) {
			suitefiles.add("Suites/Front/ServicesDetailPage.xml");
		}
	}

	public void getGalleryHomePage() {
		if (FrameValFront.getGalleryHomePage()) {
			suitefiles.add("Suites/Front/GalleryHomePage.xml");
		}
	}

	public void getGalleryDetailPage() {
		if (FrameValFront.getGalleryDetailPage()) {
			suitefiles.add("Suites/Front/GalleryDetailPage.xml");
		}
	}

	public void getPartnersHomePage() {
		if (FrameValFront.getPartnersHomePage()) {
			suitefiles.add("Suites/Front/PartnersHomePage.xml");
		}
	}

	public void getBlogsHomePage() {
		if (FrameValFront.getBlogsHomePage()) {
			suitefiles.add("Suites/Front/BlogsHomePage.xml");
		}
	}

	public void getBlogsDetailPage() {
		if (FrameValFront.getBlogsDetailPage()) {
			suitefiles.add("Suites/Front/BlogsDetailPage.xml");
		}
	}

	public void getContactUsHomePage() {
		if (FrameValFront.getContactUsHomePage()) {
			suitefiles.add("Suites/Front/ContactUsHomePage.xml");
		}
	}

	public void getContactUsDetailPage() {
		if (FrameValFront.getContactUsDetailPage()) {
			suitefiles.add("Suites/Front/ContactUsDetailPage.xml");
		}
	}

	public void getFrontFooter() {
		if (FrameValFront.getFrontFooter()) {
			suitefiles.add("Suites/Front/Footer.xml");
		}
	}

	// Pages

	public void getPageAE() {
		if (FrameVal.getPageAE()) {
			suitefiles.add("Suites/Pages/PageAE.xml");
		}
	}

	public void getPageStatus() {
		if (FrameVal.getPageStatus()) {
			suitefiles.add("Suites/Pages/PageAI.xml");
		}
	}

	public void getPageAESC() {
		if (FrameVal.getPageAESC()) {
			suitefiles.add("Suites/Pages/PageAESC.xml");
		}
	}

	// Services

	public void getServicesAddEditSaveButton() {
		if (FrameVal.getServicesAddEditSaveButton()) {
			suitefiles.add("Suites/Services/ServicesAE.xml");
		}
	}

	public void getServicesActiveInactive() {
		if (FrameVal.getServicesActiveInactive()) {
			suitefiles.add("Suites/Services/ServicesAI.xml");
		}
	}

	public void getServicesDelete() {
		if (FrameVal.getServicesDelete()) {
			suitefiles.add("Suites/Services/ServicesDelete.xml");
		}
	}

	public void getServicesAddEditSaveContinue() {
		if (FrameVal.getServicesAddEditSaveContinue()) {
			suitefiles.add("Suites/Services/ServicesAESaveContinue.xml");
		}
	}

	// Delete All
	public void getUserTestingDataDelete() {
		if (FrameVal.getUserTestingDataDelete()) {
			suitefiles.add("Suites/Testing Data/UsersTestingDataDelete.xml");
		}
	}

	public void getCategoryTestingDataDelete() {
		if (FrameVal.getCategoryTestingDataDelete()) {
			suitefiles.add("Suites/Testing Data/CategoriesTestingDataDelete.xml");
		}
	}

	public void getBlogTestingDataDelete() {
		if (FrameVal.getBlogTestingDataDelete()) {
			suitefiles.add("Suites/Testing Data/BlogsTestingDataDelete.xml");
		}
	}

	public void getServiceTestingDataDelete() {
		if (FrameVal.getServiceTestingDataDelete()) {
			suitefiles.add("Suites/Testing Data/ServicesTestingDataDelete.xml");
		}
	}

	public void getContactInquiries() {
		if (FrameVal.getContactInquiries()) {
			suitefiles.add("Suites/ContactInquiries.xml");
		}
	}

	// IP Tracker

	public void getIPTracker() {
		if (FrameVal.getIPTracker()) {
			suitefiles.add("Suites/IPTracker.xml");
		}
	}

	public void getFrontMenus() {
		if (FrameVal.getFrontMenus()) {
			suitefiles.add("Suites/FrontMenus.xml");
		}
	}
}
