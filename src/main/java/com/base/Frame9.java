package com.base;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextPane;

public class Frame9 {

	JFrame frameWindow;
	static Frame9 mainWindow;
	JButton btnExecution;
	JCheckBox chkFrontSelectAll;
	JCheckBox chkFrontHeader;
	JCheckBox chkFrontBannerSlider;
	JCheckBox chkFrontAboutUsHomePage;
	JCheckBox chkFrontAboutUsDetailPage;
	JCheckBox chkFrontServicesHomePage;

	JCheckBox chkFrontServicesDetailPage;
	JCheckBox chkFrontGalleryHomePage;
	JCheckBox chkFrontGalleryDetailPage;

	JCheckBox chkFrontPartnersHomePage;
	JCheckBox chkFrontBlogsHomePage;
	JCheckBox chkFrontBlogsDetailPage;
	JCheckBox chkFrontContactUsHomePage;
	JCheckBox chkFrontContactUsDetailPage;
	JCheckBox chkFrontFooter;

	JCheckBox chkUserAES;
	JCheckBox chkUserStatus;
	JCheckBox chkUserDelete1;
	JCheckBox chkUserAESC;
	JCheckBox chkUserDelete2;
	JCheckBox chkUserPagination;
	JCheckBox chkUsersorting;

	JCheckBox chkPageAES;
	JCheckBox chkPageStatus;
	JCheckBox chkPageDelete1;
	JCheckBox chkPageAESC;
	JCheckBox chkPageDelete2;

	JCheckBox chkCategoryAE;
	JCheckBox chkCategoryStatus;
	JCheckBox chkCategoryDelete1;
	JCheckBox chkCategoryAESC;
	JCheckBox chkCategoryDelete2;

	JCheckBox chkBlogAddEdit;
	JCheckBox chkBlogStatus;
	JCheckBox chkBlogFront;
	JCheckBox chkBlogDelete1;
	JCheckBox chkBlogAESContinue;
	JCheckBox chkBlogDelete2;
	JCheckBox chkBlogSettings;

	JCheckBox chkHomePageSliderAE;
	JCheckBox chkHomePageSliderStatus;
	JCheckBox chkHomePageSliderDelete1;
	JCheckBox chkHomePageSliderAESC;
	JCheckBox chkHomePageSliderDelete2;

	JCheckBox chkGalleryAE;
	JCheckBox chkGalleryStatus;
	JCheckBox chkGalleryDelete1;
	JCheckBox chkGalleryAESC;
	JCheckBox chkGalleryDelete2;

	JCheckBox chkServicesAE;
	JCheckBox chkServicesStatus;
	JCheckBox chkServicesDelete1;
	JCheckBox chkServicesAESC;
	JCheckBox chkServicesDelete2;

	JCheckBox chkPartnerAE;
	JCheckBox chkPartnerStatus;
	JCheckBox chkPartnerDelete1;
	JCheckBox chkIPTracker;
	JCheckBox chkContactInquiries;

	JCheckBox chkSettings;
	JCheckBox chkFrontMenus;
	JCheckBox chkBrowserBackground;

	JCheckBox chkUsersDelete;
	JCheckBox chkCategoriesDelete;
	JCheckBox chkBlogsDelete;
	JCheckBox chkServicesDelete;

//	public static void main(String[] args) {
//		EventQueue.invokeLater(() -> {
//			try {
//				mainWindow = new Frame9();
//				mainWindow.frameWindow.setVisible(true);
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//		});
//	}

	public Frame9() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		String addEditS = "Add Edit Save";
		String editS = "Edit Save";
		String delete = "Delete";
		String status = "Status";
		String pagination = "Pagination";
		String addEditSC = "Add Edit Save Continue";
		String editSC = "Edit Save Continue";
		String sorting = "Sorting";
		String mulitpleAdd = "Multiple Add";
		String mulitpleDelete = "Multiple Delete";
		String selectAll = "Select All";
		String front = "Front";

		frameWindow = new JFrame();
		frameWindow.setBounds(100, 100, 918, 705);
		frameWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frameWindow.getContentPane().setLayout(null);

		btnExecution = new JButton("Execute Script");
		btnExecution.setBounds(57, 616, 133, 23);
		frameWindow.getContentPane().add(btnExecution);

		// Users
		JCheckBox chkUserSelectAll = new JCheckBox(selectAll);
		chkUserSelectAll.setBounds(6, 44, 112, 23);
		frameWindow.getContentPane().add(chkUserSelectAll);

		chkUserAES = new JCheckBox(addEditS);
		chkUserAES.setBounds(6, 68, 112, 23);
		frameWindow.getContentPane().add(chkUserAES);

		chkUserStatus = new JCheckBox(status);
		chkUserStatus.setBounds(6, 92, 97, 23);
		frameWindow.getContentPane().add(chkUserStatus);

		chkUserDelete1 = new JCheckBox(delete);
		chkUserDelete1.setBounds(6, 116, 97, 23);
		frameWindow.getContentPane().add(chkUserDelete1);

		chkUserAESC = new JCheckBox(addEditSC);
		chkUserAESC.setBounds(6, 140, 133, 23);
		frameWindow.getContentPane().add(chkUserAESC);

		chkUserDelete2 = new JCheckBox(delete);
		chkUserDelete2.setBounds(6, 164, 97, 23);
		frameWindow.getContentPane().add(chkUserDelete2);

		chkUserPagination = new JCheckBox(pagination);
		chkUserPagination.setBounds(6, 188, 97, 23);

		chkUsersorting = new JCheckBox(sorting);
		chkUsersorting.setBounds(6, 212, 97, 23);

		JCheckBox chkMultipleUserAdd = new JCheckBox(mulitpleAdd);
		chkMultipleUserAdd.setBounds(6, 236, 97, 23);

		JCheckBox chkMulitpleUserDelete = new JCheckBox(mulitpleDelete);
		chkMulitpleUserDelete.setBounds(6, 260, 133, 23);

		// Pages

		JCheckBox chkPageSelectAll = new JCheckBox(selectAll);
		chkPageSelectAll.setBounds(155, 44, 146, 23);
		frameWindow.getContentPane().add(chkPageSelectAll);

		chkPageAES = new JCheckBox(editS);
		chkPageAES.setBounds(155, 68, 146, 23);
		frameWindow.getContentPane().add(chkPageAES);

		chkPageStatus = new JCheckBox(status);
		chkPageStatus.setBounds(155, 92, 146, 23);
		frameWindow.getContentPane().add(chkPageStatus);

		chkPageDelete1 = new JCheckBox(delete);
		chkPageDelete1.setBounds(155, 140, 146, 23);

		chkPageAESC = new JCheckBox(editSC);
		chkPageAESC.setBounds(155, 116, 146, 23);
		frameWindow.getContentPane().add(chkPageAESC);

		chkPageDelete2 = new JCheckBox(delete);
		chkPageDelete2.setBounds(155, 164, 146, 23);

		JCheckBox chkPagePagination = new JCheckBox(pagination);
		chkPagePagination.setBounds(155, 188, 146, 23);

		JCheckBox chkPageSorting = new JCheckBox(sorting);
		chkPageSorting.setBounds(155, 212, 146, 23);

		JCheckBox chkMultiplePageAdd = new JCheckBox(mulitpleAdd);
		chkMultiplePageAdd.setBounds(155, 236, 146, 23);

		JCheckBox chkMultiplePageDelete = new JCheckBox(mulitpleDelete);
		chkMultiplePageDelete.setBounds(155, 240, 146, 23);

		// Categories

		JCheckBox chkCategorySelectAll = new JCheckBox(selectAll);
		chkCategorySelectAll.setBounds(322, 44, 146, 23);
		frameWindow.getContentPane().add(chkCategorySelectAll);

		chkCategoryAE = new JCheckBox(addEditS);
		chkCategoryAE.setBounds(322, 68, 146, 23);
		frameWindow.getContentPane().add(chkCategoryAE);

		chkCategoryStatus = new JCheckBox(status);
		chkCategoryStatus.setBounds(322, 92, 146, 23);
		frameWindow.getContentPane().add(chkCategoryStatus);

		chkCategoryDelete1 = new JCheckBox(delete);
		chkCategoryDelete1.setBounds(322, 116, 146, 23);
		frameWindow.getContentPane().add(chkCategoryDelete1);

		chkCategoryAESC = new JCheckBox(addEditSC);
		chkCategoryAESC.setBounds(322, 140, 146, 23);
		frameWindow.getContentPane().add(chkCategoryAESC);

		chkCategoryDelete2 = new JCheckBox(delete);
		chkCategoryDelete2.setBounds(322, 164, 146, 23);
		frameWindow.getContentPane().add(chkCategoryDelete2);

		JCheckBox chkCategoryPagination = new JCheckBox(pagination);
		chkCategoryPagination.setBounds(322, 188, 146, 23);

		JCheckBox chkCategorySorting = new JCheckBox(sorting);
		chkCategorySorting.setBounds(322, 212, 146, 23);

		JCheckBox chkMultipleCategoryAdd = new JCheckBox(mulitpleAdd);
		chkMultipleCategoryAdd.setBounds(322, 236, 146, 23);

		JCheckBox chkMultipleCategoryDelete = new JCheckBox(mulitpleDelete);
		chkMultipleCategoryDelete.setBounds(322, 240, 146, 23);

		// Blogs
		JCheckBox chkBlogSelectAll = new JCheckBox(selectAll);
		chkBlogSelectAll.setBounds(498, 44, 146, 23);
		frameWindow.getContentPane().add(chkBlogSelectAll);

		chkBlogAddEdit = new JCheckBox(addEditS);
		chkBlogAddEdit.setBounds(498, 68, 146, 23);
		frameWindow.getContentPane().add(chkBlogAddEdit);

		chkBlogStatus = new JCheckBox(status);
		chkBlogStatus.setBounds(498, 92, 146, 23);
		frameWindow.getContentPane().add(chkBlogStatus);

		chkBlogFront = new JCheckBox(front);
		chkBlogFront.setBounds(498, 116, 146, 23);
		frameWindow.getContentPane().add(chkBlogFront);

		chkBlogDelete1 = new JCheckBox(delete);
		chkBlogDelete1.setBounds(498, 140, 146, 23);
		frameWindow.getContentPane().add(chkBlogDelete1);

		chkBlogAESContinue = new JCheckBox(addEditSC);
		chkBlogAESContinue.setBounds(498, 164, 146, 23);
		frameWindow.getContentPane().add(chkBlogAESContinue);

		chkBlogDelete2 = new JCheckBox(delete);
		chkBlogDelete2.setBounds(498, 188, 146, 23);
		frameWindow.getContentPane().add(chkBlogDelete2);

		chkBlogSettings = new JCheckBox("Settings");
		chkBlogSettings.setBounds(498, 212, 146, 23);
		frameWindow.getContentPane().add(chkBlogSettings);

		JCheckBox chkBlogPagination = new JCheckBox(pagination);
		chkBlogPagination.setBounds(498, 236, 146, 23);

		JCheckBox chkBlogSorting = new JCheckBox(sorting);
		chkBlogSorting.setBounds(498, 260, 146, 23);

		JCheckBox chkMultipleBlogAdd = new JCheckBox(mulitpleAdd);
		chkMultipleBlogAdd.setBounds(498, 284, 146, 23);

		JCheckBox chkMultipleBlogDelete = new JCheckBox(mulitpleDelete);
		chkMultipleBlogDelete.setBounds(498, 308, 146, 23);

		// Home Page Sliders
		JCheckBox chkHomePageSliderSelectAll = new JCheckBox(selectAll);
		chkHomePageSliderSelectAll.setBounds(694, 44, 146, 23);
		frameWindow.getContentPane().add(chkHomePageSliderSelectAll);

		chkHomePageSliderAE = new JCheckBox(addEditS);
		chkHomePageSliderAE.setBounds(694, 68, 146, 23);
		frameWindow.getContentPane().add(chkHomePageSliderAE);

		chkHomePageSliderStatus = new JCheckBox(status);
		chkHomePageSliderStatus.setBounds(694, 92, 146, 23);
		frameWindow.getContentPane().add(chkHomePageSliderStatus);

		chkHomePageSliderDelete1 = new JCheckBox(delete);
		chkHomePageSliderDelete1.setBounds(694, 116, 146, 23);
		frameWindow.getContentPane().add(chkHomePageSliderDelete1);

		chkHomePageSliderAESC = new JCheckBox(addEditSC);
		chkHomePageSliderAESC.setBounds(694, 140, 146, 23);
		frameWindow.getContentPane().add(chkHomePageSliderAESC);

		chkHomePageSliderDelete2 = new JCheckBox(delete);
		chkHomePageSliderDelete2.setBounds(694, 164, 146, 23);
		frameWindow.getContentPane().add(chkHomePageSliderDelete2);

		JCheckBox chkHomePageSliderPagination = new JCheckBox(pagination);
		chkHomePageSliderPagination.setBounds(694, 188, 146, 23);

		JCheckBox chkMultipleHomePageSliderAdd = new JCheckBox(mulitpleAdd);
		chkMultipleHomePageSliderAdd.setBounds(694, 212, 146, 23);

		JCheckBox chkMultipleHomePageSliderDelete = new JCheckBox(mulitpleDelete);
		chkMultipleHomePageSliderDelete.setBounds(694, 236, 146, 23);

		// Galleries
		JCheckBox chkGallerySelectAll = new JCheckBox(selectAll);
		chkGallerySelectAll.setBounds(10, 322, 129, 23);
		frameWindow.getContentPane().add(chkGallerySelectAll);

		chkGalleryAE = new JCheckBox(addEditS);
		chkGalleryAE.setBounds(10, 346, 129, 23);
		frameWindow.getContentPane().add(chkGalleryAE);

		chkGalleryStatus = new JCheckBox(status);
		chkGalleryStatus.setBounds(10, 370, 129, 23);
		frameWindow.getContentPane().add(chkGalleryStatus);

		chkGalleryDelete1 = new JCheckBox(delete);
		chkGalleryDelete1.setBounds(10, 394, 129, 23);
		frameWindow.getContentPane().add(chkGalleryDelete1);

		chkGalleryAESC = new JCheckBox(addEditSC);
		chkGalleryAESC.setBounds(10, 418, 146, 23);
		frameWindow.getContentPane().add(chkGalleryAESC);

		chkGalleryDelete2 = new JCheckBox(delete);
		chkGalleryDelete2.setBounds(10, 442, 129, 23);
		frameWindow.getContentPane().add(chkGalleryDelete2);

		JCheckBox chkGalleryPagination = new JCheckBox(pagination);
		chkGalleryPagination.setBounds(10, 466, 129, 23);

		JCheckBox chkMultipleGalleryAdd = new JCheckBox(mulitpleAdd);
		chkMultipleGalleryAdd.setBounds(10, 490, 129, 23);

		JCheckBox chkMultipleGalleryDelete = new JCheckBox(mulitpleDelete);
		chkMultipleGalleryDelete.setBounds(10, 514, 129, 23);

		// Services
		JCheckBox chkServicesSelectAll = new JCheckBox(selectAll);
		chkServicesSelectAll.setBounds(181, 322, 129, 23);
		frameWindow.getContentPane().add(chkServicesSelectAll);

		chkServicesAE = new JCheckBox(addEditS);
		chkServicesAE.setBounds(181, 346, 129, 23);
		frameWindow.getContentPane().add(chkServicesAE);

		chkServicesStatus = new JCheckBox(status);
		chkServicesStatus.setBounds(181, 370, 129, 23);
		frameWindow.getContentPane().add(chkServicesStatus);

		chkServicesDelete1 = new JCheckBox(delete);
		chkServicesDelete1.setBounds(181, 394, 129, 23);
		frameWindow.getContentPane().add(chkServicesDelete1);

		chkServicesAESC = new JCheckBox(addEditSC);
		chkServicesAESC.setBounds(181, 418, 151, 23);
		frameWindow.getContentPane().add(chkServicesAESC);

		chkServicesDelete2 = new JCheckBox(delete);
		chkServicesDelete2.setBounds(181, 442, 129, 23);
		frameWindow.getContentPane().add(chkServicesDelete2);

		JCheckBox chkServicesPagination = new JCheckBox(pagination);
		chkServicesPagination.setBounds(181, 466, 129, 23);

		JCheckBox chkServicesSorting = new JCheckBox(sorting);
		chkServicesSorting.setBounds(181, 490, 129, 23);

		JCheckBox chkServicesMulitpleAdd = new JCheckBox(mulitpleAdd);
		chkServicesMulitpleAdd.setBounds(181, 514, 129, 23);

		JCheckBox chkServicesMulitpleDelete = new JCheckBox(mulitpleDelete);
		chkServicesMulitpleDelete.setBounds(181, 538, 129, 23);

		// Partners
		JCheckBox chkPartnerSelectAll = new JCheckBox(selectAll);
		chkPartnerSelectAll.setBounds(352, 322, 129, 23);
		frameWindow.getContentPane().add(chkPartnerSelectAll);

		chkPartnerAE = new JCheckBox(addEditS);
		chkPartnerAE.setBounds(352, 346, 129, 23);
		frameWindow.getContentPane().add(chkPartnerAE);

		chkPartnerStatus = new JCheckBox(status);
		chkPartnerStatus.setBounds(352, 370, 129, 23);
		frameWindow.getContentPane().add(chkPartnerStatus);

		chkPartnerDelete1 = new JCheckBox(delete);
		chkPartnerDelete1.setBounds(352, 394, 129, 23);
		frameWindow.getContentPane().add(chkPartnerDelete1);

		JCheckBox chkPartnerAddEditSaveContinue = new JCheckBox(addEditSC);
		chkPartnerAddEditSaveContinue.setBounds(352, 418, 151, 23);

		JCheckBox chkPartnerDelete2 = new JCheckBox(delete);
		chkPartnerDelete2.setBounds(352, 442, 129, 23);

		// IP Tracker

		chkIPTracker = new JCheckBox("IPTracker");
		chkIPTracker.setBounds(57, 514, 129, 23);
		frameWindow.getContentPane().add(chkIPTracker);

		// Contact Us

		chkContactInquiries = new JCheckBox("Conatct Inquiries");
		chkContactInquiries.setBounds(203, 514, 129, 23);
		frameWindow.getContentPane().add(chkContactInquiries);

		// Settings

		chkSettings = new JCheckBox("Settings");
		chkSettings.setBounds(352, 514, 129, 23);
		frameWindow.getContentPane().add(chkSettings);

		JTextPane txtpnUsers = new JTextPane();
		txtpnUsers.setText("Users");
		txtpnUsers.setBounds(21, 22, 44, 20);
		frameWindow.getContentPane().add(txtpnUsers);

		JTextArea txtrPages = new JTextArea();
		txtrPages.setText("Pages");
		txtrPages.setBounds(171, 24, 48, 23);
		frameWindow.getContentPane().add(txtrPages);

		JTextArea txtrCategories = new JTextArea();
		txtrCategories.setText("Categories");
		txtrCategories.setBounds(332, 20, 89, 22);
		frameWindow.getContentPane().add(txtrCategories);

		JTextArea txtrBlog = new JTextArea();
		txtrBlog.setText("Blogs");
		txtrBlog.setBounds(517, 20, 89, 23);
		frameWindow.getContentPane().add(txtrBlog);

		JTextArea txtHomePageSliders = new JTextArea();
		txtHomePageSliders.setText("Home Page Sliders");
		txtHomePageSliders.setBounds(684, 19, 151, 23);
		frameWindow.getContentPane().add(txtHomePageSliders);

		JTextArea txtGalleries = new JTextArea();
		txtGalleries.setText("Galleries");
		txtGalleries.setBounds(21, 293, 97, 23);
		frameWindow.getContentPane().add(txtGalleries);

		JTextArea txtrServices = new JTextArea();
		txtrServices.setText("Services");
		txtrServices.setBounds(195, 293, 97, 23);
		frameWindow.getContentPane().add(txtrServices);

		JTextArea txtPartners = new JTextArea();
		txtPartners.setText("Partners");
		txtPartners.setBounds(371, 293, 97, 23);
		frameWindow.getContentPane().add(txtPartners);

		// Front Side
		JTextArea txtFrontSide = new JTextArea();
		txtFrontSide.setText("Front Side");
		txtFrontSide.setBounds(718, 264, 97, 23);
		frameWindow.getContentPane().add(txtFrontSide);

		chkFrontSelectAll = new JCheckBox(selectAll);
		chkFrontSelectAll.setBounds(718, 293, 129, 23);
		frameWindow.getContentPane().add(chkFrontSelectAll);

		chkFrontHeader = new JCheckBox("Header");
		chkFrontHeader.setBounds(718, 317, 129, 23);
		frameWindow.getContentPane().add(chkFrontHeader);

		chkFrontBannerSlider = new JCheckBox("Banner Slider");
		chkFrontBannerSlider.setBounds(718, 341, 129, 23);
		frameWindow.getContentPane().add(chkFrontBannerSlider);

		chkFrontAboutUsHomePage = new JCheckBox("About Us Home Page");
		chkFrontAboutUsHomePage.setBounds(718, 365, 162, 23);
		frameWindow.getContentPane().add(chkFrontAboutUsHomePage);

		chkFrontAboutUsDetailPage = new JCheckBox("About Us Detail Page");
		chkFrontAboutUsDetailPage.setBounds(718, 389, 162, 23);
		frameWindow.getContentPane().add(chkFrontAboutUsDetailPage);

		chkFrontServicesHomePage = new JCheckBox("Services Home Page");
		chkFrontServicesHomePage.setBounds(718, 413, 162, 23);
		frameWindow.getContentPane().add(chkFrontServicesHomePage);

		chkFrontServicesDetailPage = new JCheckBox("Services Detail Page");
		chkFrontServicesDetailPage.setBounds(718, 437, 162, 23);
		frameWindow.getContentPane().add(chkFrontServicesDetailPage);

		chkFrontGalleryHomePage = new JCheckBox("Gallery Home Page");
		chkFrontGalleryHomePage.setBounds(718, 461, 162, 23);
		frameWindow.getContentPane().add(chkFrontGalleryHomePage);

		chkFrontGalleryDetailPage = new JCheckBox("Gallery Detail Page");
		chkFrontGalleryDetailPage.setBounds(718, 485, 162, 23);
		frameWindow.getContentPane().add(chkFrontGalleryDetailPage);

		chkFrontPartnersHomePage = new JCheckBox("Parnters Home Page");
		chkFrontPartnersHomePage.setBounds(718, 509, 162, 23);
		frameWindow.getContentPane().add(chkFrontPartnersHomePage);

		chkFrontBlogsHomePage = new JCheckBox("Blogs Home Page");
		chkFrontBlogsHomePage.setBounds(718, 533, 162, 23);
		frameWindow.getContentPane().add(chkFrontBlogsHomePage);

		chkFrontBlogsDetailPage = new JCheckBox("Blogs Detail Page");
		chkFrontBlogsDetailPage.setBounds(718, 557, 162, 23);
		frameWindow.getContentPane().add(chkFrontBlogsDetailPage);

		chkFrontContactUsHomePage = new JCheckBox("Contact Us Home Page");
		chkFrontContactUsHomePage.setBounds(718, 581, 162, 23);
		frameWindow.getContentPane().add(chkFrontContactUsHomePage);

		chkFrontContactUsDetailPage = new JCheckBox("Contact Us Detail Page");
		chkFrontContactUsDetailPage.setBounds(718, 605, 162, 23);
		frameWindow.getContentPane().add(chkFrontContactUsDetailPage);

		chkFrontFooter = new JCheckBox("Footer");
		chkFrontFooter.setBounds(718, 629, 162, 23);
		frameWindow.getContentPane().add(chkFrontFooter);

		JTextArea txtDeleteAll = new JTextArea();
		txtDeleteAll.setText("Delete All");
		txtDeleteAll.setBounds(536, 293, 97, 23);
		frameWindow.getContentPane().add(txtDeleteAll);

		JCheckBox chkSelectAllDelete = new JCheckBox("Select All");
		chkSelectAllDelete.setBounds(517, 322, 129, 23);
		frameWindow.getContentPane().add(chkSelectAllDelete);

		chkUsersDelete = new JCheckBox("Users");
		chkUsersDelete.setBounds(517, 346, 129, 23);
		frameWindow.getContentPane().add(chkUsersDelete);

		chkCategoriesDelete = new JCheckBox("Categories");
		chkCategoriesDelete.setBounds(517, 370, 129, 23);
		frameWindow.getContentPane().add(chkCategoriesDelete);

		chkBlogsDelete = new JCheckBox("Blogs");
		chkBlogsDelete.setBounds(517, 394, 129, 23);
		frameWindow.getContentPane().add(chkBlogsDelete);

		chkServicesDelete = new JCheckBox("Services");
		chkServicesDelete.setBounds(517, 418, 129, 23);
		frameWindow.getContentPane().add(chkServicesDelete);

		chkBrowserBackground = new JCheckBox("Browser in Background");
		chkBrowserBackground.setBounds(57, 558, 186, 23);
		frameWindow.getContentPane().add(chkBrowserBackground);

		chkFrontMenus = new JCheckBox("Front Menus");
		chkFrontMenus.setBounds(504, 515, 129, 23);
		frameWindow.getContentPane().add(chkFrontMenus);

		chkUserSelectAll.addActionListener(e -> {
			if (chkUserSelectAll.isSelected()) {
				chkUserAES.setSelected(true);
				chkUserAESC.setSelected(true);
				chkUserDelete1.setSelected(true);
				chkUserDelete2.setSelected(true);
				chkUserStatus.setSelected(true);
			} else {
				chkUserAES.setSelected(false);
				chkUserAESC.setSelected(false);
				chkUserDelete1.setSelected(false);
				chkUserDelete2.setSelected(false);
				chkUserStatus.setSelected(false);
			}
		});

		chkPageSelectAll.addActionListener(e -> {
			if (chkPageSelectAll.isSelected()) {
				chkPageAES.setSelected(true);
				chkPageAESC.setSelected(true);
				chkPageStatus.setSelected(true);

				JOptionPane.showMessageDialog(null, "Make sure you got backup of database");
			} else {
				chkPageAES.setSelected(false);
				chkPageAESC.setSelected(false);
				chkPageStatus.setSelected(false);
			}
		});

		chkPageAES.addActionListener(e -> {
			JOptionPane.showMessageDialog(null, "Make sure you got backup of database");
		});

		chkPageAESC.addActionListener(e -> {
			JOptionPane.showMessageDialog(null, "Make sure you got backup of database");
		});

		chkPageStatus.addActionListener(e -> {
			JOptionPane.showMessageDialog(null, "Make sure you got backup of database");
		});

		chkCategorySelectAll.addActionListener(e -> {
			if (chkCategorySelectAll.isSelected()) {
				chkCategoryAE.setSelected(true);
				chkCategoryAESC.setSelected(true);
				chkCategoryStatus.setSelected(true);
				chkCategoryDelete1.setSelected(true);
				chkCategoryDelete2.setSelected(true);
			} else {
				chkCategoryAE.setSelected(false);
				chkCategoryAESC.setSelected(false);
				chkCategoryStatus.setSelected(false);
				chkCategoryDelete1.setSelected(false);
				chkCategoryDelete2.setSelected(false);
			}
		});

		chkBlogSelectAll.addActionListener(e -> {
			if (chkBlogSelectAll.isSelected()) {
				chkBlogAddEdit.setSelected(true);
				chkBlogAESContinue.setSelected(true);
				chkBlogDelete1.setSelected(true);
				chkBlogDelete2.setSelected(true);
				chkBlogStatus.setSelected(true);
				chkBlogFront.setSelected(true);
				chkBlogSettings.setSelected(true);
			} else {
				chkBlogAddEdit.setSelected(false);
				chkBlogAESContinue.setSelected(false);
				chkBlogDelete1.setSelected(false);
				chkBlogDelete2.setSelected(false);
				chkBlogStatus.setSelected(false);
				chkBlogFront.setSelected(false);
				chkBlogSettings.setSelected(false);
			}
		});

		chkHomePageSliderSelectAll.addActionListener(e -> {
			if (chkHomePageSliderSelectAll.isSelected()) {
				chkHomePageSliderAE.setSelected(true);
				chkHomePageSliderAESC.setSelected(true);
				chkHomePageSliderDelete1.setSelected(true);
				chkHomePageSliderDelete2.setSelected(true);
				chkHomePageSliderStatus.setSelected(true);
			} else {
				chkHomePageSliderAE.setSelected(false);
				chkHomePageSliderAESC.setSelected(false);
				chkHomePageSliderDelete1.setSelected(false);
				chkHomePageSliderDelete2.setSelected(false);
				chkHomePageSliderStatus.setSelected(false);
			}
		});

		chkGallerySelectAll.addActionListener(e -> {
			if (chkGallerySelectAll.isSelected()) {
				chkGalleryAE.setSelected(true);
				chkGalleryAESC.setSelected(true);
				chkGalleryDelete1.setSelected(true);
				chkGalleryDelete2.setSelected(true);
				chkGalleryStatus.setSelected(true);
			} else {
				chkGalleryAE.setSelected(false);
				chkGalleryAESC.setSelected(false);
				chkGalleryDelete1.setSelected(false);
				chkGalleryDelete2.setSelected(false);
				chkGalleryStatus.setSelected(false);
			}
		});

		chkServicesSelectAll.addActionListener(e -> {
			if (chkServicesSelectAll.isSelected()) {
				chkServicesAE.setSelected(true);
				chkServicesAESC.setSelected(true);
				chkServicesDelete1.setSelected(true);
				chkServicesDelete2.setSelected(true);
				chkServicesStatus.setSelected(true);
			} else {
				chkServicesAE.setSelected(false);
				chkServicesAESC.setSelected(false);
				chkServicesDelete1.setSelected(false);
				chkServicesDelete2.setSelected(false);
				chkServicesStatus.setSelected(false);
			}
		});

		chkPartnerSelectAll.addActionListener(e -> {
			if (chkPartnerSelectAll.isSelected()) {
				chkPartnerAE.setSelected(true);
				chkPartnerDelete1.setSelected(true);
				chkPartnerStatus.setSelected(true);
			} else {
				chkPartnerAE.setSelected(false);
				chkPartnerDelete1.setSelected(false);
				chkPartnerStatus.setSelected(false);
			}
		});

		chkSelectAllDelete.addActionListener(e -> {
			if (chkSelectAllDelete.isSelected()) {
				chkUsersDelete.setSelected(true);
				chkCategoriesDelete.setSelected(true);
				chkBlogsDelete.setSelected(true);
				chkServicesDelete.setSelected(true);
			} else {
				chkUsersDelete.setSelected(false);
				chkCategoriesDelete.setSelected(false);
				chkBlogsDelete.setSelected(false);
				chkServicesDelete.setSelected(false);
			}
		});

		chkSelectAllDelete.addActionListener(e -> {
			JOptionPane.showMessageDialog(null, "Make sure you got backup of database");
		});

		chkUsersDelete.addActionListener(e -> {
			JOptionPane.showMessageDialog(null, "Make sure you got backup of database");
		});

		chkCategoriesDelete.addActionListener(e -> {
			JOptionPane.showMessageDialog(null, "Make sure you got backup of database");
		});

		chkBlogsDelete.addActionListener(e -> {
			JOptionPane.showMessageDialog(null, "Make sure you got backup of database");
		});

		chkServicesDelete.addActionListener(e -> {
			JOptionPane.showMessageDialog(null, "Make sure you got backup of database");
		});

		chkSettings.addActionListener(e -> {
			JOptionPane.showMessageDialog(null, "Make sure you got backup of database");
		});

		chkFrontSelectAll.addActionListener(e -> {
			if (chkFrontSelectAll.isSelected()) {
				chkFrontHeader.setSelected(true);
				chkFrontBannerSlider.setSelected(true);
				chkFrontAboutUsHomePage.setSelected(true);
				chkFrontAboutUsDetailPage.setSelected(true);
				chkFrontServicesHomePage.setSelected(true);
				chkFrontServicesDetailPage.setSelected(true);
				chkFrontGalleryHomePage.setSelected(true);
				chkFrontGalleryDetailPage.setSelected(true);
				chkFrontPartnersHomePage.setSelected(true);
				chkFrontBlogsHomePage.setSelected(true);
				chkFrontBlogsDetailPage.setSelected(true);
				chkFrontContactUsHomePage.setSelected(true);
				chkFrontContactUsDetailPage.setSelected(true);
				chkFrontFooter.setSelected(true);
			} else {
				chkFrontHeader.setSelected(false);
				chkFrontBannerSlider.setSelected(false);
				chkFrontAboutUsHomePage.setSelected(false);
				chkFrontAboutUsDetailPage.setSelected(false);
				chkFrontServicesHomePage.setSelected(false);
				chkFrontServicesDetailPage.setSelected(false);
				chkFrontGalleryHomePage.setSelected(false);
				chkFrontGalleryDetailPage.setSelected(false);
				chkFrontPartnersHomePage.setSelected(false);
				chkFrontBlogsHomePage.setSelected(false);
				chkFrontBlogsDetailPage.setSelected(false);
				chkFrontContactUsHomePage.setSelected(false);
				chkFrontContactUsDetailPage.setSelected(false);
				chkFrontFooter.setSelected(false);
			}
		});

		chkFrontSelectAll.addActionListener(e -> {
			JOptionPane.showMessageDialog(null, "Make sure you got backup of database");
		});

		chkFrontHeader.addActionListener(e -> {
			JOptionPane.showMessageDialog(null, "Make sure you got backup of database");
		});

		chkFrontFooter.addActionListener(e -> {
			JOptionPane.showMessageDialog(null, "Make sure you got backup of database");
		});

		chkFrontMenus.addActionListener(e -> {
			JOptionPane.showMessageDialog(null, "Make sure you got backup of database");
		});

		btnExecution.addActionListener(e -> {
			FrameVal.browserInBackground(chkBrowserBackground);
			adminCheck();

			frontCheck();

//			File f = new File(System.getProperty("user.dir") + "\\allure-results");
//			if (f.isDirectory()) {
//				try {
//					FileUtils.deleteDirectory(f);
//				} catch (IOException e1) {
//					e1.printStackTrace();
//				}
//			}

			TestMain tm = new TestMain();
			tm.getAndVerifyTestNgSuiteName();

//			try {
//
//				Runtime.getRuntime().exec("cmd /c start cmd.exe /K allure serve");
//
//			} catch (IOException e3) {
//				e3.printStackTrace();
//			}

		});
	}

	public void adminCheck() {
		// Users
		FrameVal.userAE(chkUserAES);
		FrameVal.userStatus(chkUserStatus);
		FrameVal.userDelete(chkUserDelete1);
		FrameVal.userAESC(chkUserAESC);
		if (chkUserDelete2.isSelected()) {
			FrameVal.userDelete(chkUserDelete2);
		}
		FrameVal.userTestingDataDelete(chkUsersDelete);

		// Pages
		FrameVal.pageAE(chkPageAES);
		FrameVal.pageStatus(chkPageStatus);
		FrameVal.pageAESC(chkPageAESC);

		// Categories
		FrameVal.categoryAE(chkCategoryAE);
		FrameVal.categoryStatus(chkCategoryStatus);
		FrameVal.categoryDelete(chkCategoryDelete1);
		FrameVal.categoryAESC(chkCategoryAESC);
		FrameVal.categoryDelete(chkCategoryDelete2);
		FrameVal.categoryTestingDataDelete(chkCategoriesDelete);

		// Blogs
		FrameVal.blogAddEditSaveButton(chkBlogAddEdit);
		FrameVal.blogStatus(chkBlogStatus);
		FrameVal.blogFront(chkBlogFront);
		FrameVal.blogDelete(chkBlogDelete1);
		FrameVal.blogAESC(chkBlogAESContinue);
		FrameVal.blogFront(chkBlogFront);
		FrameVal.blogTestingDataDelete(chkBlogsDelete);
		FrameVal.blogDelete(chkBlogDelete2);
		FrameVal.blogSettings(chkBlogSettings);

		// Galleries
		FrameVal.galleryAE(chkGalleryAE);
		FrameVal.galleryStatus(chkGalleryStatus);
		FrameVal.galleryDelete(chkGalleryDelete1);
		FrameVal.galleryAESC(chkGalleryAESC);
		FrameVal.galleryDelete(chkGalleryDelete2);

		// Home Page Sliders
		FrameVal.homePageSliderAE(chkHomePageSliderAE);
		FrameVal.homePageSliderStatus(chkHomePageSliderStatus);
		FrameVal.homePageSliderDelete(chkHomePageSliderDelete1);
		FrameVal.homePageSliderAESC(chkHomePageSliderAESC);
		FrameVal.homePageSliderDelete(chkHomePageSliderDelete2);

		// Services
		FrameVal.servicesAddEditSaveButton(chkServicesAE);
		FrameVal.servicesActiveInactive(chkServicesStatus);
		FrameVal.servicesDelete(chkServicesDelete1);
		FrameVal.servicesAddEditSaveContinue(chkServicesAESC);
		FrameVal.servicesDelete(chkServicesDelete2);
		FrameVal.serviceTestingDataDelete(chkServicesDelete);

		// Partners
		FrameVal.partnersAddEditSaveButton(chkPartnerAE);
		FrameVal.partnersActiveInactive(chkPartnerStatus);
		FrameVal.partnersDelete(chkPartnerDelete1);

		FrameVal.settings(chkSettings);

		FrameVal.frontMenus(chkFrontMenus);

		FrameVal.contactInquiries(chkContactInquiries);

		FrameVal.ipTracker(chkIPTracker);

	}

	public void frontCheck() {
		// Header
		FrameValFront.header(chkFrontHeader);

		// Banner Slider
		FrameValFront.bannerSlider(chkFrontBannerSlider);

		// About Us
		FrameValFront.aboutUsHomePage(chkFrontAboutUsHomePage);
		FrameValFront.aboutUsDetailPage(chkFrontAboutUsDetailPage);

		// Services
		FrameValFront.servicesHomePage(chkFrontServicesHomePage);
		FrameValFront.servicesDetailPage(chkFrontServicesDetailPage);

		// Work Gallery
		FrameValFront.galleryHomePage(chkFrontGalleryHomePage);
		FrameValFront.galleryDetailPage(chkFrontGalleryDetailPage);

		// Partners
		FrameValFront.partnersHomePage(chkFrontPartnersHomePage);

		// Blogs
		FrameValFront.blogsHomePage(chkFrontBlogsHomePage);
		FrameValFront.blogsDetailPage(chkFrontBlogsDetailPage);

		// Contact Us
		FrameValFront.contactUsHomePage(chkFrontContactUsHomePage);
		FrameValFront.contactUsDetailPage(chkFrontContactUsDetailPage);

		// Footer
		FrameValFront.frontFooter(chkFrontFooter);

	}
}
