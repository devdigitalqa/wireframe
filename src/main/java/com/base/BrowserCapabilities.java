package com.base;

import java.util.HashMap;

import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

public class BrowserCapabilities {

	public ChromeOptions getChromeOptions() {

		DesiredCapabilities chrome = new DesiredCapabilities();

		String downloadFilepath = System.getProperty("user.dir") + "\\src\\main\\resources\\com\\extrafiles";
		HashMap<String, Object> chromePrefs = new HashMap<>();
		chromePrefs.put("profile.default_content_settings.popups", 0);
		chromePrefs.put("download.default_directory", downloadFilepath);

		ChromeOptions options = new ChromeOptions();
		options.addArguments("--test-type");
		options.addArguments("--disable-popup-blocking");
		if (FrameVal.getBrowserInBackground()) {
			options.addArguments("--headless");
		}
		options.setCapability(ChromeOptions.CAPABILITY, chrome);
		chrome.setJavascriptEnabled(true);
		options.setExperimentalOption("prefs", chromePrefs);

		return options;
	}
}
