package com.base;

import java.awt.EventQueue;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JTextField;

import com.utilities.CommonVariables;

public class Frame {

	JFrame frameWindow;
	static Frame mainWindow;
	JButton btnExecution;
	JTextField txtFrontURL;
	JTextField txtAdminURL;
	JComboBox dropdownTemplate;
	public static final CommonVariables commonVariables = new CommonVariables();
	private JTextField txtPleaseEnterEmail;
	private JTextField txtPleaseEnterPassword;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(() -> {
			try {
				mainWindow = new Frame();
				mainWindow.frameWindow.setVisible(true);
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
	}

	/**
	 * Create the application.
	 * 
	 * @wbp.parser.entryPoint
	 */
	public Frame() {
		initialize();

	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frameWindow = new JFrame();
		frameWindow.setBounds(100, 100, 918, 705);
		frameWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frameWindow.getContentPane().setLayout(null);

		btnExecution = new JButton("Execute Script");
		btnExecution.setBounds(109, 297, 133, 23);
		frameWindow.getContentPane().add(btnExecution);

		txtFrontURL = new JTextField("Please enter front url");
		txtAdminURL = new JTextField("Please enter admin url");

		txtFrontURL.setBounds(109, 175, 183, 23);
		frameWindow.getContentPane().add(txtFrontURL);
		txtAdminURL.setBounds(109, 227, 183, 23);
		frameWindow.getContentPane().add(txtAdminURL);
		// array of string contating cities
		String s1[] = { "Template 1", "Template 2", "Template 3", "Template 4", "Template 5", "Template 7",
				"Template 8", "Template 9" };

		dropdownTemplate = new JComboBox(s1);
		dropdownTemplate.setBounds(109, 122, 96, 21);
		frameWindow.getContentPane().add(dropdownTemplate);

		txtPleaseEnterEmail = new JTextField("Please enter email address");
		txtPleaseEnterEmail.setBounds(341, 227, 183, 23);
		frameWindow.getContentPane().add(txtPleaseEnterEmail);

		txtPleaseEnterPassword = new JTextField("Please enter password");
		txtPleaseEnterPassword.setBounds(341, 279, 183, 23);
		frameWindow.getContentPane().add(txtPleaseEnterPassword);

		btnExecution.addActionListener(e -> {
			EventQueue.invokeLater(() -> {
				try {
					if (dropdownTemplate.getSelectedItem().equals("Template 9")) {
						Frame9 window = new Frame9();
						window.frameWindow.setVisible(true);
						mainWindow.frameWindow.setVisible(false);
					} else {
						mainWindow.frameWindow.setVisible(true);
					}
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			});
		});

		txtFrontURL.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent arg0) {
				if (!txtFrontURL.getText().isEmpty()) {
					Frame.commonVariables.setFrontURL(txtFrontURL.getText());
				} else {
					txtFrontURL.setText("Please enter front url");
					Frame.commonVariables.setFrontURL("");
				}
			}

			@Override
			public void focusGained(FocusEvent arg0) {
				if (txtFrontURL.getText().equals("Please enter front url")) {
					txtFrontURL.setText("");
				}
			}
		});

		txtAdminURL.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent arg0) {
				if (!txtAdminURL.getText().isEmpty()) {
					Frame.commonVariables.setAdminURL(txtAdminURL.getText());
				} else {
					txtAdminURL.setText("Please enter admin url");
					Frame.commonVariables.setAdminURL("");
				}
			}

			@Override
			public void focusGained(FocusEvent arg0) {
				if (txtAdminURL.getText().equals("Please enter admin url")) {
					txtAdminURL.setText("");
				}
			}
		});

		txtPleaseEnterEmail.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent arg0) {
				if (!txtPleaseEnterEmail.getText().isEmpty()) {
					Frame.commonVariables.loginEmail = txtPleaseEnterEmail.getText();
				} else {
					txtPleaseEnterEmail.setText("Please enter email address");
					Frame.commonVariables.loginEmail = "";
				}
			}

			@Override
			public void focusGained(FocusEvent arg0) {
				if (txtPleaseEnterEmail.getText().equals("Please enter email address")) {
					txtPleaseEnterEmail.setText("");
				}
			}
		});

		txtPleaseEnterPassword.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent arg0) {
				if (!txtPleaseEnterPassword.getText().isEmpty()) {
					Frame.commonVariables.loginPwd = txtPleaseEnterPassword.getText();
				} else {
					txtPleaseEnterPassword.setText("Please enter password");
					Frame.commonVariables.loginPwd = "";
				}
			}

			@Override
			public void focusGained(FocusEvent arg0) {
				if (txtPleaseEnterPassword.getText().equals("Please enter password")) {
					txtPleaseEnterPassword.setText("");
				}
			}
		});
	}
}
