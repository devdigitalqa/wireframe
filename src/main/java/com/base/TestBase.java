package com.base;

import static org.testng.Assert.assertTrue;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import com.basicactions.LogHelper;
import com.utilities.FilesPaths;

public class TestBase {

	static WebDriver driver;
	static Logger log = LogHelper.getLogger(TestBase.class);
	static BrowserCapabilities browserCapabilities;

	public static WebDriver launchBrowser(String browserName) {
		if (browserName.equalsIgnoreCase("chrome")) {
			log.info("********************Initializing chrome browser********************");
			browserCapabilities = new BrowserCapabilities();
			ChromeOptions cap = browserCapabilities.getChromeOptions();
			System.setProperty("webdriver.chrome.driver", FilesPaths.CHROME_DRIVER);
			driver = new ChromeDriver(cap);
			log.info("********************Initialized chrome browser********************");

		} else {
			log.error("********************Invalid browser name********************");

			assertTrue(false, "No Browser found exception");
		}
		return driver;
	}

	TestBase(String browserName) {
		if (driver == null) {
			launchBrowser(browserName);
		} else {
			getDriver();
		}
	}

	public static WebDriver getDriver() {
		return driver;
	}
}
