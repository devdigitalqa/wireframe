package com.base;

import javax.swing.JCheckBox;

public class FrameVal {

	// Users
	static boolean userAE = false;
	static boolean userStatus = false;
	static boolean userDelete = false;
	static boolean userTestingDataDelete = false;
	static boolean userAESC = false;

	// Pages
	static boolean pageAE = false;
	static boolean pageStatus = false;
	static boolean pageAESC = false;

	// Blogs
	static boolean blogAddEditSaveButton = false;
	static boolean blogAESC = false;
	static boolean blogDelete = false;
	static boolean blogStatus = false;
	static boolean blogFront = false;
	static boolean blogSettings = false;
	static boolean blogTestingDataDelete = false;

	// Galleries
	static boolean galleryAE = false;
	static boolean galleryStatus = false;
	static boolean galleryDelete = false;
	static boolean galleryAESC = false;

	// Home Page Sliders
	static boolean homePageSliderAE = false;
	static boolean homePageSliderStatus = false;
	static boolean homePageSliderDelete = false;
	static boolean homePageSliderAESC = false;

	// Services
	static boolean servicesAddEditSaveButton = false;
	static boolean servicesActiveInactive = false;
	static boolean servicesDelete = false;
	static boolean servicesAddEditSaveContinue = false;
	static boolean serviceTestingDataDelete = false;

	// Partners
	static boolean partnersAddEditSaveButton = false;
	static boolean partnersActiveInactive = false;
	static boolean partnersDelete = false;
	static boolean partnersAddEditSaveContinue = false;

	// Categories
	static boolean categoryAES = false;
	static boolean categoryDelete = false;
	static boolean categoryStatus = false;
	static boolean categoryAESC = false;
	static boolean categoryTestingDataDelete = false;

	// Settings
	static boolean settings = false;

	static boolean frontMenus = false;

	// Contact Inquiries
	static boolean contactInquiries = false;

	// IP Tracker
	static boolean ipTracker = false;

	// Browser in Backgorund
	static boolean browserInBackground = false;

	// Browser in Backgorund
	public static void browserInBackground(JCheckBox chkModule) {
		if (chkModule.isSelected()) {
			browserInBackground = true;
		} else {
			browserInBackground = false;
		}
	}

	public static boolean getBrowserInBackground() {
		return browserInBackground;
	}

	// Users
	public static void userAE(JCheckBox chkModule) {
		if (chkModule.isSelected()) {
			userAE = true;
		} else {
			userAE = false;
		}
	}

	public static boolean getUserAE() {
		return userAE;
	}

	public static void userStatus(JCheckBox chkModule) {
		if (chkModule.isSelected()) {
			
		} else {
			userStatus = false;
		}
	}

	public static boolean getUserStatus() {
		return userStatus;
	}

	public static void userDelete(JCheckBox chkModule) {
		if (chkModule.isSelected()) {
			userDelete = true;
		} else {
			userDelete = false;
		}
	}

	public static boolean getUserDelete() {
		return userDelete;
	}

	public static void userTestingDataDelete(JCheckBox chkModule) {
		if (chkModule.isSelected()) {
			userTestingDataDelete = true;
		} else {
			userTestingDataDelete = false;
		}
	}

	public static boolean getUserTestingDataDelete() {
		return userTestingDataDelete;
	}

	public static void userAESC(JCheckBox chkModule) {
		if (chkModule.isSelected()) {
			userAESC = true;
		} else {
			userAESC = false;
		}
	}

	public static boolean getUserAESC() {
		return userAESC;
	}

	// Pages

	public static void pageAE(JCheckBox chkModule) {
		if (chkModule.isSelected()) {
			pageAE = true;
		} else {
			pageAE = false;
		}
	}

	public static boolean getPageAE() {
		return pageAE;
	}

	public static void pageStatus(JCheckBox chkModule) {
		if (chkModule.isSelected()) {
			pageStatus = true;
		} else {
			pageStatus = false;
		}
	}

	public static boolean getPageStatus() {
		return pageStatus;
	}

	public static void pageAESC(JCheckBox chkModule) {
		if (chkModule.isSelected()) {
			pageAESC = true;
		} else {
			pageAESC = false;
		}
	}

	public static boolean getPageAESC() {
		return pageAESC;
	}

	// Categories
	public static void categoryAE(JCheckBox chkModule) {
		if (chkModule.isSelected()) {
			categoryAES = true;
		} else {
			categoryAES = false;
		}
	}

	public static boolean getCategoryAE() {
		return categoryAES;
	}

	public static void categoryStatus(JCheckBox chkModule) {
		if (chkModule.isSelected()) {
			categoryStatus = true;
		} else {
			categoryStatus = false;
		}
	}

	public static boolean getCategoryStatus() {
		return categoryStatus;
	}

	public static void categoryDelete(JCheckBox chkModule) {
		if (chkModule.isSelected()) {
			categoryDelete = true;
		} else {
			categoryDelete = false;
		}
	}

	public static boolean getCategoryDelete() {
		return categoryDelete;
	}

	public static void categoryTestingDataDelete(JCheckBox chkModule) {
		if (chkModule.isSelected()) {
			categoryTestingDataDelete = true;
		} else {
			categoryTestingDataDelete = false;
		}
	}

	public static boolean getCategoryTestingDataDelete() {
		return categoryTestingDataDelete;
	}

	public static void categoryAESC(JCheckBox chkModule) {
		if (chkModule.isSelected()) {
			categoryAESC = true;
		} else {
			categoryAESC = false;
		}
	}

	public static boolean getCategoryAESC() {
		return categoryAESC;
	}

	// Blogs
	public static void blogAddEditSaveButton(JCheckBox chkModule) {
		if (chkModule.isSelected()) {
			blogAddEditSaveButton = true;
		} else {
			blogAddEditSaveButton = false;
		}
	}

	public static boolean getBlogAddEditSaveButton() {
		return blogAddEditSaveButton;
	}

	public static void blogStatus(JCheckBox chkModule) {
		if (chkModule.isSelected()) {
			blogStatus = true;
		} else {
			blogStatus = false;
		}
	}

	public static boolean getBlogStatus() {
		return blogStatus;
	}

	public static void blogTestingDataDelete(JCheckBox chkModule) {
		if (chkModule.isSelected()) {
			blogTestingDataDelete = true;
		} else {
			blogTestingDataDelete = false;
		}
	}

	public static boolean getBlogTestingDataDelete() {
		return blogTestingDataDelete;
	}

	public static void blogFront(JCheckBox chkModule) {
		if (chkModule.isSelected()) {
			blogFront = true;
		} else {
			blogFront = false;
		}
	}

	public static boolean getBlogFront() {
		return blogFront;
	}

	public static void blogAESC(JCheckBox chkModule) {
		if (chkModule.isSelected()) {
			blogAESC = true;
		} else {
			blogAESC = false;
		}
	}

	public static boolean getBlogAESC() {
		return blogAESC;
	}

	public static void blogSettings(JCheckBox chkModule) {
		if (chkModule.isSelected()) {
			blogSettings = true;
		} else {
			blogSettings = false;
		}
	}

	public static boolean getBlogSettings() {
		return blogSettings;
	}

	public static void blogDelete(JCheckBox chkModule) {
		if (chkModule.isSelected()) {
			blogDelete = true;
		} else {
			blogDelete = false;
		}
	}

	public static boolean getBlogDelete() {
		return blogDelete;
	}

	// Services

	public static void servicesAddEditSaveButton(JCheckBox chkModule) {
		if (chkModule.isSelected()) {
			servicesAddEditSaveButton = true;
		} else {
			servicesAddEditSaveButton = false;
		}
	}

	public static void servicesActiveInactive(JCheckBox chkModule) {
		if (chkModule.isSelected()) {
			servicesActiveInactive = true;
		} else {
			servicesActiveInactive = false;
		}
	}

	public static void servicesDelete(JCheckBox chkModule) {
		if (chkModule.isSelected()) {
			servicesDelete = true;
		} else {
			servicesDelete = false;
		}
	}

	public static void servicesAddEditSaveContinue(JCheckBox chkModule) {
		if (chkModule.isSelected()) {
			servicesAddEditSaveContinue = true;
		} else {
			servicesAddEditSaveContinue = false;
		}
	}

	public static boolean getServicesAddEditSaveButton() {
		return servicesAddEditSaveButton;
	}

	public static boolean getServicesActiveInactive() {
		return servicesActiveInactive;
	}

	public static boolean getServicesDelete() {
		return servicesDelete;
	}

	public static boolean getServicesAddEditSaveContinue() {
		return servicesAddEditSaveContinue;
	}

	public static void serviceTestingDataDelete(JCheckBox chkModule) {
		if (chkModule.isSelected()) {
			serviceTestingDataDelete = true;
		} else {
			serviceTestingDataDelete = false;
		}
	}

	public static boolean getServiceTestingDataDelete() {
		return serviceTestingDataDelete;
	}

	// Partners

	public static void partnersAddEditSaveButton(JCheckBox chkModule) {
		if (chkModule.isSelected()) {
			partnersAddEditSaveButton = true;
		} else {
			partnersAddEditSaveButton = false;
		}
	}

	public static void partnersActiveInactive(JCheckBox chkModule) {
		if (chkModule.isSelected()) {
			partnersActiveInactive = true;
		} else {
			partnersActiveInactive = false;
		}
	}

	public static void partnersDelete(JCheckBox chkModule) {
		if (chkModule.isSelected()) {
			partnersDelete = true;
		} else {
			partnersDelete = false;
		}
	}

	public static void partnersAddEditSaveContinue(JCheckBox chkModule) {
		if (chkModule.isSelected()) {
			partnersAddEditSaveContinue = true;
		} else {
			partnersAddEditSaveContinue = false;
		}
	}

	public static boolean getPartnersAddEditSaveButton() {
		return partnersAddEditSaveButton;
	}

	public static boolean getPartnersActiveInactive() {
		return partnersActiveInactive;
	}

	public static boolean getPartnersDelete() {
		return partnersDelete;
	}

	public static boolean getPartnersAddEditSaveContinue() {
		return partnersAddEditSaveContinue;
	}

	// Galleries

	public static void galleryAE(JCheckBox chkModule) {
		if (chkModule.isSelected()) {
			galleryAE = true;
		} else {
			galleryAE = false;
		}
	}

	public static void galleryStatus(JCheckBox chkModule) {
		if (chkModule.isSelected()) {
			galleryStatus = true;
		} else {
			galleryStatus = false;
		}
	}

	public static void galleryDelete(JCheckBox chkModule) {
		if (chkModule.isSelected()) {
			galleryDelete = true;
		} else {
			galleryDelete = false;
		}
	}

	public static void galleryAESC(JCheckBox chkModule) {
		if (chkModule.isSelected()) {
			galleryAESC = true;
		} else {
			galleryAESC = false;
		}
	}

	public static boolean getGalleryAE() {
		return galleryAE;
	}

	public static boolean getGalleryStatus() {
		return galleryStatus;
	}

	public static boolean getGalleryDelete() {
		return galleryDelete;
	}

	public static boolean getGalleryAESC() {
		return galleryAESC;
	}

	// Home Page Sliders

	public static void homePageSliderAE(JCheckBox chkModule) {
		if (chkModule.isSelected()) {
			homePageSliderAE = true;
		} else {
			homePageSliderAE = false;
		}
	}

	public static void homePageSliderStatus(JCheckBox chkModule) {
		if (chkModule.isSelected()) {
			homePageSliderStatus = true;
		} else {
			homePageSliderStatus = false;
		}
	}

	public static void homePageSliderDelete(JCheckBox chkModule) {
		if (chkModule.isSelected()) {
			homePageSliderDelete = true;
		} else {
			homePageSliderDelete = false;
		}
	}

	public static void homePageSliderAESC(JCheckBox chkModule) {
		if (chkModule.isSelected()) {
			homePageSliderAESC = true;
		} else {
			homePageSliderAESC = false;
		}
	}

	public static boolean getHomePageSliderAE() {
		return homePageSliderAE;
	}

	public static boolean getHomePageSliderStatus() {
		return homePageSliderStatus;
	}

	public static boolean getHomePageSliderDelete() {
		return homePageSliderDelete;
	}

	public static boolean getHomePageSliderAESC() {
		return homePageSliderAESC;
	}

	// Settings

	public static void settings(JCheckBox chkModule) {
		if (chkModule.isSelected()) {
			settings = true;
		} else {
			settings = false;
		}
	}

	public static boolean getSettings() {
		return settings;
	}

	// Front Menus

	public static void frontMenus(JCheckBox chkModule) {
		if (chkModule.isSelected()) {
			frontMenus = true;
		} else {
			frontMenus = false;
		}
	}

	public static boolean getFrontMenus() {
		return frontMenus;
	}

	// Contact Inquiries

	public static void contactInquiries(JCheckBox chkModule) {
		if (chkModule.isSelected()) {
			contactInquiries = true;
		} else {
			contactInquiries = false;
		}
	}

	public static boolean getContactInquiries() {
		return contactInquiries;
	}

	// IP Tracker

	public static void ipTracker(JCheckBox chkModule) {
		if (chkModule.isSelected()) {
			ipTracker = true;
		} else {
			ipTracker = false;
		}
	}

	public static boolean getIPTracker() {
		return ipTracker;
	}

	private FrameVal() {
	}
}
