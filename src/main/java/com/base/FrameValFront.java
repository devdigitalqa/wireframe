package com.base;

import javax.swing.JCheckBox;

public class FrameValFront {

	static boolean header = false;

	static boolean bannerSlider = false;

	static boolean aboutUsHomePage = false;
	static boolean aboutUsDetailPage = false;

	static boolean servicesDetailPage = false;
	static boolean servicesHomePage = false;

	static boolean galleryDetailPage = false;
	static boolean galleryHomePage = false;

	static boolean partnersHomePage = false;

	static boolean blogsHomePage = false;
	static boolean blogsDetailPage = false;
	
	static boolean contactUsHomePage = false;
	static boolean contactUsDetailPage = false;

	static boolean frontFooter = false;

	// Header
	public static void header(JCheckBox chkModule) {
		if (chkModule.isSelected()) {
			header = true;
		} else {
			header = false;
		}
	}

	public static boolean getHeader() {
		return header;
	}

	// Banner Slider
	public static void bannerSlider(JCheckBox chkModule) {
		if (chkModule.isSelected()) {
			bannerSlider = true;
		} else {
			bannerSlider = false;
		}
	}

	public static boolean getBannerSlider() {
		return bannerSlider;
	}

	// About Us
	public static void aboutUsHomePage(JCheckBox chkModule) {
		if (chkModule.isSelected()) {
			aboutUsHomePage = true;
		} else {
			aboutUsHomePage = false;
		}
	}

	public static boolean getAboutUsHomePage() {
		return aboutUsHomePage;
	}

	public static void aboutUsDetailPage(JCheckBox chkModule) {
		if (chkModule.isSelected()) {
			aboutUsDetailPage = true;
		} else {
			aboutUsDetailPage = false;
		}
	}

	public static boolean getAboutUsDetailPage() {
		return aboutUsDetailPage;
	}

	// Services
	public static void servicesHomePage(JCheckBox chkModule) {
		if (chkModule.isSelected()) {
			servicesHomePage = true;
		} else {
			servicesHomePage = false;
		}
	}

	public static boolean getServicesHomePage() {
		return servicesHomePage;
	}

	public static void servicesDetailPage(JCheckBox chkModule) {
		if (chkModule.isSelected()) {
			servicesDetailPage = true;
		} else {
			servicesDetailPage = false;
		}
	}

	public static boolean getServicesDetailPage() {
		return servicesDetailPage;
	}

	// Work Gallery
	public static void galleryHomePage(JCheckBox chkModule) {
		if (chkModule.isSelected()) {
			galleryHomePage = true;
		} else {
			galleryHomePage = false;
		}
	}

	public static boolean getGalleryHomePage() {
		return galleryHomePage;
	}

	public static void galleryDetailPage(JCheckBox chkModule) {
		if (chkModule.isSelected()) {
			galleryDetailPage = true;
		} else {
			galleryDetailPage = false;
		}
	}

	public static boolean getGalleryDetailPage() {
		return galleryDetailPage;
	}

	// Partners
	public static void partnersHomePage(JCheckBox chkModule) {
		if (chkModule.isSelected()) {
			partnersHomePage = true;
		} else {
			partnersHomePage = false;
		}
	}

	public static boolean getPartnersHomePage() {
		return partnersHomePage;
	}

	// Blogs
	public static void blogsHomePage(JCheckBox chkModule) {
		if (chkModule.isSelected()) {
			blogsHomePage = true;
		} else {
			blogsHomePage = false;
		}
	}

	public static boolean getBlogsHomePage() {
		return blogsHomePage;
	}

	public static void blogsDetailPage(JCheckBox chkModule) {
		if (chkModule.isSelected()) {
			blogsDetailPage = true;
		} else {
			blogsDetailPage = false;
		}
	}

	public static boolean getBlogsDetailPage() {
		return blogsDetailPage;
	}
	
	//Contact Us
	
	public static void contactUsHomePage(JCheckBox chkModule) {
		if (chkModule.isSelected()) {
			contactUsHomePage = true;
		} else {
			contactUsHomePage = false;
		}
	}

	public static boolean getContactUsHomePage() {
		return contactUsHomePage;
	}
	
	public static void contactUsDetailPage(JCheckBox chkModule) {
		if (chkModule.isSelected()) {
			contactUsDetailPage = true;
		} else {
			contactUsDetailPage = false;
		}
	}

	public static boolean getContactUsDetailPage() {
		return contactUsDetailPage;
	}

	// Footer

	public static void frontFooter(JCheckBox chkModule) {
		if (chkModule.isSelected()) {
			frontFooter = true;
		} else {
			frontFooter = false;
		}
	}

	public static boolean getFrontFooter() {
		return frontFooter;
	}

	private FrameValFront() {
	}
}
