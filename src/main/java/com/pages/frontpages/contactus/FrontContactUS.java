package com.pages.frontpages.contactus;

import static org.testng.Assert.assertTrue;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import com.basicactions.DropDownHelper;
import com.utilities.CommonFunc;
import com.utilities.CommonVariables;
import com.utilities.CommonXpath;

public class FrontContactUS extends FrontContactUSXpath {

	WebDriver driver;
	DropDownHelper dropDownHelper;
	CommonFunc commonFunc;
	CommonVariables commonVariables;
	CommonXpath commonXpath;
	
	public FrontContactUS(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		commonFunc = new CommonFunc(driver);
		commonXpath = new CommonXpath(driver);

		dropDownHelper = new DropDownHelper(driver);
	}

	public void verifyElements() {
		int size = firstNameConatctUs.size();
		assertTrue(size > 0);

		size = lastNameConatctUs.size();
		assertTrue(size > 0);

		size = phoneConatctUs.size();
		assertTrue(size > 0);

		size = emailConatctUs.size();
		assertTrue(size > 0);

		size = messageConatctUs.size();
		assertTrue(size > 0);

		size = recaptchaConatctUs.size();
		assertTrue(size > 0);

		size = submitContactUs.size();
		assertTrue(size > 0);

	}

}
