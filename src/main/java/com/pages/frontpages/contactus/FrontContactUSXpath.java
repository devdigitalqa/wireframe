package com.pages.frontpages.contactus;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class FrontContactUSXpath {

	@FindBy(xpath = "//*[@id='first_name']")
	public List<WebElement> firstNameConatctUs;

	@FindBy(xpath = "//*[@id='last_name']")
	public List<WebElement> lastNameConatctUs;

	@FindBy(xpath = "//*[@id='phone']")
	public List<WebElement> phoneConatctUs;

	@FindBy(xpath = "//*[@id='email']")
	public List<WebElement> emailConatctUs;

	@FindBy(xpath = "//*[@id='message']")
	public List<WebElement> messageConatctUs;

	@FindBy(xpath = "//*[@id='g-recaptcha']")
	public List<WebElement> recaptchaConatctUs;

	@FindBy(xpath = "//input[@type='submit']")
	public List<WebElement> submitContactUs;

}
