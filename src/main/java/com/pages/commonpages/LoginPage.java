package com.pages.commonpages;

import org.json.simple.parser.ParseException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import com.pages.adminpages.HomePage;
import com.utilities.CommonFunc;
import com.utilities.CommonXpath;

public class LoginPage {

	WebDriver driver;
	HomePage homePage;
	CommonFunc commonFunc;
	CommonXpath commonXpath;

	public LoginPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		commonXpath = new CommonXpath(driver);
		commonFunc = new CommonFunc(driver);
	}

	public HomePage login(String un, String pw) throws ParseException {
		commonFunc.urlGet();
		int size = commonXpath.usernameList.size();
		if (size > 0) {
			commonXpath.username.sendKeys(un);
			commonXpath.password.sendKeys(pw);
			commonXpath.login.click();
		}

		return homePage;
	}

}