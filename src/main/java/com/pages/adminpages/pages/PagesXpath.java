package com.pages.adminpages.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class PagesXpath {
	@FindBy(xpath = "//*[@id='title']")
	public WebElement titleTextbox;
	
	@FindBy(xpath = "//*[@id='title']/following::div[1]")
	public WebElement titleErrorMessage;

	@FindBy(xpath = "//*[@id='status']")
	public WebElement statusDropdown;

	@FindBy(xpath = "//*[@id='tinymce']")
	public WebElement pageContentEditor;

	@FindBy(xpath = "//*[@id='DataTables_Table_0']/tbody/tr[1]/td[7]/a")
	public WebElement pagesEditButton;

}
