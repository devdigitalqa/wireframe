package com.pages.adminpages.pages;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.basicactions.DropDownHelper;
import com.basicactions.ExcelHelper;
import com.basicactions.LogHelper;
import com.utilities.CommonFunc;
import com.utilities.CommonVariables;
import com.utilities.CommonXpath;
import com.utilities.FilesPaths;

public class Pages extends PagesXpath {
	WebDriver driver;
	DropDownHelper dropDownHelper;
	CommonFunc commonFunc;
	CommonVariables commonVariables;
	CommonXpath commonXpath;
	private Logger log = LogHelper.getLogger(Pages.class);
	boolean verifyDetails = false;

	public Pages(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		commonFunc = new CommonFunc(driver);
		commonXpath = new CommonXpath(driver);
		dropDownHelper = new DropDownHelper(driver);
	}

	public void enterTitle(String title) {
		log.info("********************Enter the Author Name********************");
		titleTextbox.clear();
		titleTextbox.sendKeys(title);
	}

	public void enterStatus(String status) {
		log.info("********************Enter the Status********************");
		dropDownHelper.selectByVaule(statusDropdown, status);
	}

	public void clickonEditbutton() {
		log.info("********************Click on Edit button********************");
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", pagesEditButton);
	}

	public void enterPageContent(String pageContent) throws InterruptedException {
		log.info("********************Enter the Description********************");
		driver.switchTo().frame("description_ifr");
		pageContentEditor.sendKeys(pageContent);
		driver.switchTo().defaultContent();

		Thread.sleep(2000);
	}

	public void enterMetaTitle(String metaTitle) {
		log.info("********************Enter the Meta Title********************");

		commonXpath.metaTitle.sendKeys(metaTitle);
	}

	public void enterMetaDescription(String metaDescription) {
		log.info("********************Enter the Meta Description********************");

		commonXpath.metaDescription.sendKeys(metaDescription);
	}

	public void fillFormSubmitForm(String moduleName) throws InterruptedException {
		ExcelHelper.readDataFromXLS(FilesPaths.TEMPLATE9WIREFRAME, CommonVariables.PAGES);
		int endvalue = ExcelHelper.getTotalRowsCount();
		for (int i = 8; i < endvalue; i++) {
			String title = ExcelHelper.getData(0, i);
			if (title.isEmpty()) {
				break;
			} else {
				enterTitle(title);
				String status = ExcelHelper.getData(1, i);
				enterStatus(status);
				String pageContent = ExcelHelper.getData(2, i);
				enterPageContent(pageContent);
				String metaTitle = ExcelHelper.getData(3, i);
				enterMetaTitle(metaTitle);
				String metaDescription = ExcelHelper.getData(4, i);
				enterMetaDescription(metaDescription);
				Thread.sleep(2000);
				commonFunc.clickOnSave(moduleName);
				Thread.sleep(2000);
				commonFunc.clickOnAddNewButton(moduleName);
			}
		}
	}

	public void sortingFun() {
		List<WebElement> tdList = commonXpath.tableRowListATag;
		ArrayList<String> list = new ArrayList<>();
		for (int i = 0; i < tdList.size(); i++) {

			list.add(tdList.get(i).getText());
		}
		driver.findElement(By.xpath("//*[@id='DataTables_Table_0']/thead/tr/th[4]")).click();

		commonFunc.checkElementAvailableWithAttributeCompare(commonXpath.elementList, commonXpath.element,
				CommonVariables.STYLE, CommonVariables.DISPLAY_NONE);

		Collections.sort(list, String.CASE_INSENSITIVE_ORDER);

		driver.findElement(By.xpath("//*[@id='DataTables_Table_0']/thead/tr/th[4]")).click();

		commonFunc.checkElementAvailableWithAttributeCompare(commonXpath.elementList, commonXpath.element,
				CommonVariables.STYLE, CommonVariables.DISPLAY_NONE);

		tdList = commonXpath.tableRowListATag;
		ArrayList<String> list1 = new ArrayList<>();

		for (int i = 0; i < tdList.size(); i++) {
			list1.add(tdList.get(i).getText());
		}

		String xpath = "//*[@id='DataTables_Table_0']/thead/tr/th[5]";

		driver.findElement(By.xpath(xpath)).click();

		List<WebElement> tdList2 = driver.findElements(By.xpath("//*[@id='DataTables_Table_0']/tbody/tr/td[5]"));
		ArrayList<String> list2 = new ArrayList<>();
		for (int i = 0; i < tdList2.size(); i++) {

			list2.add(tdList2.get(i).getText());
		}
		driver.findElement(By.xpath(xpath)).click();

		commonFunc.checkElementAvailableWithAttributeCompare(commonXpath.elementList, commonXpath.element,
				CommonVariables.STYLE, CommonVariables.DISPLAY_NONE);

		Collections.sort(list2, String.CASE_INSENSITIVE_ORDER);

		driver.findElement(By.xpath(xpath)).click();

		commonFunc.checkElementAvailableWithAttributeCompare(commonXpath.elementList, commonXpath.element,
				CommonVariables.STYLE, CommonVariables.DISPLAY_NONE);

		tdList2 = driver.findElements(By.xpath("//*[@id='DataTables_Table_0']/tbody/tr/td[5]"));
		ArrayList<String> list3 = new ArrayList<>();
		for (int i = 0; i < tdList2.size(); i++) {
			list3.add(tdList.get(i).getText());
		}
	}

	public void validations() throws InterruptedException {
		commonFunc.validationField("Required Validation", "textbox", titleTextbox, titleErrorMessage,
				"The title field is required.");
	}
}