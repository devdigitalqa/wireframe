package com.pages.adminpages.galleries;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class GalleriesXpath {
	@FindBy(id = "title")
	public WebElement titleTextbox;

	@FindBy(xpath = "//*[@id='alt_image_text']")
	public WebElement imageAltTextbox;

	@FindBy(xpath = "//*[@id='status']")
	public WebElement statusDropdown;
}
