package com.pages.adminpages.galleries;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;

import com.basicactions.DropDownHelper;
import com.basicactions.LogHelper;
import com.utilities.CommonFunc;
import com.utilities.CommonVariables;
import com.utilities.CommonXpath;
import com.utilities.FilesPaths;

public class Galleries extends GalleriesXpath {
	WebDriver driver;
	DropDownHelper dropDownHelper;
	CommonFunc commonFunc;
	CommonVariables commonVariables;
	CommonXpath commonXpath;
	private Logger log = LogHelper.getLogger(Galleries.class);
	boolean verifyDetails = false;

	public Galleries(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		commonFunc = new CommonFunc(driver);
		commonXpath = new CommonXpath(driver);
		dropDownHelper = new DropDownHelper(driver);
	}

	public void enterTitle(String title) {
		log.info("********************Enter the Title********************");
		titleTextbox.clear();
		titleTextbox.sendKeys(title);
	}

	public void clickonEditbutton() throws InterruptedException {
		log.info("********************Click on Edit button********************");
		Thread.sleep(1500);
		List<WebElement> galleryrecord = driver
				.findElements(By.xpath("//*[@id='main']/div[3]/form/div/div[2]/div/div/div/div[1]/div/a[2]"));
		int recordnum = galleryrecord.size();

		Actions actions = new Actions(driver);
		WebElement mousehover = driver.findElement(
				By.xpath("/html/body/div[2]/div[3]/form/div/div[2]/div[" + recordnum + "]/div/div/div[1]"));
		actions.moveToElement(mousehover).perform();
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", driver.findElement(
				By.xpath("//*[@id='main']/div[3]/form/div/div[2]/div[" + recordnum + "]/div/div/div[1]/div/a[2]")));
	}

	public void clickonSettingmenu() {
		log.info("********************Click on Setting Menu********************");
	}

	public void clickonSettingsave() {
		log.info("********************Click on Setting Save Button********************");
		commonXpath.settingSave.click();
	}

	public void enterDescription(String description) {
		log.info("********************Enter the Description********************");
		driver.switchTo().frame("description_ifr");
		commonXpath.blogDetailDescription.sendKeys(description);
		driver.switchTo().defaultContent();
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView();", driver.findElement(By.xpath("//*[@id='meta_title']")));
	}

	public void selectImage(String image) {
		log.info("********************Select the Image********************");
		commonXpath.image.sendKeys(FilesPaths.EXTRA_FILES_FOLDER + image);
		commonFunc.checkElementAvailableWithAttributeCompare(commonXpath.elementList, commonXpath.element,
				CommonVariables.STYLE, CommonVariables.DISPLAY_NONE);
	}

	public void enterImageAlt(String imageAlt) {
		log.info("********************Enter the Image Alt********************");
		commonXpath.galleryImageAlt.clear();
		commonXpath.galleryImageAlt.sendKeys(imageAlt);
	}

	public void enterMetaTitle(String metaTitle) {
		log.info("********************Enter the Meta Title********************");

		commonXpath.metaTitle.sendKeys(metaTitle);

	}

	public void enterMetaDescription(String metaDescription) {
		log.info("********************Enter the Meta Description********************");

		commonXpath.metaDescription.sendKeys(metaDescription);

	}

}
