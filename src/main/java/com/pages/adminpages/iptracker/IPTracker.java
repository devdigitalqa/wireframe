package com.pages.adminpages.iptracker;

import static org.testng.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.basicactions.DropDownHelper;
import com.basicactions.LogHelper;
import com.utilities.CommonFunc;
import com.utilities.CommonVariables;
import com.utilities.CommonXpath;

public class IPTracker extends IPTrackerXpath {

	WebDriver driver;
	DropDownHelper dropDownHelper;
	CommonFunc commonFunc;
	CommonVariables commonVariables;
	CommonXpath commonXpath;
	String value = "value";

	private Logger log = LogHelper.getLogger(IPTracker.class);
	boolean verifyDetails = false;

	public IPTracker(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		commonFunc = new CommonFunc(driver);
		commonXpath = new CommonXpath(driver);

		dropDownHelper = new DropDownHelper(driver);
	}

	public String getContacttitle() {
		return contactTitle.getAttribute(value);
	}

	public String getthankyoumessge() {
		return contactMessage.getAttribute(value);
	}

	public String getcontactemail() {
		return contactEmail.getAttribute(value);
	}

	public void contactSettings(String contacttitle, String contactmessage, String contactemail)
			throws InterruptedException {
		contactSettingMenu.click();
		Thread.sleep(1000);
		entercontactTitle(contacttitle);
		entercontactmessage(contactmessage);
		entercontactemail(contactemail);

		clickonSavebutton();
		assertTrue(contacttitle.equals(getContacttitle()));
		assertTrue(contactmessage.equals(getthankyoumessge()));
		assertTrue(contactemail.equals(getcontactemail()));
	}

	public void entercontactTitle(String contacttitletle) {
		log.info("********************Enter the contacttitletle ********************");
		contactSettingTitle.clear();
		contactSettingTitle.sendKeys(contacttitletle);
	}

	public void entercontactmessage(String contactmessage) {
		log.info("********************Enter the Meta Description********************");
		contactMessage.clear();
		contactMessage.sendKeys(contactmessage);

	}

	public void entercontactemail(String contactemail) {
		log.info("********************Enter the Meta Description********************");
		contactEmail.clear();
		contactEmail.sendKeys(contactemail);
	}

	public void clickonSavebutton() {
		log.info("********************Click on Save button********************");
		contactSave.click();
	}

	public void sortingFun() {
		List<WebElement> tdList = driver.findElements(By.xpath("//*[@id='DataTables_Table_0']/tbody/tr/td[2]"));
		ArrayList<String> list = new ArrayList<>();
		for (int i = 0; i < tdList.size(); i++) {
			list.add(tdList.get(i).getText());
		}
		driver.findElement(By.xpath("//*[@id='DataTables_Table_0']/thead/tr/th[4]")).click();

		commonFunc.checkElementAvailableWithAttributeCompare(commonXpath.elementList, commonXpath.element,
				CommonVariables.STYLE, CommonVariables.DISPLAY_NONE);

		Collections.sort(list, String.CASE_INSENSITIVE_ORDER);

		driver.findElement(By.xpath("//*[@id='DataTables_Table_0']/thead/tr/th[4]")).click();

		commonFunc.checkElementAvailableWithAttributeCompare(commonXpath.elementList, commonXpath.element,
				CommonVariables.STYLE, CommonVariables.DISPLAY_NONE);

		tdList = driver.findElements(By.xpath("//*[@id='DataTables_Table_0']/tbody/tr/td[2]"));
		ArrayList<String> list1 = new ArrayList<>();
		for (int i = 0; i < tdList.size(); i++) {
			list1.add(tdList.get(i).getText());
		}

		ArrayList<String> list2 = new ArrayList<>();
		for (int i = 0; i < tdList.size(); i++) {
			list2.add(tdList.get(i).getText());
		}
		driver.findElement(By.xpath("//*[@id='DataTables_Table_0']/thead/tr/th[5]")).click();

		commonFunc.checkElementAvailableWithAttributeCompare(commonXpath.elementList, commonXpath.element,
				CommonVariables.STYLE, CommonVariables.DISPLAY_NONE);

		Collections.sort(list, String.CASE_INSENSITIVE_ORDER);

		driver.findElement(By.xpath("//*[@id='DataTables_Table_0']/thead/tr/th[5]")).click();

		commonFunc.checkElementAvailableWithAttributeCompare(commonXpath.elementList, commonXpath.element,
				CommonVariables.STYLE, CommonVariables.DISPLAY_NONE);

		List<WebElement> tdList2 = driver.findElements(By.xpath("//*[@id='DataTables_Table_0']/tbody/tr/td[3]"));
		ArrayList<String> list3 = new ArrayList<>();
		for (int i = 0; i < tdList.size(); i++) {
			list3.add(tdList2.get(i).getText());
		}
	}
}
