package com.pages.adminpages.iptracker;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class IPTrackerXpath {
	@FindBy(xpath = "//*[@id='contact_title']")
	public WebElement contactTitle;
	
	@FindBy(xpath = "//*[@id='contact_title']")
	public WebElement contactSettingTitle;

	@FindBy(xpath = "//*[@id='btnsave']")
	public WebElement contactSave;

	@FindBy(xpath = "//*[@id='contact_thank_you_message']")
	public WebElement contactMessage;

	@FindBy(xpath = "//*[@id='contact_email']")
	public WebElement contactEmail;
	
	@FindBy(xpath = "/html/body/nav/ul/li[11]/div/a[2]")
	public WebElement contactSettingMenu;

}
