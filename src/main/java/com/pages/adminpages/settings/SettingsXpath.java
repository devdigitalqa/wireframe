package com.pages.adminpages.settings;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SettingsXpath {
	@FindBy(xpath = "//*[@id='app_name']")
	public WebElement appNameTextbox;

	@FindBy(xpath = "//*[@id='app_name']/following::div[1]")
	public WebElement appNameErrorMessage;

	@FindBy(xpath = "//*[@id='records_per_page']")
	public WebElement noOfRecordsperPageDropdown;

	@FindBy(xpath = "//*[@id='footer_title_for_admin']")
	public WebElement footerTitleforAdminTextbox;

	@FindBy(xpath = "//*[@id='footer_title_for_admin']/following::div[1]")
	public WebElement footerTitleforAdminErrorMessage;

	@FindBy(xpath = "//*[@id='site_title']")
	public WebElement siteTitleTextbox;

	@FindBy(xpath = "//*[@id='site_title']/following::div[1]")
	public WebElement siteTitleErrorMessage;

	@FindBy(xpath = "//*[@id='tagline']")
	public WebElement taglineTextbox;

	@FindBy(xpath = "//*[@id='btnsave']")
	public WebElement saveButton;

	@FindBy(xpath = "//*[@id='copyright_text']")
	public WebElement copyrightTextTextbox;

	@FindBy(xpath = "//*[@id='copyright_text']/following::div[1]")
	public WebElement copyrightTextErrorMessage;

	@FindBy(id = "meta_description_for_front")
	public WebElement metaDescriptionForFront;

	@FindBy(xpath = "//*[@id='company_name']")
	public WebElement companyNameTextbox;

	@FindBy(xpath = "//*[@id='company_name']/following::div[1]")
	public WebElement companyNameErrorMessage;

	@FindBy(xpath = "//*[@id='company_address_line1']")
	public WebElement companyAddress1Textbox;

	@FindBy(xpath = "//*[@id='company_address_line1']/following::div[1]")
	public WebElement companyAddress1ErrorMessage;

	@FindBy(xpath = "//*[@id='company_address_line2']")
	public WebElement companyAddress2Textbox;

	@FindBy(xpath = "//*[@id='company_address_line2']/following::div[1]")
	public WebElement companyAddress2ErrorMessage;

	@FindBy(xpath = "//*[@id='company_city']")
	public WebElement companyCityTextbox;

	@FindBy(xpath = "//*[@id='company_city']/following::div[1]")
	public WebElement companyCityErrorMessage;

	@FindBy(xpath = "//*[@id='company_state']")
	public WebElement companyStateTextbox;

	@FindBy(xpath = "//*[@id='company_state']/following::div[1]")
	public WebElement companyStateErrorMessage;

	@FindBy(xpath = "//*[@id='company_country']")
	public WebElement companyCountryTextbox;

	@FindBy(xpath = "//*[@id='company_country']/following::div[1]")
	public WebElement companyCountryErrorMessage;

	@FindBy(xpath = "//*[@id='company_zipcode']")
	public WebElement companyZipcodeTextbox;

	@FindBy(xpath = "//*[@id='company_zipcode']/following::div[1]")
	public WebElement companyZipcodeErrorMessage;

	@FindBy(xpath = "//*[@id='phone']")
	public WebElement phoneTextbox;

	@FindBy(xpath = "//*[@id='company_email']")
	public WebElement emailTextbox;

	@FindBy(xpath = "//*[@id='from_name']")
	public WebElement fromNameTextbox;
	
	@FindBy(xpath = "//*[@id='from_name']/following::div[1]")
	public WebElement fromNameErrorMessage;

	@FindBy(xpath = "//*[@id='from_email']")
	public WebElement fromEmailTextbox;
	
	@FindBy(xpath = "//*[@id='from_email']/following::div[1]")
	public WebElement fromEmailErrorMessage;

	@FindBy(xpath = "//*[@id='admin_email']")
	public WebElement adminEmailTextbox;
	
	@FindBy(xpath = "//*[@id='admin_email']/following::div[1]")
	public WebElement adminEmailErrorMessage;

	@FindBy(xpath = "//*[@id='login_attempt']")
	public WebElement loginAttemptTextbox;
	
	@FindBy(xpath = "//*[@id='login_attempt']/following::div[1]")
	public WebElement loginAttemptErrorMessage;
	
	@FindBy(xpath = "//*[@id='password_strength']")
	public WebElement passwordStrengthDropdown;
	
	@FindBy(xpath = "//*[@id='minimum_password_length']")
	public WebElement minPWDLengthTextbox;
	
	@FindBy(xpath = "//*[@id='minimum_password_length']/following::div[1]")
	public WebElement minPWDLengthErrorMessage;

	@FindBy(xpath = "//*[@id='user_block_time']")
	public WebElement userBlockTimeTextbox;

	@FindBy(xpath = "//*[@id='user_block_time']/following::div[1]")
	public WebElement userBlockTimeErrorMessage;

	@FindBy(xpath = "//*[@id='facebook_link']")
	public WebElement facebookTextbox;

	@FindBy(xpath = "//*[@id='googlemap_embedded']")
	public WebElement googleMapTextbox;

	@FindBy(xpath = "//*[@id='twitter_link']")
	public WebElement twitterTextbox;

	@FindBy(xpath = "//*[@id='linkedin_link']")
	public WebElement linkedInTextbox;

	@FindBy(xpath = "//*[@id='GOOGLE_RECAPTCHA_KEY']")
	public WebElement googleRecaptchaSiteKeyTextbox;

	@FindBy(xpath = "//*[@id='GOOGLE_RECAPTCHA_SECRET']")
	public WebElement googleRecaptchaSecertKeyTextbox;

	@FindBy(xpath = "//*[@id='robots_meta_tag']")
	public WebElement robotsMetaTagTextbox;

	@FindBy(xpath = "//*[@id='google_analytics_code']")
	public WebElement googleAnalyticsCodeTextbox;

	@FindBy(xpath = "//*[@id='DataTables_Table_0']/tbody/tr[1]/td[7]/a")
	public WebElement settingeditbutton;

	@FindBy(xpath = "//*[@id='site-config-heading-1']/h5/a")
	public WebElement frontEndAccordion;

	@FindBy(xpath = "/html/body/nav/ul/li[11]/div/a[2]")
	public WebElement contactSettingMenu;

	@FindBy(xpath = "//*[@id='site-config-heading-2']/h5/a")
	public WebElement companyAccordion;

	@FindBy(xpath = "//*[@id='site-config-heading-3']/h5/a")
	public WebElement emailAccordion;

	@FindBy(xpath = "//*[@id='site-config-heading-4']/h5/a")
	public WebElement passwordAccordion;

	@FindBy(xpath = "//*[@id='site-config-heading-5']/h5/a")
	public WebElement sociallinkAccordion;

	@FindBy(xpath = "//*[@id='normal_btns']/div")
	public WebElement sliderMessage;

	@FindBy(xpath = "//*[@id='site-config-heading-6']/h5/a")
	public WebElement googleCaptchaAccordion;

	@FindBy(xpath = "//*[@id='site-config-heading-7']/h5/a")
	public WebElement seoAccordion;

	@FindBy(xpath = "//*[@id='company_address_line1']")
	public WebElement companyAddressLine1;

	@FindBy(xpath = "//*[@id='company_address_line2']")
	public WebElement companyAddressLine2;

	@FindBy(xpath = "//*[@id='company_city']")
	public WebElement companyCity;

	@FindBy(xpath = "//*[@id='company_state']")
	public WebElement companyState;

	@FindBy(xpath = "//*[@id='company_zipcode']")
	public WebElement companyZipcode;

	@FindBy(xpath = "//*[@id='company_country']")
	public WebElement companyCountry;

}
