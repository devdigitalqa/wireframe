package com.pages.adminpages.settings;

import static org.testng.Assert.assertTrue;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import com.basicactions.DropDownHelper;
import com.basicactions.LogHelper;
import com.utilities.CommonFunc;
import com.utilities.CommonVariables;
import com.utilities.CommonXpath;

import jxl.write.WriteException;

public class Settings extends SettingsXpath {
	WebDriver driver;
	DropDownHelper dropDownHelper;
	CommonFunc commonFunc;
	CommonVariables commonVariables;
	CommonXpath commonXpath;
	private Logger log = LogHelper.getLogger(Settings.class);
	boolean verifyDetails = false;
	String value = "value";

	public Settings(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		commonFunc = new CommonFunc(driver);
		commonXpath = new CommonXpath(driver);
		dropDownHelper = new DropDownHelper(driver);
	}

	public void enterAppName(String appName) throws InterruptedException {
		log.info("********************Enter the Author Name********************");
		Thread.sleep(2000);
		appNameTextbox.clear();
		appNameTextbox.sendKeys(appName);
	}

	public String getAppName() {
		return appNameTextbox.getAttribute(value);
	}

	public String getnoofRecordsperPage() {
		return noOfRecordsperPageDropdown.getAttribute(value);
	}

	public String getfooterTitleforAdmin() {
		return footerTitleforAdminTextbox.getAttribute(value);
	}

	public String getSiteTitle() {
		return siteTitleTextbox.getAttribute(value);
	}

	public String getCopyrighttext() {
		return copyrightTextTextbox.getAttribute(value);
	}

	public String getTagline() {

		return taglineTextbox.getAttribute(value);
	}

	public String getMetadescriptionforfront() {
		return metaDescriptionForFront.getAttribute(value);
	}

	public String getCompanyName() {
		return companyNameTextbox.getAttribute(value);
	}

	public String getAddressLine1() {
		return companyAddressLine1.getAttribute(value);
	}

	public String getAddressLine2() {
		return companyAddressLine2.getAttribute(value);
	}

	public String getCity() {
		return companyCity.getAttribute(value);
	}

	public String getState() {
		return companyState.getAttribute(value);
	}

	public String getCountry() {
		return companyCountry.getAttribute(value);
	}

	public String getZipcode() {
		return companyZipcode.getAttribute(value);
	}

	public String getPhone() {
		return phoneTextbox.getAttribute(value);
	}

	public String getEmail() {
		return emailTextbox.getAttribute(value);
	}

	public String getFromName() {
		return fromNameTextbox.getAttribute(value);
	}

	public String getFromEmail() {
		return fromEmailTextbox.getAttribute(value);
	}

	public String getAdminEmail() {
		return adminEmailTextbox.getAttribute(value);
	}

	public String getLoginattempt() {
		return loginAttemptTextbox.getAttribute(value);
	}

	public String getPasswordStrength() {
		return passwordStrengthDropdown.getAttribute(value);
	}

	public String getMinpwdlength() {
		return minPWDLengthTextbox.getAttribute(value);
	}

	public String getUserBlockTime() {
		return userBlockTimeTextbox.getAttribute(value);
	}

	public String getFacebook() {
		return facebookTextbox.getAttribute(value);
	}

	public String getTwitter() {
		return twitterTextbox.getAttribute(value);
	}

	public String getLinkedIn() {
		return linkedInTextbox.getAttribute(value);
	}

	public String getGooglemap() {
		return googleMapTextbox.getAttribute(value);
	}

	public String getGoogleRecaptchaSiteKey() {
		return googleRecaptchaSiteKeyTextbox.getAttribute(value);
	}

	public String getGoogleRecaptchaSecertKey() {
		return googleRecaptchaSecertKeyTextbox.getAttribute(value);
	}

	public String getRobotsMetaTag() {
		return robotsMetaTagTextbox.getAttribute(value);
	}

	public String getGoogleAnalyticsCode() {
		return googleAnalyticsCodeTextbox.getAttribute(value);
	}

	public void backupAdminSettings() throws WriteException, IOException {
		String adminSettings = "Admin Settings";
		commonFunc.settingsBackup(adminSettings, "App Name", getAppName());
		commonFunc.settingsBackup(adminSettings, "No of Records per Page", getnoofRecordsperPage());
		commonFunc.settingsBackup(adminSettings, "Footer Title for Admin", getfooterTitleforAdmin());
	}

	public void backupFrontEndSettings() throws WriteException, IOException {
		String adminSettings = "Front End Settings";
		commonFunc.settingsBackup(adminSettings, "Site Title", getSiteTitle());
		commonFunc.settingsBackup(adminSettings, "Tagline", getTagline());
		commonFunc.settingsBackup(adminSettings, "CopyRight Text", getCopyrighttext());
		commonFunc.settingsBackup(adminSettings, "Meta Description for Front", getMetadescriptionforfront());
	}

	public void backupCompanyDetailsSettings() throws WriteException, IOException {
		String adminSettings = "Company Detaila Settings";
		commonFunc.settingsBackup(adminSettings, "Company Name", getCompanyName());
		commonFunc.settingsBackup(adminSettings, "Address Line1", getAddressLine1());
		commonFunc.settingsBackup(adminSettings, "Address Line2", getAddressLine2());
		commonFunc.settingsBackup(adminSettings, "City", getCity());
		commonFunc.settingsBackup(adminSettings, "State", getState());
		commonFunc.settingsBackup(adminSettings, "Country", getCountry());
		commonFunc.settingsBackup(adminSettings, "Zipcode", getZipcode());
		commonFunc.settingsBackup(adminSettings, "Phone", getPhone());
		commonFunc.settingsBackup(adminSettings, "Email", getEmail());
		commonFunc.settingsBackup(adminSettings, "Google Map", getGooglemap());
	}

	public void backupEmailSettings() throws WriteException, IOException {
		String adminSettings = "Email Settings";
		commonFunc.settingsBackup(adminSettings, "From Name", getFromName());
		commonFunc.settingsBackup(adminSettings, "From Email", getFromEmail());
		commonFunc.settingsBackup(adminSettings, "Admin Name", getAdminEmail());
	}

	public void adminSettings(String appName, String footerTitleforAdmin) throws InterruptedException {
		enterAppName(appName);
//		enterNoofRecordsperPage(noofRecordsperPage);
		enterFooterTitleforAdmin(footerTitleforAdmin);

		clickonSavebutton();
		assertTrue(appName.equals(getAppName()));
//		assertTrue(noofRecordsperPage.equals(getnoofRecordsperPage()));
		assertTrue(footerTitleforAdmin.equals(getfooterTitleforAdmin()));
	}

	public void frontEndSettings(String sitetitle, String tagline, String copyrightText, String metaDescription)
			throws InterruptedException {
		frontEndAccordion.click();
		Thread.sleep(1000);
		enterSiteTitle(sitetitle);
		enterTagline(tagline);
		enterCopyrighttext(copyrightText);
		enterMetadescriptionforfront(metaDescription);

		clickonSavebutton();
		assertTrue(sitetitle.equals(getSiteTitle()));
		assertTrue(tagline.equals(getTagline()));
		assertTrue(copyrightText.equals(getCopyrighttext()));
		assertTrue(metaDescription.equals(getMetadescriptionforfront()));
	}

	public void companySettings(String companyName, String addressline1, String addressline2, String city, String state,
			String country, String zipcode) throws InterruptedException {
		companyAccordion.click();
		Thread.sleep(1000);
		enterCompanyName(companyName);
		enterAddressLine1(addressline1);
		enterAddressLine2(addressline2);
		enterCity(city);
		enterState(state);
		enterCountry(country);
		enterZipcode(zipcode);

		clickonSavebutton();
		assertTrue(companyName.equals(getCompanyName()));
		assertTrue(addressline1.equals(getAddressLine1()));
		assertTrue(addressline2.equals(getAddressLine2()));
		assertTrue(city.equals(getCity()));
		assertTrue(state.equals(getState()));
		assertTrue(country.equals(getCountry()));
		assertTrue(zipcode.equals(getZipcode()));
	}

	public void companySettings(String phone, String email) throws InterruptedException {
		companyAccordion.click();
		Thread.sleep(1000);
		enterPhone(phone);
		enterEmail(email);

		clickonSavebutton();
		assertTrue(phone.equals(getPhone()));
		assertTrue(email.equals(getEmail()));
	}

	public void emailSettings(String fromName, String fromEmail, String adminEmail) throws InterruptedException {
		emailAccordion.click();
		Thread.sleep(1000);
		enterFromName(fromName);
		enterFromEmail(fromEmail);
		enterAdminEmail(adminEmail);

		clickonSavebutton();
		assertTrue(fromName.equals(getFromName()));
		assertTrue(fromEmail.equals(getFromEmail()));
		assertTrue(adminEmail.equals(getAdminEmail()));
	}

	public void seoSettings() {
		String robotMetaTag = driver.findElement(By.xpath("//*[@id=\"robots_meta_tag\"]")).getAttribute("value").trim();
		assertTrue(driver.getPageSource().contains(robotMetaTag));
	}

	public void passwordSettings(String passwordStrength, String loginattempt, String minpwdlength,
			String userBlockTime) throws InterruptedException {
		passwordAccordion.click();
		Thread.sleep(1000);
		enterPasswordStrength(passwordStrength);
		enterLoginattempt(loginattempt);
		enterMinpwdlength(minpwdlength);
		enterUserBlockTime(userBlockTime);

		clickonSavebutton();
		assertTrue(passwordStrength.equals(getPasswordStrength()));
		assertTrue(loginattempt.equals(getLoginattempt()));
		assertTrue(minpwdlength.equals(getMinpwdlength()));
		assertTrue(userBlockTime.equals(getUserBlockTime()));
	}

	public void sociallinkSettings(String facebook, String twitter, String linkedIn) throws InterruptedException {
		sociallinkAccordion.click();
		Thread.sleep(1000);
		enterFacebook(facebook);
		enterTwitter(twitter);
		enterLinkedIn(linkedIn);

		clickonSavebutton();
		assertTrue(facebook.equals(getFacebook()));
		assertTrue(twitter.equals(getTwitter()));
		assertTrue(linkedIn.equals(getLinkedIn()));
	}

	public void googlecaptchaSettings(String googleRecaptchaSiteKey, String googleRecaptchaSecertKey)
			throws InterruptedException {
		googleCaptchaAccordion.click();
		Thread.sleep(1000);
		enterGoogleRecaptchaSiteKey(googleRecaptchaSiteKey);
		enterGoogleRecaptchaSecertKey(googleRecaptchaSecertKey);

		clickonSavebutton();
		assertTrue(googleRecaptchaSiteKey.equals(getGoogleRecaptchaSiteKey()));
		assertTrue(googleRecaptchaSecertKey.equals(getGoogleRecaptchaSecertKey()));
	}

	public void seoSettings(String googleAnalyticsCode, String robotsMetaTag) throws InterruptedException {
		seoAccordion.click();
		Thread.sleep(1000);
		enterGoogleAnalyticsCode(googleAnalyticsCode);
		enterRobotsMetaTag(robotsMetaTag);

		clickonSavebutton();
		assertTrue(googleAnalyticsCode.equals(getGoogleAnalyticsCode()));
		assertTrue(robotsMetaTag.equals(getRobotsMetaTag()));
	}

	public void enterNoofRecordsperPage(String noofRecordsperPage) {
		log.info("********************Enter the Status********************");
		dropDownHelper.selectVisibleText(noOfRecordsperPageDropdown, noofRecordsperPage);

	}

	public void clickonEditbutton() {
		log.info("********************Click on Edit button********************");
		settingeditbutton.click();
	}

	public void clickonSavebutton() {
		log.info("********************Click on Save button********************");
		saveButton.click();
		commonFunc.checkElementAvailableWithAttributeCompare(commonXpath.elementList, commonXpath.element,
				CommonVariables.STYLE, CommonVariables.DISPLAY_NONE);
	}

	public void enterFooterTitleforAdmin(String footerTitleforAdmin) throws InterruptedException {
		log.info("********************Enter the Description********************");
		footerTitleforAdminTextbox.clear();
		footerTitleforAdminTextbox.sendKeys(footerTitleforAdmin);
		Thread.sleep(2000);
	}

	public void enterSiteTitle(String sitetitle) {
		log.info("********************Enter the Meta Title********************");
		siteTitleTextbox.clear();
		siteTitleTextbox.sendKeys(sitetitle);
	}

	public void enterTagline(String tagline) {
		log.info("********************Enter the Tag line********************");
		taglineTextbox.clear();
		taglineTextbox.sendKeys(tagline);
	}

	public void enterCopyrighttext(String copyrighttext) {
		log.info("********************Enter the Copy Right Text********************");
		copyrightTextTextbox.clear();
		copyrightTextTextbox.sendKeys(copyrighttext);
	}

	public void enterMetadescriptionforfront(String metadescriptionforfront) {
		log.info("********************Enter the Meta Description For Front********************");
		metaDescriptionForFront.clear();
		metaDescriptionForFront.sendKeys(metadescriptionforfront);
	}

	public void enterCompanyName(String companyName) {
		log.info("********************Enter the Company name********************");
		companyNameTextbox.clear();
		companyNameTextbox.sendKeys(companyName);
	}

	public void enterAddressLine1(String addressline1) {
		log.info("********************Enter the Address line 1********************");
		companyAddress1Textbox.clear();
		companyAddress1Textbox.sendKeys(addressline1);
	}

	public void enterAddressLine2(String addressline2) {
		log.info("********************Enter the Address line 2********************");
		companyAddress2Textbox.clear();
		companyAddress2Textbox.sendKeys(addressline2);
	}

	public void enterCity(String city) {
		log.info("********************Enter the City********************");
		companyCityTextbox.clear();
		companyCityTextbox.sendKeys(city);
	}

	public void enterState(String state) {
		log.info("********************Enter the State********************");
		companyStateTextbox.clear();
		companyStateTextbox.sendKeys(state);
	}

	public void enterCountry(String country) {
		log.info("********************Enter the Country********************");
		companyCountryTextbox.clear();
		companyCountryTextbox.sendKeys(country);
	}

	public void enterZipcode(String zipcode) {
		log.info("********************Enter the Zipcode********************");
		companyZipcodeTextbox.clear();
		companyZipcodeTextbox.sendKeys(zipcode);
	}

	public void enterPhone(String phone) {
		log.info("********************Enter the Phone********************");
		phoneTextbox.clear();
		phoneTextbox.sendKeys(phone);
	}

	public void enterEmail(String email) {
		log.info("********************Enter the Email********************");
		emailTextbox.clear();
		emailTextbox.sendKeys(email);
	}

	public void enterFromName(String fromName) {
		log.info("********************Enter the From Name********************");
		fromNameTextbox.clear();
		fromNameTextbox.sendKeys(fromName);
	}

	public void enterFromEmail(String fromEmail) {
		log.info("********************Enter the From Email********************");
		fromEmailTextbox.clear();
		fromEmailTextbox.sendKeys(fromEmail);
	}

	public void enterAdminEmail(String adminEmail) {
		log.info("********************Enter the Admin Email********************");
		adminEmailTextbox.clear();
		adminEmailTextbox.sendKeys(adminEmail);
	}

	public void enterPasswordStrength(String passwordStrength) {
		log.info("********************Enter the Password Strength********************");
		dropDownHelper.selectVisibleText(passwordStrengthDropdown, passwordStrength);
	}

	public void enterLoginattempt(String loginattempt) {
		log.info("********************Enter the Login Attempt********************");
		loginAttemptTextbox.clear();
		loginAttemptTextbox.sendKeys(loginattempt);
	}

	public void enterMinpwdlength(String minpwdlength) {
		log.info("********************Enter the Min pwd length********************");
		minPWDLengthTextbox.clear();
		minPWDLengthTextbox.sendKeys(minpwdlength);
	}

	public void enterUserBlockTime(String userBlockTime) {
		log.info("********************Enter the User Block Time********************");
		userBlockTimeTextbox.clear();
		userBlockTimeTextbox.sendKeys(userBlockTime);
	}

	public void enterFacebook(String facebook) {
		log.info("********************Enter the Facebook********************");
		facebookTextbox.clear();
		facebookTextbox.sendKeys(facebook);
	}

	public void enterTwitter(String twitter) {
		log.info("********************Enter the Twitter********************");
		twitterTextbox.clear();
		twitterTextbox.sendKeys(twitter);
	}

	public void enterLinkedIn(String linkedIn) {
		log.info("********************Enter the Linkedin********************");
		linkedInTextbox.clear();
		linkedInTextbox.sendKeys(linkedIn);
	}

	public void enterGoogleRecaptchaSiteKey(String googleRecaptchaSiteKey) {
		log.info("********************Enter the Google Recaptcha Site key********************");
		googleRecaptchaSiteKeyTextbox.clear();
		googleRecaptchaSiteKeyTextbox.sendKeys(googleRecaptchaSiteKey);
	}

	public void enterGoogleRecaptchaSecertKey(String googleRecaptchaSecertKey) {
		log.info("********************Enter the Google Recaptcha Secret Key********************");
		googleRecaptchaSecertKeyTextbox.clear();
		googleRecaptchaSecertKeyTextbox.sendKeys(googleRecaptchaSecertKey);
	}

	public void enterGooglemap(String googlemap) {
		log.info("********************Enter the Google Map********************");
		googleMapTextbox.clear();
		googleMapTextbox.sendKeys(googlemap);
	}

	public void enterRobotsMetaTag(String robotsMetaTag) {
		log.info("********************Enter the Robot Meta tag********************");
		robotsMetaTagTextbox.clear();
		robotsMetaTagTextbox.sendKeys(robotsMetaTag);
	}

	public void enterGoogleAnalyticsCode(String googleAnalyticsCode) {
		log.info("********************Enter the Google Analytics Code********************");
		googleAnalyticsCodeTextbox.clear();
		googleAnalyticsCodeTextbox.sendKeys(googleAnalyticsCode);
	}

	public void entermissionStatement(String misssionstatement) throws InterruptedException {
		log.info("********************Enter the Author Name********************");
		Thread.sleep(2000);
		commonXpath.about_us_mission_statement.clear();
		commonXpath.about_us_mission_statement.sendKeys(misssionstatement);
	}

	public String getmissionStatement() {
		return commonXpath.about_us_mission_statement.getAttribute(value);
	}

	public void validations() throws InterruptedException {
		String requiredValidation = "Required Validation";
		String emailValidation = "Email Validation";
		String numberValidation = "Number Validation";
		String textbox = "textbox";
		commonFunc.validationField(requiredValidation, textbox, appNameTextbox, appNameErrorMessage,
				"The app name field is required.");
		commonFunc.validationField(requiredValidation, textbox, footerTitleforAdminTextbox,
				footerTitleforAdminErrorMessage, "The footer title for admin field is required.");

		frontEndAccordion.click();
		commonFunc.validationField(requiredValidation, textbox, siteTitleTextbox, siteTitleErrorMessage,
				"The site title field is required.");
		commonFunc.validationField(requiredValidation, textbox, copyrightTextTextbox, copyrightTextErrorMessage,
				"The copyright text field is required.");

		companyAccordion.click();
		commonFunc.validationField(requiredValidation, textbox, companyNameTextbox, companyNameErrorMessage,
				"The company name field is required.");
		commonFunc.validationField(requiredValidation, textbox, companyAddress1Textbox, companyAddress1ErrorMessage,
				"The company address line1 field is required.");
		commonFunc.validationField(requiredValidation, textbox, companyCityTextbox, companyCityErrorMessage,
				"The company city field is required.");
		commonFunc.validationField(requiredValidation, textbox, companyStateTextbox, companyStateErrorMessage,
				"The company state field is required.");
		commonFunc.validationField(requiredValidation, textbox, companyCountryTextbox, companyCountryErrorMessage,
				"The company country field is required.");
		commonFunc.validationField(requiredValidation, textbox, companyZipcodeTextbox, companyZipcodeErrorMessage,
				"The company zipcode field is required.");

		emailAccordion.click();
		commonFunc.validationField(requiredValidation, textbox, fromNameTextbox, fromNameErrorMessage,
				"The from name field is required.");
		commonFunc.validationField(requiredValidation, textbox, fromEmailTextbox, fromEmailErrorMessage,
				"The from email field is required.");
		commonFunc.validationField(emailValidation, textbox, fromEmailTextbox, fromEmailErrorMessage,
				"The from email must be a valid email address.");
		commonFunc.validationField(requiredValidation, textbox, adminEmailTextbox, adminEmailErrorMessage,
				"The admin email field is required.");
		commonFunc.validationField(emailValidation, textbox, fromEmailTextbox, fromEmailErrorMessage,
				"The admin email must be a valid email address.");

		passwordAccordion.click();
		commonFunc.validationField(requiredValidation, textbox, loginAttemptTextbox, loginAttemptErrorMessage,
				"The login attempt field is required.");
		commonFunc.validationField(numberValidation, textbox, loginAttemptTextbox, loginAttemptErrorMessage,
				"The login attempt must be a number.");
		commonFunc.validationField(requiredValidation, textbox, minPWDLengthTextbox, minPWDLengthErrorMessage,
				"The minimum password length field is required.");
		commonFunc.validationField(numberValidation, textbox, minPWDLengthTextbox, minPWDLengthErrorMessage,
				"The minimum password length must be a number.");
		commonFunc.validationField(requiredValidation, textbox, userBlockTimeTextbox, userBlockTimeErrorMessage,
				"The user block time field is required.");
		commonFunc.validationField(numberValidation, textbox, userBlockTimeTextbox, userBlockTimeErrorMessage,
				"The user block time must be a number.");
	}

}
