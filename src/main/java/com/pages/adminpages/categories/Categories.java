package com.pages.adminpages.categories;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.basicactions.DropDownHelper;
import com.basicactions.ExcelHelper;
import com.basicactions.LogHelper;
import com.utilities.CommonFunc;
import com.utilities.CommonVariables;
import com.utilities.CommonXpath;
import com.utilities.FilesPaths;

public class Categories extends CategoriesXpath {
	WebDriver driver;
	DropDownHelper dropDownHelper;
	CommonFunc commonFunc;
	CommonVariables commonVariables;
	CommonXpath commonXpath;
	private Logger log = LogHelper.getLogger(Categories.class);
	boolean verifyDetails = false;

	public Categories(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		commonFunc = new CommonFunc(driver);
		commonXpath = new CommonXpath(driver);
		dropDownHelper = new DropDownHelper(driver);
	}

	public void enterTitle(String title) {
		log.info("********************Enter the Title********************");
		titleTextbox.clear();
		titleTextbox.sendKeys(title);
	}

	public void enterStatus(String status) {
		log.info("********************Enter the Status********************");
		dropDownHelper.selectByVaule(commonXpath.status, status);
	}

	public void clickonEditbutton() throws InterruptedException {
		log.info("********************Click on Edit button********************");
		Thread.sleep(3000);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", commonXpath.categorieseditbutton);
	}

	public void enterDescription(String description) throws InterruptedException {
		log.info("********************Enter the Description********************");
		driver.switchTo().frame("description_ifr");
		commonXpath.descriptionEditor.sendKeys(description);
		driver.switchTo().defaultContent();
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView();", driver.findElement(By.xpath("//*[@id='meta_title']")));
		Thread.sleep(2000);
	}

	public void enterImage(String image) throws InterruptedException {
		log.info("********************Enter the Image********************");
		Thread.sleep(3000);
		commonXpath.image.sendKeys(FilesPaths.EXTRA_FILES_FOLDER + image);
		commonFunc.checkElementAvailableWithAttributeCompare(commonXpath.elementList, commonXpath.element, "style",
				"display: none;");
	}

	public void enterImageAlt(String imageAlt) throws InterruptedException {
		log.info("********************Enter the Image Alt********************");
		commonXpath.imageAlt.sendKeys(imageAlt);
		Thread.sleep(2000);

	}

	public void enterMetaTitle(String metaTitle) {
		log.info("********************Enter the Meta Title********************");

		commonXpath.metaTitle.sendKeys(metaTitle);

	}

	public void enterMetaDescription(String metaDescription) {
		log.info("********************Enter the Meta Description********************");

		commonXpath.categoriesMetadesc.sendKeys(metaDescription);

	}

	public void fillFormSubmitForm(String moduleName) throws InterruptedException {
		ExcelHelper.readDataFromXLS(FilesPaths.TEMPLATE9WIREFRAME, CommonVariables.CATEGORIES);
		int categoriesendvalue = ExcelHelper.getTotalRowsCount();
		for (int i = 10; i < categoriesendvalue; i++) {
			String title = ExcelHelper.getData(0, i);
			if (title.isEmpty()) {
				break;
			} else {
				enterTitle(title);
				String status = ExcelHelper.getData(1, i);
				enterStatus(status);
				String imageAlt = ExcelHelper.getData(2, i);
				enterImageAlt(imageAlt);
				String image = ExcelHelper.getData(3, i);
				enterImage(image);
				String description = ExcelHelper.getData(4, i);
				enterDescription(description);
				Thread.sleep(2000);
				commonFunc.clickOnSave(moduleName);
				Thread.sleep(2000);
				commonFunc.clickOnAddNewButton(moduleName);
			}
		}
	}

	public void sortingFun() {
		driver.findElement(By.xpath("//*[@id='DataTables_Table_0']/thead/tr/th[4]")).click();
		commonFunc.checkElementAvailableWithAttributeCompare(commonXpath.elementList, commonXpath.element,
				CommonVariables.STYLE, CommonVariables.DISPLAY_NONE);

		List<WebElement> tdList = commonXpath.tableRowListATag;
		ArrayList<String> list = new ArrayList<>();
		for (int i = 0; i < tdList.size(); i++) {
			list.add(tdList.get(i).getText());
		}

		Collections.sort(list, String.CASE_INSENSITIVE_ORDER);

		driver.findElement(By.xpath("//*[@id='DataTables_Table_0']/thead/tr/th[4]")).click();

		commonFunc.checkElementAvailableWithAttributeCompare(commonXpath.elementList, commonXpath.element,
				CommonVariables.STYLE, CommonVariables.DISPLAY_NONE);

		tdList = commonXpath.tableRowListATag;
		ArrayList<String> list1 = new ArrayList<>();
		for (int i = 0; i < tdList.size(); i++) {
			list1.add(tdList.get(i).getText());
		}

		driver.findElement(By.xpath("//*[@id='DataTables_Table_0']/thead/tr/th[5]")).click();
		commonFunc.checkElementAvailableWithAttributeCompare(commonXpath.elementList, commonXpath.element,
				CommonVariables.STYLE, CommonVariables.DISPLAY_NONE);

		List<WebElement> tdList2 = driver.findElements(By.xpath("//*[@id='DataTables_Table_0']/tbody/tr/td[5]"));
		ArrayList<String> list2 = new ArrayList<>();
		for (int i = 0; i < tdList2.size(); i++) {
			list2.add(tdList2.get(i).getText());
		}

		Collections.sort(list2, String.CASE_INSENSITIVE_ORDER);

		driver.findElement(By.xpath("//*[@id='DataTables_Table_0']/thead/tr/th[5]")).click();

		commonFunc.checkElementAvailableWithAttributeCompare(commonXpath.elementList, commonXpath.element,
				CommonVariables.STYLE, CommonVariables.DISPLAY_NONE);

		tdList2 = driver.findElements(By.xpath("//*[@id='DataTables_Table_0']/tbody/tr/td[5]"));
		ArrayList<String> list3 = new ArrayList<>();
		for (int i = 0; i < tdList2.size(); i++) {
			list3.add(tdList2.get(i).getText());
		}
	}

	public void validations() throws InterruptedException {
		commonFunc.validationField("Required Validation", "textbox", titleTextbox, titleErrorMessage,
				"The title field is required.");
	}
}
