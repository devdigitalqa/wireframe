package com.pages.adminpages.categories;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CategoriesXpath {
	@FindBy(xpath = "//*[@id='title']")
	public WebElement titleTextbox;

	@FindBy(xpath = "//*[@id='title']/following::div[1]")
	public WebElement titleErrorMessage;

	@FindBy(xpath = "//*[@id='parent_category']")
	public WebElement blogCategory;

	@FindBy(id = "author_first_name")
	public WebElement authorFirstName;

	@FindBy(id = "author_last_name")
	public WebElement authorLastName;

	@FindBy(id = "publish_date")
	public WebElement publishDate;

	@FindBy(xpath = "//*[@id='DataTables_Table_0']/tbody/tr[1]/td[10]/a")
	public WebElement blogEditButton;
	
	@FindBy(xpath = "/html/body/nav/ul/li[7]/div/a[2]")
	public WebElement blogSettingMenu;
	
	@FindBy(xpath = "//*[@id='btnsave']")
	public WebElement settingSave;
	
	@FindBy(xpath = "//body[@id='tinymce']")
	public WebElement descriptionEditor;
	
	@FindBy(xpath = "//input[@type='file']")
	public WebElement imageUpload;

	@FindBy(xpath = "//*[@id='frmaddedit']/div[2]/div/div[7]/div[2]/div[3]/table/tbody/tr[3]/td[2]")
	public WebElement blogselectdate;

	@FindBy(xpath = "//*[@id='frmaddedit']/div[2]/div/div[7]/div[2]/div[2]/table/tbody/tr/td/fieldset[2]/span[5]")
	public WebElement selectTime;
	
	@FindBy(xpath = "//*[@id='comment_moderation']/option[1]")
	public WebElement commentModeration;
	
	@FindBy(xpath = "//*[@id='status']")
	public WebElement status;
	
	@FindBy(xpath = "//*[@id='field_blog_per_page_display']")
	public WebElement fieldBlog;
	
	@FindBy(xpath = "//*[@id='frmaddedit']/div[2]/div/div[7]/div[2]/div[1]/table/tbody/tr/td/fieldset/span[11]")
	public WebElement time;
	
	@FindBy(xpath = "//*[@id='main_image_alt']")
	public WebElement blogImageAlt;

	@FindBy(xpath = "//*[@id='DataTables_Table_0']/tbody/tr[1]/td[6]")
	public WebElement date;

}
