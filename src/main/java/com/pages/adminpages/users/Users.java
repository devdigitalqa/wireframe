package com.pages.adminpages.users;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.base.Frame;
import com.basicactions.DropDownHelper;
import com.basicactions.ExcelHelper;
import com.basicactions.LogHelper;
import com.utilities.CommonFunc;
import com.utilities.CommonVariables;
import com.utilities.CommonXpath;
import com.utilities.FilesPaths;

public class Users extends UsersXpath {

	WebDriver driver;
	DropDownHelper dropDownHelper;
	CommonFunc commonFunc;
	CommonVariables commonVariables;
	CommonXpath commonXpath;

	private Logger log = LogHelper.getLogger(Users.class);
	boolean verifyDetails = false;

	public Users(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		commonFunc = new CommonFunc(driver);
		commonXpath = new CommonXpath(driver);
		dropDownHelper = new DropDownHelper(driver);
	}

	public void enterUserFirstName(String firstName) {
		log.info("********************Enter User First Name********************");
		firstNameTextbox.clear();
		firstNameTextbox.sendKeys(firstName);
		firstNameTextbox.sendKeys(Keys.TAB);
	}

	public void enterUserLastName(String lastName) {
		log.info("********************Enter User Last Name********************");
		lastNameTextbox.clear();
		lastNameTextbox.sendKeys(lastName);
		lastNameTextbox.sendKeys(Keys.TAB);
	}

	public void enterUserEmail(String email) {
		log.info("********************Enter User Email********************");
		userEmailTextbox.clear();
		userEmailTextbox.sendKeys(email);
		userEmailTextbox.sendKeys(Keys.TAB);
	}

	public void enterchangeFirstName(String changefirstname) throws InterruptedException {
		log.info("********************Enter User First Name********************");
		firstNameTextbox.clear();
		firstNameTextbox.sendKeys(changefirstname);
		Thread.sleep(3000);
	}

	public void enterchangeLastName(String changelastName) throws InterruptedException {
		log.info("********************Enter User Last Name********************");
		lastNameTextbox.clear();
		lastNameTextbox.sendKeys(changelastName);
		Thread.sleep(3000);

	}

	public void fillFormSubmitForm(String moduleName) throws InterruptedException {
		ExcelHelper.readDataFromXLS(FilesPaths.TEMPLATE9WIREFRAME, CommonVariables.USERS);
		int endvalue = ExcelHelper.getTotalRowsCount();
		for (int i = 8; i < endvalue; i++) {
			String firstname = ExcelHelper.getData(1, i);
			if (firstname.isEmpty()) {
				break;
			}
			enterUserFirstName(firstname);
			String lastname = ExcelHelper.getData(3, i);
			enterUserLastName(lastname);
			String email = ExcelHelper.getData(5, i);
			enterUserEmail(email);
			Frame.commonVariables.setEmail(email);
			Thread.sleep(2000);
			commonFunc.clickOnSave(moduleName);
			Thread.sleep(2000);
			commonFunc.clickOnAddNewButton(moduleName);

		}
	}

	public void sortingFun() {
		driver.findElement(By.xpath("//*[@id='DataTables_Table_0']/thead/tr/th[4]")).click();

		commonFunc.checkElementAvailableWithAttributeCompare(commonXpath.elementList, commonXpath.element,
				CommonVariables.STYLE, CommonVariables.DISPLAY_NONE);
		List<WebElement> tdList = commonXpath.tableRowList;
		ArrayList<String> list = new ArrayList<>();
		for (int i = 0; i < tdList.size(); i++) {
			list.add(tdList.get(i).getText());
		}

		Collections.sort(list, String.CASE_INSENSITIVE_ORDER);

		driver.findElement(By.xpath("//*[@id='DataTables_Table_0']/thead/tr/th[4]")).click();

		commonFunc.checkElementAvailableWithAttributeCompare(commonXpath.elementList, commonXpath.element,
				CommonVariables.STYLE, CommonVariables.DISPLAY_NONE);

		tdList = commonXpath.tableRowList;
		ArrayList<String> list1 = new ArrayList<>();
		for (int i = 0; i < tdList.size(); i++) {
			list1.add(tdList.get(i).getText());
		}

		driver.findElement(By.xpath("//*[@id='DataTables_Table_0']/thead/tr/th[5]")).click();
		commonFunc.checkElementAvailableWithAttributeCompare(commonXpath.elementList, commonXpath.element,
				CommonVariables.STYLE, CommonVariables.DISPLAY_NONE);
		List<WebElement> tdList2 = driver.findElements(By.xpath("//*[@id='DataTables_Table_0']/tbody/tr/td[5]"));
		ArrayList<String> list2 = new ArrayList<>();
		for (int i = 0; i < tdList2.size(); i++) {
			list2.add(tdList2.get(i).getText());
		}

		Collections.sort(list2, String.CASE_INSENSITIVE_ORDER);

		driver.findElement(By.xpath("//*[@id='DataTables_Table_0']/thead/tr/th[5]")).click();

		commonFunc.checkElementAvailableWithAttributeCompare(commonXpath.elementList, commonXpath.element,
				CommonVariables.STYLE, CommonVariables.DISPLAY_NONE);

		tdList2 = commonXpath.tableRowList;
		ArrayList<String> list3 = new ArrayList<>();
		for (int i = 0; i < tdList2.size(); i++) {
			list3.add(tdList2.get(i).getText());
		}
	}

	public void validations() throws InterruptedException {
		String textbox = "textbox";
		commonFunc.validationField("Required Validation", textbox, firstNameTextbox, firstNameErrorMessage,
				"The first name field is required.");
		commonFunc.validationField("Required Validation", textbox, userEmailTextbox, userEmailErrorMessage,
				"The email field is required.");
		commonFunc.validationField("Email Validation", textbox, userEmailTextbox, userEmailErrorMessage,
				"The email must be a valid email address.");
	}
}
