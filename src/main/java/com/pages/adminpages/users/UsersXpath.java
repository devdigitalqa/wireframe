package com.pages.adminpages.users;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class UsersXpath {
	@FindBy(xpath = "//*[@id='first_name']")
	public WebElement firstNameTextbox;

	@FindBy(xpath = "//*[@id='first_name']/following::div[1]")
	public WebElement firstNameErrorMessage;
	
	@FindBy(xpath = "//*[@id='last_name']")
	public WebElement lastNameTextbox;
	
	@FindBy(xpath = "//*[@id='email']")
	public WebElement userEmailTextbox;

	@FindBy(xpath = "//*[@id='email']/following::div[1]")
	public WebElement userEmailErrorMessage;

	@FindBy(xpath = "//*[@id='status']")
	public WebElement statusDropdown;

}
