package com.pages.adminpages.changepassword;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ChangepasswordXpath {
	@FindBy(xpath = "//*[@id='current-password']")
	public WebElement currentPWD;
	
	@FindBy(xpath = "//*[@id='new-password']")
	public WebElement newPWD;

	@FindBy(xpath = "//*[@id='password-confirm']")
	public WebElement passwordConfirm;
}
