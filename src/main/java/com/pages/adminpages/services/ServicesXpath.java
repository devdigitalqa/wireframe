package com.pages.adminpages.services;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ServicesXpath {
	@FindBy(xpath = "//*[@id='title']")
	public WebElement titleTextbox;

	@FindBy(xpath = "//*[@id='title']/following::div[1]")
	public WebElement titleErrorMessage;

	@FindBy(xpath = "//*[@id='status']")
	public WebElement statusDropdown;

	@FindBy(xpath = "//body[@id='tinymce']")
	public WebElement serviceContentEditor;

	@FindBy(xpath = "//*[@id='overview_text']")
	public WebElement homePageContentTextbox;

	@FindBy(xpath = "//*[@id='description']/following-sibling::div[2]")
	public WebElement serviceContentErrorMessage;

	@FindBy(xpath = "//*[@id='overview_text']/following::div[1]")
	public WebElement homePageContentErrorMessage;

	@FindBy(xpath = "//input[@type='file']")
	public WebElement imageUploader;

	@FindBy(xpath = "//div[@id='error_image']")
	public WebElement imageUploaderErrorMessage;

	@FindBy(xpath = "//*[@id='alt_image_text']")
	public WebElement imageAltTextbox;

	@FindBy(xpath = "//*[@id='alt_image_text']/following::div[1]")
	public WebElement imageAltErrorMessage;

	@FindBy(xpath = "//*[@id='DataTables_Table_0']/tbody/tr[1]/td[8]/a")
	public WebElement serviceEditButton;

	@FindBy(xpath = "//*[@id='is_featured']")
	public WebElement featuredCheckbox;

}
