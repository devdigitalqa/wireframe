package com.pages.adminpages.services;

import org.apache.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import com.basicactions.DropDownHelper;
import com.basicactions.LogHelper;
import com.utilities.CommonFunc;
import com.utilities.CommonXpath;
import com.utilities.FilesPaths;

public class Services extends ServicesXpath {
	WebDriver driver;
	DropDownHelper dropDownHelper;
	CommonFunc commonFunc;
	CommonXpath commonXpath;
	private Logger log = LogHelper.getLogger(Services.class);
	boolean verifyDetails = false;

	public Services(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		dropDownHelper = new DropDownHelper(driver);
		commonFunc = new CommonFunc(driver);
		commonXpath = new CommonXpath(driver);
	}

	public void enterTitle(String titleText) {
		log.info("********************Enter the Title********************");
		titleTextbox.clear();
		titleTextbox.sendKeys(titleText);
	}

	public void enterStatus(String statusVal) {
		log.info("********************Enter the Status********************");
		dropDownHelper.selectByVaule(statusDropdown, statusVal);
	}

	public void clickonEditbutton() throws InterruptedException {
		log.info("********************Click on Edit button********************");
		Thread.sleep(3000);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", serviceEditButton);
	}

	public void enterHomePageContent(String homePageContent) throws InterruptedException {
		log.info("********************Enter the Home Page Content********************");
		homePageContentTextbox.clear();
		homePageContentTextbox.sendKeys(homePageContent);

		Thread.sleep(2000);
	}

	public void enterServiceContent(String serviceContent) throws InterruptedException {
		log.info("********************Enter the Service Content********************");
		driver.switchTo().frame("description_ifr");
		serviceContentEditor.sendKeys(serviceContent);
		driver.switchTo().defaultContent();
		Thread.sleep(2000);
	}

	public void enterImage(String image) throws InterruptedException {
		log.info("********************Enter the Image********************");
		imageUploader.sendKeys(FilesPaths.EXTRA_FILES_FOLDER + image);
		commonFunc.checkElementAvailableWithAttributeCompare(commonXpath.elementList, commonXpath.element, "style",
				"display: none;");
		Thread.sleep(3000);
	}

	public void enterImageAlt(String imageAlt) throws InterruptedException {
		log.info("********************Enter the Image Alt********************");
		imageAltTextbox.sendKeys(imageAlt);
		Thread.sleep(2000);

	}

	public void selectFeaturedCheckbox(String featuredVal) throws InterruptedException {
		log.info("********************Select Featured Checkbox********************");
		if (featuredVal.equals("1")) {
			featuredCheckbox.click();
		}
		Thread.sleep(2000);
	}

	public void validations() throws InterruptedException {
		String requiredValidation = "Required Validation";
		commonFunc.validationField(requiredValidation, "textbox", titleTextbox, titleErrorMessage,
				"The title field is required.");
		commonFunc.validationField(requiredValidation, "imageUpload", imageUploader, imageUploaderErrorMessage,
				"The image field is required.");
		commonFunc.validationField(requiredValidation, "textbox", imageAltTextbox, imageAltErrorMessage,
				"The alt image text field is required.");
		commonFunc.validationField(requiredValidation, "editor", serviceContentEditor, serviceContentErrorMessage,
				"The service content field is required.");
		commonFunc.validationField(requiredValidation, "textbox", homePageContentTextbox, homePageContentErrorMessage,
				"The home page content field is required.");
	}
}
