package com.pages.adminpages.blogs;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class BlogsXpath {
	@FindBy(xpath = "//*[@id='title']")
	public WebElement titleTextbox;

	@FindBy(xpath = "//*[@id='title']/following::div[1]")
	public WebElement titleErrorMessage;

	@FindBy(xpath = "//*[@id='parent_category']")
	public WebElement blogCategory;

	@FindBy(id = "author_first_name")
	public WebElement authorFirstName;

	@FindBy(id = "author_last_name")
	public WebElement authorLastName;

	@FindBy(xpath = "//*[@id='publish_date']")
	public WebElement publishDate;

	@FindBy(xpath = "//*[@id='publish_date']/following::div[1]")
	public WebElement publishDateErrorMessage;

	@FindBy(xpath = "//*[@id='DataTables_Table_0']/tbody/tr[1]/td[9]/a")
	public WebElement blogEditButton;

	@FindBy(xpath = "//*[@id='navbarDropdown' and @title='Blogs']//parent::li[1]/div[1]/a[2]")
	public WebElement blogSettingMenu;

	@FindBy(xpath = "//*[@id='btnsave']")
	public WebElement settingSave;
	
	@FindBy(xpath = "/html/body/nav/ul/li[4]/div/a[2]")
	public WebElement blogCategories;

	@FindBy(xpath = "//body[@id='tinymce']")
	public WebElement descriptionEditor;

	@FindBy(xpath = "//*[@id='frmaddedit']/div[2]/div/div[9]/div[2]")
	public WebElement descriptionEditorErrorMessage;
	
	@FindBy(xpath = "//input[@type='file']")
	public WebElement imageUpload;

	@FindBy(xpath = "//div[@id='error_main_image']")
	public WebElement imageUploadErrorMessage;

	@FindBy(xpath = "//*[@id='frmaddedit']/div[2]/div/div[7]/div[2]/div[3]/table/tbody/tr[3]/td[2]")
	public WebElement blogselectdate;

	@FindBy(xpath = "//*[@id='frmaddedit']/div[2]/div/div[7]/div[2]/div[2]/table/tbody/tr/td/fieldset[2]/span[5]")
	public WebElement selectTime;

	@FindBy(xpath = "//*[@id='comment_moderation']/option[1]")
	public WebElement commentModeration;

	@FindBy(xpath = "//*[@id='status']")
	public WebElement status;

	@FindBy(xpath = "//*[@id='field_blog_per_page_display']")
	public WebElement fieldBlog;

	@FindBy(xpath = "//*[@id='frmaddedit']/div[2]/div/div[7]/div[2]/div[1]/table/tbody/tr/td/fieldset/span[11]")
	public WebElement time;

	@FindBy(xpath = "//*[@id='main_image_alt']")
	public WebElement blogImageAlt;

	@FindBy(xpath = "//*[@id='meta_title']")
	public WebElement blogMetaTitle;
	
	@FindBy(xpath = "//*[@id='meta_title']/following::div[1]")
	public WebElement blogMetaTitleErrorMessage;
	
	@FindBy(xpath = "//*[@id='meta_desc']")
	public WebElement blogMetaDescription;

	@FindBy(xpath = "//*[@id='meta_desc']/following::div[1]")
	public WebElement blogMetaDescriptionErrorMessage;

	@FindBy(xpath = "//*[@id='main_image_alt']/following::div[1]")
	public WebElement blogImageAltErrorMessage;

	@FindBy(xpath = "//*[@id='DataTables_Table_0']/tbody/tr[1]/td[6]")
	public WebElement date;
	
	@FindBy(xpath = "//*[@name='field_blog_per_page_display']/following::div[1]")
	public WebElement blogDisplayFieldValidation;
	
	@FindBy(xpath = "/html/body/nav/ul/li[4]/div/a[3]")
	public WebElement BlogSettings;
	
	

}
