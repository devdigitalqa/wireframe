package com.pages.adminpages.blogs;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.base.Frame;
import com.basicactions.DropDownHelper;
import com.basicactions.ExcelHelper;
import com.basicactions.LogHelper;
import com.utilities.CommonFunc;
import com.utilities.CommonVariables;
import com.utilities.CommonXpath;
import com.utilities.FilesPaths;

public class Blogs extends BlogsXpath {
	WebDriver driver;
	DropDownHelper dropDownHelper;
	CommonFunc commonFunc;
	CommonVariables commonVariables;
	CommonXpath commonXpath;
	private Logger log = LogHelper.getLogger(Blogs.class);
	boolean verifyDetails = false;

	public Blogs(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		commonFunc = new CommonFunc(driver);
		commonXpath = new CommonXpath(driver);
		dropDownHelper = new DropDownHelper(driver);
	}

	public void enterTitle(String title) {
		log.info("********************Enter the Title********************");
		titleTextbox.clear();
		titleTextbox.sendKeys(title);
	}

	public void selectBlogCategory(String blogCategory) {
		log.info("********************Select the Blog Categories********************");
		this.blogCategory.sendKeys(blogCategory);
	}

	public void enterAuthorFirstName(String authorfirstName) {
		log.info("********************Enter the Author FirstName********************");
		authorFirstName.clear();
		authorFirstName.sendKeys(authorfirstName);
	}

	public void enterAuthorLastName(String authorlastName) {
		log.info("********************Enter the Author LastName********************");
		authorLastName.clear();
		authorLastName.sendKeys(authorlastName);
	}

	public void enterFieldblog(String fieldblog) {
		log.info("********************Enter the Field Blog********************");
		fieldBlog.clear();
		fieldBlog.sendKeys(fieldblog);
	}

	public void clearBlogDisplayField() {
		log.info("********************Clear the Field Blog********************");
		fieldBlog.clear();
	}

	public void selectCommentmoderation(String commentmoderation) {
		log.info("********************Select Comment Moderation********************");
		dropDownHelper.selectByVaule(commentModeration, commentmoderation);
	}

	public void selectStatus(String status) {
		log.info("********************Enter the Status********************");
		dropDownHelper.selectByVaule(this.status, status);
	}

	public void enterPublishDate() {
		log.info("********************Enter the Publish Date********************");
		publishDate.click();
		blogselectdate.click();
		selectTime.click();
		time.click();
	}

	public String getPublishdate() {
		log.info("********************Get Publish Date********************");
		return date.getText().trim();
	}

	public void clickonEditbutton() {
		log.info("********************Click on Edit button********************");
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", blogEditButton);
	}

	public void clickonSettingmenu() {
		log.info("********************Click on Setting Menu********************");
		blogSettingMenu.click();
	}

	public void clickonSettingsave() {
		log.info("********************Click on Setting Save Button********************");
		settingSave.click();
	}

	public void checkValidationBlogDisplayField() {
		clearBlogDisplayField();
		clickonSettingsave();

		assertEquals(blogDisplayFieldValidation.getText().trim(), "The Default number of blogs to display field is required.");
	}

	public void clickonBlogCategoriesmenu() {
		log.info("********************Click on Setting Menu********************");
		blogCategories.click();
	}

	public void enterDescription(String description) {
		log.info("********************Enter the Description********************");
		driver.switchTo().frame("description_ifr");
		descriptionEditor.sendKeys(description);
		driver.switchTo().defaultContent();
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView();", driver.findElement(By.xpath("//*[@id='meta_title']")));
	}

	public void selectImage(String image) {
		log.info("********************Select the Image********************");
		imageUpload.sendKeys(FilesPaths.EXTRA_FILES_FOLDER + image);
		commonFunc.checkElementAvailableWithAttributeCompare(commonXpath.elementList, commonXpath.element, "style",
				"display: none;");
	}

	public void enterImageAlt(String imageAlt) {
		log.info("********************Enter the Image Alt********************");
		blogImageAlt.sendKeys(imageAlt);
	}

	public void enterMetaTitle(String metaTitle) {
		log.info("********************Enter the Meta Title********************");
		blogMetaTitle.sendKeys(metaTitle);
	}

	public void enterMetaDescription(String metaDescription) {
		log.info("********************Enter the Meta Description********************");
		blogMetaDescription.sendKeys(metaDescription);
	}

	public void fillFormSubmitForm(String moduleName) throws InterruptedException {
		ExcelHelper.readDataFromXLS(FilesPaths.TEMPLATE9WIREFRAME, CommonVariables.BLOGS);
		int blogendvalue = ExcelHelper.getTotalRowsCount();
		for (int i = 15; i < blogendvalue; i++) {
			String title = ExcelHelper.getData(0, i);
			if (title.isEmpty()) {
				break;
			} else {
				enterTitle(title);
				String image = ExcelHelper.getData(1, i);
				selectImage(image);
				String imageAlt = ExcelHelper.getData(2, i);
				enterImageAlt(imageAlt);
				enterPublishDate();
				String description = ExcelHelper.getData(3, i);
				enterDescription(description);
				String metaTitle = ExcelHelper.getData(4, i);
				enterMetaTitle(metaTitle);
				String metaDescription = ExcelHelper.getData(5, i);
				enterMetaDescription(metaDescription);
				commonFunc.clickOnSave(moduleName);
				Thread.sleep(2000);
				commonFunc.clickOnAddNewButton(moduleName);
			}
		}
	}

	public void sortingFun() {
		List<WebElement> tdList = commonXpath.tableRowListATag;
		ArrayList<String> list = new ArrayList<>();
		for (int i = 0; i < tdList.size(); i++) {
			list.add(tdList.get(i).getText());
		}
		driver.findElement(By.xpath("//*[@id='DataTables_Table_0']/thead/tr/th[4]")).click();

		commonFunc.checkElementAvailableWithAttributeCompare(commonXpath.elementList, commonXpath.element,
				CommonVariables.STYLE, CommonVariables.DISPLAY_NONE);

		Collections.sort(list, String.CASE_INSENSITIVE_ORDER);

		driver.findElement(By.xpath("//*[@id='DataTables_Table_0']/thead/tr/th[4]")).click();

		commonFunc.checkElementAvailableWithAttributeCompare(commonXpath.elementList, commonXpath.element,
				CommonVariables.STYLE, CommonVariables.DISPLAY_NONE);

		tdList = commonXpath.tableRowListATag;
		ArrayList<String> list1 = new ArrayList<>();
		for (int i = 0; i < tdList.size(); i++) {
			list1.add(tdList.get(i).getText());
		}

		List<WebElement> tdList1 = driver.findElements(By.xpath("//*[@id='DataTables_Table_0']/tbody/tr/td[5]"));
		ArrayList<String> list2 = new ArrayList<>();
		for (int i = 0; i < tdList1.size(); i++) {
			list2.add(tdList1.get(i).getText());
		}
		driver.findElement(By.xpath("//*[@id='DataTables_Table_0']/thead/tr/th[5]")).click();

		commonFunc.checkElementAvailableWithAttributeCompare(commonXpath.elementList, commonXpath.element,
				CommonVariables.STYLE, CommonVariables.DISPLAY_NONE);

		Collections.sort(list, String.CASE_INSENSITIVE_ORDER);

		driver.findElement(By.xpath("//*[@id='DataTables_Table_0']/thead/tr/th[5]")).click();

		commonFunc.checkElementAvailableWithAttributeCompare(commonXpath.elementList, commonXpath.element,
				CommonVariables.STYLE, CommonVariables.DISPLAY_NONE);

		tdList = driver.findElements(By.xpath("//*[@id='DataTables_Table_0']/tbody/tr/td[5]"));
		ArrayList<String> list3 = new ArrayList<>();
		for (int i = 0; i < tdList.size(); i++) {
			list3.add(tdList.get(i).getText());
		}
	}

	public void verifyBlogTitle() throws InterruptedException {
		log.info("********************Enter the Title********************");

		List<WebElement> ele = driver
				.findElements(By.xpath("//div[contains(@class,'blog-listing')]/div/div/div[1]/div[1]/div"));
		int count = ele.size();
		int tmp = 1;
		while (tmp == 1) {
			for (int i = 1; i <= count; i++) {

				WebElement eleBlogName = driver
						.findElement(By.xpath("//div[contains(@class,'blog-listing')]/div/div/div[1]/div[1]/div[" + i
								+ "]/div[1]/a[1]/div[2]/div[1]"));

				String blogName = eleBlogName.getText().trim();

				log.info("blog name =" + blogName);
				log.info("search text = " + Frame.commonVariables.getTxtSearchCmnVar());

				if (blogName.contains(Frame.commonVariables.getTxtSearchCmnVar())) {
					eleBlogName.click();
					Thread.sleep(3000);
					tmp = 0;
					break;
				}
			}
		}

		WebElement eleBlogTitle = driver
				.findElement(By.xpath("//div[contains(@class,'blog-listing')]/div/div/div[1]/div[1]/div[1]/h2[1]"));

		Assert.assertEquals(eleBlogTitle.getText().trim(), Frame.commonVariables.getTxtSearchCmnVar());
	}

	public void verifyBlogAuthorName() {
		String adminAuthor = "Author | " + Frame.commonVariables.getAdminauthor();
		String adminAuthorCMS = driver
				.findElement(
						By.xpath("//div[contains(@class,'blog-listing')]/div/div/div[1]/div[1]/div[1]/ul[1]/li[1]"))
				.getText().trim();
		log.info("author xpath = " + adminAuthorCMS);
		log.info("admin value = " + adminAuthor);
		Assert.assertEquals(adminAuthorCMS.trim(), adminAuthor);
	}

	public void verifyShareOption() {
		// Share Links
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView();", commonXpath.blogsDetailPageSocialMediaSection);
		int size = commonXpath.blogsDetailPageSocialMediaSectionList.size();
		assertTrue(size > 0);

		size = commonXpath.blogsDetailPageSocialMediaLinks.size();
		assertTrue(size > 0);
	}

	public void verifyBackToBlogList() {
		// Back to Blog List button
		int size = commonXpath.blogsDetailPageBackToBlogListButtonList.size();
		assertTrue(size > 0);

		Assert.assertEquals(commonXpath.blogsDetailPageBackToBlogListButton.getAttribute("href"),
				Frame.commonVariables.getFrontURL() + "blogs");
	}

	public void validations() throws InterruptedException {
		String requiredValidation = "Required Validation";
		String textbox = "textbox";
		commonFunc.validationField(requiredValidation, textbox, titleTextbox, titleErrorMessage,
				"The title field is required.");
		commonFunc.validationField(requiredValidation, "imageUpload", imageUpload, imageUploadErrorMessage,
				"The main image field is required.");
		commonFunc.validationField(requiredValidation, textbox, blogImageAlt, blogImageAltErrorMessage,
				"The main image alt field is required.");
		commonFunc.validationField(requiredValidation, "datepicker", publishDate, publishDateErrorMessage,
				"The publish date field is required.");
		commonFunc.validationField(requiredValidation, "editor", descriptionEditor, descriptionEditorErrorMessage,
				"The description field is required.");
		commonFunc.validationField(requiredValidation, textbox, blogMetaTitle, blogMetaTitleErrorMessage,
				"The meta title field is required.");
		commonFunc.validationField(requiredValidation, textbox, blogMetaDescription, blogMetaDescriptionErrorMessage,
				"The meta description field is required.");
	}
}