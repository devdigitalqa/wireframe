package com.pages.adminpages.homepagesliders;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;

import com.basicactions.DropDownHelper;
import com.basicactions.LogHelper;
import com.utilities.CommonFunc;
import com.utilities.CommonVariables;
import com.utilities.CommonXpath;

public class HomePageSliders extends HomePageSlidersXpath {
	WebDriver driver;
	DropDownHelper dropDownHelper;
	CommonFunc commonFunc;
	CommonVariables commonVariables;
	CommonXpath commonXpath;
	private Logger log = LogHelper.getLogger(HomePageSliders.class);
	boolean verifyDetails = false;

	public HomePageSliders(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		commonFunc = new CommonFunc(driver);
		commonXpath = new CommonXpath(driver);
		dropDownHelper = new DropDownHelper(driver);
	}

	public void enterTitle(String title) {
		log.info("********************Enter the Title********************");
		titleTextbox.clear();
		titleTextbox.sendKeys(title);
	}

	public void enterOverlayText(String overlaytxt) {
		log.info("********************Enter the overlay text********************");
		sliderOverlayText.clear();
		sliderOverlayText.sendKeys(overlaytxt);
	}

	public void enterImageAlt(String imagealt) {
		log.info("********************Enter the image alt text********************");
		sliderImageAlt.clear();
		sliderImageAlt.sendKeys(imagealt);
	}

	public void selectDisplaySlider(String displaySlider) {
		log.info("********************Enter the Status********************");
		dropDownHelper.selectByVaule(displaySliderDropdown, displaySlider);
	}

	public void clickonEditbutton() throws InterruptedException {
		log.info("********************Click on Edit button********************");

		List<WebElement> img = driver.findElements(By.xpath("//*[@id='main']/div[3]//form/div/div[2]/div/div/div"));
		int sliderCount = img.size();

		Actions actions = new Actions(driver);
		WebElement menuOption = driver
				.findElement(By.xpath("//*[@id='main']/div[3]//form/div/div[2]/div[" + sliderCount + "]/div/div"));
		actions.moveToElement(menuOption).perform();
		Thread.sleep(1000);

		WebElement editButton = driver.findElement(
				By.xpath("//*[@id='sortable']//div[" + sliderCount + "]/div[1]/div[1]/div[1]/div[1]/a[@title='Edit']"));

		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", editButton);

	}
}
