package com.pages.adminpages.homepagesliders;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePageSlidersXpath {
	@FindBy(id = "title")
	public WebElement titleTextbox;

	@FindBy(xpath = "//*[@id='display_order_62']/div/div/div[1]/div/a[2]")
	public WebElement sliderHover;

	@FindBy(xpath = "//*[@id='overlay_text']")
	public WebElement sliderOverlayText;

	@FindBy(xpath = "//*[@id='alt_image_text']")
	public WebElement sliderImageAlt;
	
	@FindBy(xpath = "//*[@id='field_slider_title_display']")
	public WebElement displaySliderDropdown;

}
