package com.pages.adminpages.partners;

import static org.testng.Assert.assertTrue;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;

import com.basicactions.DropDownHelper;
import com.basicactions.LogHelper;
import com.utilities.CommonFunc;
import com.utilities.CommonVariables;
import com.utilities.CommonXpath;
import com.utilities.FilesPaths;

public class Partners extends PartnersXpath {
	WebDriver driver;
	DropDownHelper dropDownHelper;
	CommonFunc commonFunc;
	CommonVariables commonVariables;
	CommonXpath commonXpath;
	private Logger log = LogHelper.getLogger(Partners.class);
	boolean verifyDetails = false;
	String scrollIntoView = "arguments[0].scrollIntoView();";

	public Partners(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		commonFunc = new CommonFunc(driver);
		commonXpath = new CommonXpath(driver);
		dropDownHelper = new DropDownHelper(driver);
	}

	public void clickonEditbutton() throws InterruptedException {
		log.info("********************Click on Edit button********************");
		List<WebElement> img = driver.findElements(By.xpath("//*[@id='main']/div[3]//form/div/div[2]/div/div/div"));
		int partnerCount = img.size();

		Actions actions = new Actions(driver);
		WebElement menuOption = driver
				.findElement(By.xpath("//*[@id='main']/div[3]//form/div/div[2]/div[" + partnerCount + "]/div/div"));
		actions.moveToElement(menuOption).perform();
		Thread.sleep(1000);

		WebElement editButton = driver.findElement(By
				.xpath("//*[@id='sortable']//div[" + partnerCount + "]/div[1]/div[1]/div[1]/div[1]/a[@title='Edit']"));

		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", editButton);
	}

	public void selectImage(String image) {
		log.info("********************Select the Image********************");
		imageFileUpload.sendKeys(FilesPaths.EXTRA_FILES_FOLDER + image);
		commonFunc.checkElementAvailableWithAttributeCompare(commonXpath.elementList, commonXpath.element,
				CommonVariables.STYLE, CommonVariables.DISPLAY_NONE);
	}

	public void enterImageAlt(String imageAlt) {
		log.info("********************Enter the Image Alt********************");
		imageAltTextBox.clear();
		imageAltTextBox.sendKeys(imageAlt);
	}

	public void deletePartner() throws InterruptedException {
		List<WebElement> img = driver.findElements(By.xpath("//*[@id='main']/div[3]//form/div/div[2]/div/div/div"));
		int sliderCount = img.size();

		WebElement menuOption = driver
				.findElement(By.xpath("//*[@id='main']/div[3]//form/div/div[2]/div[" + sliderCount + "]/div/div"));

		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript(scrollIntoView, menuOption);

		Actions actions = new Actions(driver);
		actions.moveToElement(menuOption).perform();
		Thread.sleep(1000);

		WebElement deleteButton = driver.findElement(By
				.xpath("//*[@id='sortable']//div[" + sliderCount + "]/div[1]/div[1]/div[1]/div[1]/a[@title='Delete']"));
		deleteButton.click();
		Thread.sleep(2000);
		partnerSubmitDeleteButton.click();
		commonFunc.checkElementAvailableWithAttributeCompare(commonXpath.elementList, commonXpath.element,
				CommonVariables.STYLE, CommonVariables.DISPLAY_NONE);
	}

	public void activePartner(String msg) throws InterruptedException {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript(scrollIntoView, partnerActiveButton);
		Thread.sleep(1000);
		partnerActiveButton.click();
		partnerSubmitButton.click();
		Thread.sleep(2000);
		assertTrue(messageText.getText().contains(msg));
	}

	public void inactivePartner(String msg) throws InterruptedException {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript(scrollIntoView, partnerInactiveButton);
		Thread.sleep(1000);
		partnerInactiveButton.click();
		partnerSubmitButton.click();
		Thread.sleep(2000);
		log.info(messageText.getText());
		log.info(msg);
		assertTrue(messageText.getText().contains(msg));
	}

	public void selectPartner() throws InterruptedException {
		int countPartnerImage = partnersImagesList.size();
		WebElement selectCheckBox = driver.findElement(By.xpath(
				"//*[@id='sortable']/div[" + countPartnerImage + "]/div[1]/div[1]/div[2]/ul[1]/li[1]/div[1]/label[1]"));

		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript(scrollIntoView, selectCheckBox);

		selectCheckBox.click();
		Thread.sleep(2000);
	}
}
