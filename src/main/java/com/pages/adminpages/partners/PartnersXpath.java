package com.pages.adminpages.partners;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class PartnersXpath {
	@FindBy(xpath = "//*[@id='addfiles']/input")
	public WebElement imageFileUpload;

	@FindBy(xpath = "//*[@id='alt_image_text']")
	public WebElement imageAltTextBox;

	@FindBy(xpath = "//*[@id='status']")
	public WebElement statusDropdown;

	@FindBy(xpath = "//*[@id='sortable']/div")
	public List<WebElement> partnersImagesList;

	@FindBy(xpath = "//*[@id='confirm-action-submit']")
	public WebElement partnerSubmitButton;
	
	@FindBy(xpath = "//*[@id='confirm-partner-delete']")
	public WebElement partnerSubmitDeleteButton;

	@FindBy(xpath = "//*[@id='action_btns']/div/a[1]")
	public WebElement partnerActiveButton;

	@FindBy(xpath = "//*[@id='action_btns']/div/a[2]")
	public WebElement partnerInactiveButton;
	
	@FindBy(xpath = "//*[@id='main']/div[1]/span")
	public WebElement messageText;

}
