package com.pages.adminpages.contactus;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ContactUSXpath {

	// Contact Settings
	@FindBy(xpath = "//*[@id='contact_title']")
	public WebElement contactTitle;

	@FindBy(xpath = "//*[@id='contact_title']")
	public WebElement contactSettingTitle;

	@FindBy(xpath = "//*[@id='btnsave']")
	public WebElement contactSave;

	@FindBy(xpath = "//*[@id='contact_thank_you_message']")
	public WebElement contactMessage;

	@FindBy(xpath = "//*[@id='contact_email']")
	public WebElement contactEmail;

	@FindBy(xpath = "/html/body/nav/ul/li[11]/div/a[2]")
	public WebElement contactSettingMenu;
	
	@FindBy(xpath = "//*[@id=user_contact_acknowledge']")
	public WebElement contactsendAutoReply;

	@FindBy(xpath = "//*[@id='contact_subject']")
	public WebElement contactemailsub;

	@FindBy(xpath = "//*[@id='email_body']")
	public WebElement contactemailbody;

}
