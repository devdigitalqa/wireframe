package com.pages.adminpages.contactus;

import static org.testng.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.basicactions.DropDownHelper;
import com.basicactions.LogHelper;
import com.utilities.CommonFunc;
import com.utilities.CommonVariables;
import com.utilities.CommonXpath;

public class ContactUS extends ContactUSXpath {

	WebDriver driver;
	DropDownHelper dropDownHelper;
	CommonFunc commonFunc;
	CommonVariables commonVariables;
	CommonXpath commonXpath;
	String valueString = "value";

	private Logger log = LogHelper.getLogger(ContactUS.class);
	boolean verifyDetails = false;

	public ContactUS(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		commonFunc = new CommonFunc(driver);
		commonXpath = new CommonXpath(driver);

		dropDownHelper = new DropDownHelper(driver);
	}

	public String getContacttitle() {
		return contactTitle.getAttribute(valueString);
	}

	public String getthankyoumessge() {
		return contactMessage.getAttribute(valueString);
	}

	public String getcontactemail() {
		return contactEmail.getAttribute(valueString);
	}

	public void contactSettings(String contacttitle, String contactmessage, String contactemail,String sendautoReply,String emailsub,String emailbody)
			throws InterruptedException {
		contactSettingMenu.click();
		Thread.sleep(1000);
		entercontactTitle(contacttitle);
		entercontactmessage(contactmessage);
		entercontactemail(contactemail);
		entersendAutoReply(sendautoReply);
		enteremailsub(emailsub);
		enteremailbody(emailbody);

		clickonSavebutton();
		assertTrue(contacttitle.equals(getContacttitle()));
		assertTrue(contactmessage.equals(getthankyoumessge()));
		assertTrue(contactemail.equals(getcontactemail()));assertTrue(contacttitle.equals(getContacttitle()));
		assertTrue(sendautoReply.equals(getthankyoumessge()));
		assertTrue(emailsub.equals(getcontactemail()));
		
	}

	public void entercontactTitle(String contacttitletle) {
		log.info("********************Enter the contacttitletle ********************");
		contactSettingTitle.clear();
		contactSettingTitle.sendKeys(contacttitletle);
	}

	public void entercontactmessage(String contactmessage) {
		log.info("********************Enter the Meta Description********************");
		contactMessage.clear();
		contactMessage.sendKeys(contactmessage);

	}

	public void entercontactemail(String contactemail) {
		log.info("********************Enter the Meta Description********************");
		contactEmail.clear();
		contactEmail.sendKeys(contactemail);
	}
	public void entersendAutoReply(String sendautoReply) {
		log.info("********************Enter the Meta Description********************");
		contactsendAutoReply.clear();
		contactsendAutoReply.sendKeys(sendautoReply);
	}
	public void enteremailsub(String emailsub) {
		log.info("********************Enter the Meta Description********************");
		contactemailsub.clear();
		contactemailsub.sendKeys(emailsub);
	}
	public void enteremailbody(String emailbody) {
		log.info("********************Enter the Meta Description********************");
		contactemailbody.clear();
		contactemailbody.sendKeys(emailbody);
	}

	public void clickonSavebutton() {
		log.info("********************Click on Save button********************");
		contactSave.click();
	}

	public void sortingFun() {
		List<WebElement> tdList = driver.findElements(By.xpath("//*[@id='DataTables_Table_0']/tbody/tr/td[2]"));
		ArrayList<String> list = new ArrayList<>();
		for (int i = 0; i < tdList.size(); i++) {
			list.add(tdList.get(i).getText());
		}
		driver.findElement(By.xpath("//*[@id='DataTables_Table_0']/thead/tr/th[2]")).click();

		commonFunc.checkElementAvailableWithAttributeCompare(commonXpath.elementList, commonXpath.element,
				CommonVariables.STYLE, CommonVariables.DISPLAY_NONE);

		Collections.sort(list, String.CASE_INSENSITIVE_ORDER);

		driver.findElement(By.xpath("//*[@id='DataTables_Table_0']/thead/tr/th[2]")).click();

		commonFunc.checkElementAvailableWithAttributeCompare(commonXpath.elementList, commonXpath.element,
				CommonVariables.STYLE, CommonVariables.DISPLAY_NONE);

		tdList = driver.findElements(By.xpath("//*[@id='DataTables_Table_0']/tbody/tr/td[2]"));
		ArrayList<String> list1 = new ArrayList<>();
		for (int i = 0; i < tdList.size(); i++) {
			list1.add(tdList.get(i).getText());
		}

		List<WebElement> tdList2 = driver.findElements(By.xpath("//*[@id='DataTables_Table_0']/tbody/tr/td[3]"));
		ArrayList<String> list2 = new ArrayList<>();
		for (int i = 0; i < tdList2.size(); i++) {
			list2.add(tdList2.get(i).getText());
		}
		driver.findElement(By.xpath("//*[@id='DataTables_Table_0']/thead/tr/th[3]")).click();

		commonFunc.checkElementAvailableWithAttributeCompare(commonXpath.elementList, commonXpath.element,
				CommonVariables.STYLE, CommonVariables.DISPLAY_NONE);

		Collections.sort(list, String.CASE_INSENSITIVE_ORDER);

		driver.findElement(By.xpath("//*[@id='DataTables_Table_0']/thead/tr/th[3]")).click();

		commonFunc.checkElementAvailableWithAttributeCompare(commonXpath.elementList, commonXpath.element,
				CommonVariables.STYLE, CommonVariables.DISPLAY_NONE);

		tdList = driver.findElements(By.xpath("//*[@id='DataTables_Table_0']/tbody/tr/td[3]"));
		ArrayList<String> list3 = new ArrayList<>();
		for (int i = 0; i < tdList.size(); i++) {
			list3.add(tdList.get(i).getText());
		}
	}

}
