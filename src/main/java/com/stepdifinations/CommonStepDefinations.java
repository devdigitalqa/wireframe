package com.stepdifinations;

import org.json.simple.parser.ParseException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import com.base.Frame;
import com.base.TestBase;
import com.pages.adminpages.HomePage;
import com.pages.commonpages.LoginPage;
import com.utilities.CommonFunc;
import com.utilities.CommonXpath;

import io.cucumber.java.en.Given;

public class CommonStepDefinations {

	WebDriver driver;
	TestBase testBase;
	LoginPage loginPage;
	CommonFunc commonFunc;
	HomePage homePage;
	CommonXpath commonXpath;

	public CommonStepDefinations() {
		driver = TestBase.getDriver();
		PageFactory.initElements(driver, this);
		commonFunc = new CommonFunc(driver);
		commonXpath = new CommonXpath(driver);
	}

	@Given("Open Front site and Go to {string} Module")
	public void openFrontSite(String moduleName) throws ParseException {

		commonFunc.urlGet();
		driver.get(Frame.commonVariables.getFrontURL());
		commonFunc.clickFrontMenuOption(moduleName);
	}

	@Given("Login as Admin and Get Module Title")
	public void loginAsAdminGetModuleTitle() throws ParseException, InterruptedException {
		commonFunc.urlGet();
		driver.get(Frame.commonVariables.getAdminURL());
		commonFunc.credentialGet();
		loginPage = new LoginPage(driver);
		homePage = loginPage.login(Frame.commonVariables.loginEmail, Frame.commonVariables.loginPwd);

		driver.get("http://template9.devdigdev.com/admin/site-config/partner");
		Thread.sleep(1500);

		Frame.commonVariables
				.setPageContentTitle(driver.findElement(By.xpath("//*[@id='partner_title']")).getAttribute("value"));
	}

	@Given("Login as Admin and Go to {string} Module")
	public void userOnAdminDashboardPage(String moduleName) throws ParseException {
		if (moduleName.equals("Partners")) {
			moduleName = Frame.commonVariables.getPageContentTitle();
		}

		commonFunc.urlGet();
		driver.get(Frame.commonVariables.getAdminURL());
		commonFunc.credentialGet();
		loginPage = new LoginPage(driver);
		homePage = loginPage.login(Frame.commonVariables.loginEmail, Frame.commonVariables.loginPwd);

		commonFunc.verifythesheetname(moduleName);

		commonFunc.clickMenuOption(moduleName);
		commonFunc.checkElementAvailableWithAttributeCompare(commonXpath.elementList, commonXpath.element, "style",
				"display: none;");
	}
}