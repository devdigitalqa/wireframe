package com.stepdifinations;

import static org.testng.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.log4j.Logger;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.base.Frame;
import com.base.TestBase;
import com.basicactions.DropDownHelper;
import com.basicactions.ExcelHelper;
import com.basicactions.ExcelHelperPOI;
import com.basicactions.LogHelper;
import com.basicactions.WaitHelper;
import com.pages.adminpages.blogs.Blogs;
import com.pages.adminpages.categories.Categories;
import com.pages.adminpages.changepassword.Changepassword;
import com.pages.adminpages.contactus.ContactUS;
import com.pages.adminpages.galleries.Galleries;
import com.pages.adminpages.homepagesliders.HomePageSliders;
import com.pages.adminpages.iptracker.IPTracker;
import com.pages.adminpages.pages.Pages;
import com.pages.adminpages.partners.Partners;
import com.pages.adminpages.services.Services;
import com.pages.adminpages.settings.Settings;
import com.pages.adminpages.users.Users;
import com.pages.commonpages.LoginPage;
import com.utilities.CommonFunc;
import com.utilities.CommonVariables;
import com.utilities.CommonXpath;
import com.utilities.CurrentDateFormat;
import com.utilities.FilesPaths;
import com.utilities.ReadPropFile;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import jxl.write.WriteException;

public class StepDefination {

	WebDriver driver;

	TestBase testBase;
	ReadPropFile readPropFile;
	LoginPage loginPage;
	WaitHelper waitHelper;
	CommonFunc commonFunc;
	CommonXpath commonXpath;
	Categories categories;
	Services services;
	Users users;
	Partners partners;
	Blogs blogs;
	ContactUS contactus;
	IPTracker iptracker;
	Pages pages;
	Settings settings;
	Galleries galleries;
	Changepassword changepwd;
	HomePageSliders homepagesliders;
	DropDownHelper dropDownHelper;
	CommonWhenStepDefinations commonWhenStepDefinations;
	String scrollIntoView = "arguments[0].scrollIntoView();";
	private Logger log = LogHelper.getLogger(StepDefination.class);

	CurrentDateFormat dateTime = new CurrentDateFormat();

	public StepDefination() {
		driver = TestBase.getDriver();
		PageFactory.initElements(driver, this);
		waitHelper = new WaitHelper(driver);
		commonFunc = new CommonFunc(driver);
		commonXpath = new CommonXpath(driver);
		contactus = new ContactUS(driver);
		categories = new Categories(driver);
		users = new Users(driver);
		services = new Services(driver);
		iptracker = new IPTracker(driver);
		pages = new Pages(driver);
		blogs = new Blogs(driver);
		partners = new Partners(driver);
		galleries = new Galleries(driver);
		settings = new Settings(driver);
		homepagesliders = new HomePageSliders(driver);
		dropDownHelper = new DropDownHelper(driver);
		commonWhenStepDefinations = new CommonWhenStepDefinations();
	}

	@When("Verify table column in each grid {string} page")
	public void verifyTableColumnInEachGridUsersPage(String moduleName) {
		ExcelHelper.readDataFromXLS(FilesPaths.TEMPLATE9WIREFRAME, CommonVariables.TABLE_COLUMNS);

		if (moduleName.equals(ExcelHelper.getData(0, 0))) {
			commonFunc.verifytablegridData(ExcelHelper.getData(1, 0), moduleName);
		} else if (moduleName.equals(ExcelHelper.getData(0, 1))) {
			commonFunc.verifytablegridData(ExcelHelper.getData(1, 1), moduleName);
		} else if (moduleName.equals(ExcelHelper.getData(0, 2))) {
			commonFunc.verifytablegridData(ExcelHelper.getData(1, 2), moduleName);
		} else if (moduleName.equals(ExcelHelper.getData(0, 3))) {
			commonFunc.verifytablegridData(ExcelHelper.getData(1, 3), moduleName);
		} else if (moduleName.equals(ExcelHelper.getData(0, 4))) {
			commonFunc.verifytablegridData(ExcelHelper.getData(1, 4), moduleName);
		} else if (moduleName.equals(ExcelHelper.getData(0, 5))) {
			commonFunc.verifytablegridData(ExcelHelper.getData(1, 5), moduleName);
		}
	}

	@And("Verify admintest data for contactUs setting")
	public void verifyAdminTestDataForContactUsSetting() throws InterruptedException {
		ExcelHelper.readDataFromXLS(FilesPaths.TEMPLATE9WIREFRAME, CommonVariables.CONTACTUS);

		String contactUsTitle = ExcelHelper.getData(1, 0);
		Frame.commonVariables.setContactUsTitleCMS(contactUsTitle);
		String thankYouMessage = ExcelHelper.getData(1, 1);
		String contactEmail = ExcelHelper.getData(1, 2);
		String sendAutoReply = ExcelHelper.getData(1, 3);
		String emailsub = ExcelHelper.getData(1, 4);
		String emailbody = ExcelHelper.getData(1, 5);

		contactus.contactSettings(contactUsTitle, thankYouMessage, contactEmail, sendAutoReply, emailsub, emailbody);
	}

	public String getStepsName(String step, List<String> arr) {
		String[] words = step.split(" ");
		String word = "{string}";

		StringBuilder bld = new StringBuilder();
		int count = 0;
		for (int i = 0; i < words.length; i++) {
			// if match found increase count
			if (word.equals(words[i])) {
				words[i] = arr.get(count);
				count++;
			}
			bld.append(words[i]);
		}
		return bld.toString().trim();
	}

	@And("Click on {string} button in {string}")
	public void clickOnAddButtonInUsersGrid(String buttonName, String moduleName) throws InterruptedException {
		if (moduleName.equals("Partners")) {
			moduleName = Frame.commonVariables.getPageContentTitle();
		}
		commonFunc.checkElementAvailableWithAttributeCompare(commonXpath.elementList, commonXpath.element,
				CommonVariables.STYLE, CommonVariables.DISPLAY_NONE);
		switch (buttonName) {
		case "Add":
			Frame.commonVariables.setDeleteRecord(false);
			commonFunc.clickOnAddNewButton(moduleName);
			break;
		case "Save":
			commonFunc.clickOnSave(moduleName);
			break;
		case "Edit":
			commonFunc.clickOnEditButton(moduleName);
			break;
		case "Upload":
			commonFunc.clickOnUploadButton();
			break;
		case "Delete":
			commonFunc.clickOnselectcheckbox();
			commonFunc.clickOnDeleteButton();
			Thread.sleep(2500);
			commonFunc.clickOnconfirmyesbutton();
			Frame.commonVariables.setDeleteRecord(true);
			break;
		case "Save and Continue":
			commonFunc.clickOnsavecontinuebutton();
			Frame.commonVariables.setSaveandcontinue(true);
			break;
		default:
			log.error(buttonName + " is not defined in " + moduleName);
			break;
		}
	}

	@Then("{string} {string} page gets open")
	public void usersAddPageGetsOpen(String moduleName, String formName) {
		if (moduleName.contains("Partners")) {
			moduleName = Frame.commonVariables.getPageContentTitle();
		}
		commonFunc.checkElementAvailableWithAttributeCompare(commonXpath.elementList, commonXpath.element,
				CommonVariables.STYLE, CommonVariables.DISPLAY_NONE);

		String page = formName + " " + moduleName;
		String settingpage = moduleName + " " + formName;

		if (formName.equals("Settings")) {
			switch (moduleName) {
			case CommonVariables.BLOGS:
				assertTrue(commonXpath.blogSettingTitle.getText().equalsIgnoreCase(settingpage));
				break;

			case CommonVariables.CONTACT_SETTINGS:
				assertTrue(commonXpath.contactSettingTitle.getText().equalsIgnoreCase(settingpage));
				break;

			case CommonVariables.HOME_PAGE_SLIDERS:
				assertTrue(commonXpath.contactSettingTitle.getText().equalsIgnoreCase(settingpage));
				break;

			default:
				assertTrue(false);
				break;
			}
		} else if (commonXpath.addTitle.getText().equalsIgnoreCase(page)) {
			log.info("Verify the title:" + commonXpath.addTitle.getText());
		} else {
			assertTrue(false);
		}
	}

	@When("I enter all mandatory fields for {string} User")
	public void enterAllMandatoryFieldsForAddUser(String formName) {
		ExcelHelper.readDataFromXLS(FilesPaths.TEMPLATE9WIREFRAME, CommonVariables.USERS);
		switch (formName) {
		case "add":
			String firstname = ExcelHelper.getData(1, 0);
			String lastname = ExcelHelper.getData(1, 1);
			String email = ExcelHelper.getData(1, 2);

			Frame.commonVariables.setEmail(email);

			Frame.commonVariables.setTxtSearchCmnVar(firstname + " " + lastname);

			users.enterUserFirstName(firstname);
			users.enterUserLastName(lastname);
			users.enterUserEmail(email);
			break;

		case "edit":
			String updatefirstname = ExcelHelper.getData(1, 3);
			String updatelastname = ExcelHelper.getData(1, 4);

			Frame.commonVariables.setTxtSearchCmnVar(updatefirstname + " " + updatelastname);

			users.enterUserFirstName(updatefirstname);
			users.enterUserLastName(updatelastname);
			break;

		default:
			assertTrue(false);
			break;
		}
	}

	@When("I enter all mandatory fields for {string} Blog Category")
	public void enterAllMandatoryFieldsForAddCategory(String formName) throws InterruptedException {
		ExcelHelper.readDataFromXLS(FilesPaths.TEMPLATE9WIREFRAME, CommonVariables.CATEGORIES);
		switch (formName) {
		case "add":
			String title = ExcelHelper.getData(1, 0);
			String status = ExcelHelper.getData(1, 1);
			String imageAlt = ExcelHelper.getData(1, 2);
			String image = ExcelHelper.getData(1, 3);
			String description = ExcelHelper.getData(1, 4);
			String metaTitle = ExcelHelper.getData(1, 5);
			String metaDescription = ExcelHelper.getData(1, 6);

			Frame.commonVariables.setTxtSearchCmnVar(title);

			categories.enterTitle(title);
			categories.enterImage(image);
			categories.enterImageAlt(imageAlt);
			categories.enterStatus(status);
			categories.enterDescription(description);
			categories.enterMetaTitle(metaTitle);
			categories.enterMetaDescription(metaDescription);

			break;

		case "edit":
			String updateTitle = ExcelHelper.getData(1, 7);

			categories.enterTitle(updateTitle);
			Frame.commonVariables.setTxtSearchCmnVar(updateTitle);

			break;

		default:
			assertTrue(false);
			break;
		}
	}

	@When("I enter all mandatory fields for {string} Service")
	public void enterAllMandatoryFieldsForAddService(String formName) throws InterruptedException {
		ExcelHelper.readDataFromXLS(FilesPaths.TEMPLATE9WIREFRAME, CommonVariables.SERVICES);
		switch (formName) {
		case "add":
			String title = ExcelHelper.getData(1, 0);
			String status = ExcelHelper.getData(1, 1);

			String imageAlt = ExcelHelper.getData(1, 2);
			String image = ExcelHelper.getData(1, 3);
			String serviceContent = ExcelHelper.getData(1, 4);
			String featuredVal = ExcelHelper.getData(1, 5);

			Frame.commonVariables.setTxtSearchCmnVar(title);

			services.enterTitle(title);
			services.enterImage(image);
			services.enterImageAlt(imageAlt);
			services.enterStatus(status);
			services.enterHomePageContent("123");
			services.enterServiceContent(serviceContent);
			services.selectFeaturedCheckbox(featuredVal);
			break;

		case "edit":
			String updateTitle = ExcelHelper.getData(1, 6);

			categories.enterTitle(updateTitle);
			Frame.commonVariables.setTxtSearchCmnVar(updateTitle);

			break;

		default:
			assertTrue(false);
			break;
		}
	}

	@When("I enter all mandatory fields for {string} Partner")
	public void enterAllMandatoryFieldsForAddPartner(String formName) {
		ExcelHelper.readDataFromXLS(FilesPaths.TEMPLATE9WIREFRAME, CommonVariables.PARTNERS);
		switch (formName) {
		case "add":
			String image = ExcelHelper.getData(1, 1);
			partners.selectImage(image);
			break;

		case "edit":
			String imagealt = ExcelHelper.getData(1, 0);
			partners.enterImageAlt(imagealt);

			break;

		default:
			assertTrue(false);
			break;
		}
	}

	@Then("I should get {string} message on {string}")
	public void getMessageOnParticularPage(String successmessage, String moduleName) throws InterruptedException {
		Thread.sleep(1500);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript(scrollIntoView, commonXpath.successmsg);

		String actualMessage = commonXpath.successmsg.getText().trim();
		String expectedMessage = successmessage;

		if (moduleName.equals("Partners")) {
			moduleName = Frame.commonVariables.getPageContentTitle();
		}

		commonFunc.checkElementAvailableWithAttributeCompare(commonXpath.elementList, commonXpath.element,
				CommonVariables.STYLE, CommonVariables.DISPLAY_NONE);

		log.info("expected message = " + expectedMessage);
		log.info("actual message = " + actualMessage);

		if (moduleName.equals("Home Page Sliders")) {
			assertTrue(actualMessage.contains(expectedMessage));
		} else if (moduleName.equals("Galleries")) {
			if (actualMessage.contains(expectedMessage)) {
				log.info("Message :" + actualMessage);
			} else {
				log.error("Message is not match: ");
				assertTrue(false);
			}
		}

		log.info("Actual Message is = " + actualMessage);
		log.info("Expected Message is = " + expectedMessage);

		if (actualMessage.contains("account restored successfully")) {
			assert true;
		} else {
			assertTrue(actualMessage.contains(expectedMessage));
		}

		if (Frame.commonVariables.isSaveandcontinue()) {
			commonFunc.clickOnCancelbutton();
			Frame.commonVariables.setSaveandcontinue(false);
		}
	}

	@And("Verify details in {string}")
	public void verifyDetailsInGrid(String moduleName) {
		commonFunc.checkElementAvailableWithAttributeCompare(commonXpath.elementList, commonXpath.element,
				CommonVariables.STYLE, CommonVariables.DISPLAY_NONE);

		String searchText = "";
		String xpath = "";
		String commonXpathString = "//table[@id='DataTables_Table_0']/tbody/tr[1]/td[4]/a";

		if (moduleName.equals(CommonVariables.USERS)) {
			searchText = Frame.commonVariables.getTxtSearchCmnVar();
			xpath = "//table[@id='DataTables_Table_0']/tbody[1]/tr[1]/td[4]";
		} else if (moduleName.equals(CommonVariables.HOME_PAGE_SLIDERS)) {
			searchText = Frame.commonVariables.getTxtSearchCmnVar();
			xpath = "//*[@id='main']/div[3]/form/div/div[2]/div[1]/div/div/div[2]/ul/li[2]/div/a";
		} else if (moduleName.equals(CommonVariables.GALLERIES)) {
			searchText = Frame.commonVariables.getTxtSearchCmnVar();
			xpath = "//*[@id='main']/div[3]/form/div/div[2]/div[1]/div/div/div[2]/ul/li[2]/p";
		} else if (moduleName.equals(CommonVariables.BLOGS) || moduleName.equals(CommonVariables.BlogCategories)
				|| moduleName.equals(CommonVariables.SERVICES)) {
			searchText = Frame.commonVariables.getTxtSearchCmnVar();
			xpath = commonXpathString;
		} else if (moduleName.equals(CommonVariables.IP_TRACKER)) {
			String searchvar = driver.findElement(By.xpath("//*[@id='dropdownMenu2']/span/span[1]")).getText();
			String[] values = searchvar.split(":");
			searchText = values[1];
			xpath = "//*[@id='DataTables_Table_0']/tbody/tr[1]/td[2]";
			String loggedin = driver.findElement(By.xpath("//*[@id='DataTables_Table_0']/tbody/tr[1]/td[4]")).getText();
			String lastactivity = driver.findElement(By.xpath("//*[@id='DataTables_Table_0']/tbody/tr[1]/td[5]"))
					.getText();
			assertTrue(loggedin.equals(lastactivity));
		} else if (moduleName.equals(CommonVariables.PAGES)) {
			searchText = "";
			xpath = commonXpathString;
		} else {
			assertTrue(false);
		}

		if (Frame.commonVariables.isDeleteRecord()) {
			xpath = "//*[@id='frmlist']/table/tbody/tr/td";
			log.info("delete record " + driver.findElement(By.xpath(xpath)).getText());
		}

		commonFunc.searchRecord(searchText.trim(), searchText.trim(), xpath, moduleName, "");

	}

	@When("Verify details in {string} for Testing Data")
	public void verifyDetailsInModuleForTestingData(String moduleName) {
		commonFunc.checkElementAvailableWithAttributeCompare(commonXpath.elementList, commonXpath.element,
				CommonVariables.STYLE, CommonVariables.DISPLAY_NONE);

		String searchText = "";
		String xpath = "";
		String commonXpathString = "//table[@id='DataTables_Table_0']/tbody/tr[1]/td[4]/a";

		if (moduleName.equals(CommonVariables.USERS)) {
			ExcelHelper.readDataFromXLS(FilesPaths.TEMPLATE9WIREFRAME, CommonVariables.USERS);
			searchText = ExcelHelper.getData(1, 2);
			xpath = "//table[@id='DataTables_Table_0']/tbody[1]/tr[1]/td[4]";
		} else if (moduleName.equals(CommonVariables.BLOGS) || moduleName.equals(CommonVariables.CATEGORIES)
				|| moduleName.equals(CommonVariables.SERVICES)) {
			ExcelHelper.readDataFromXLS(FilesPaths.TEMPLATE9WIREFRAME, moduleName);
			searchText = ExcelHelper.getData(1, 0);
			xpath = commonXpathString;
		}

		commonFunc.searchRecord(searchText.trim(), searchText.trim(), xpath, moduleName, "Testing Data");

	}

	@Then("Delete Data {string}")
	public void deleteTestingData(String moduleName) throws InterruptedException {
		driver.findElement(By.xpath("//*[@id=\"selectAll\"]")).click();
		Thread.sleep(1500);
		commonFunc.clickOnDeleteButton();
		Thread.sleep(1500);
		commonFunc.clickOnconfirmyesbutton();
		commonFunc.checkElementAvailableWithAttributeCompare(commonXpath.elementList, commonXpath.element,
				CommonVariables.STYLE, CommonVariables.DISPLAY_NONE);
	}

	@And("{string} is {string}")
	public void moduleActiveInactive(String moduleName, String status) {
		commonFunc.checkElementAvailableWithAttributeCompare(commonXpath.elementList, commonXpath.element,
				CommonVariables.STYLE, CommonVariables.DISPLAY_NONE);
		if (commonXpath.statuscolumn.getAttribute("class").equals("sort active ")) {
			Frame.commonVariables.setInactive("false");
			if (status.equals("Inactive")) {
				assertTrue(false);
			}
		} else if (commonXpath.statuscolumn.getAttribute("class").equals("sort inactive ")) {
			Frame.commonVariables.setInactive("true");
			if (status.equals("Active")) {
				assertTrue(false);
			}
		} else {
			Frame.commonVariables.setInactive("");
			assertTrue(false);
		}
	}

	@Then("Make {string} {string} and verify {string}")
	public void makeActiveAndVerifyErrorMessage(String moduleName, String status, String message)
			throws InterruptedException {
		commonFunc.checkElementAvailableWithAttributeCompare(commonXpath.elementList, commonXpath.element,
				CommonVariables.STYLE, CommonVariables.DISPLAY_NONE);

		if (moduleName.equals(CommonVariables.USERS)) {
			String messageText = "The user account is not validated yet, user needs to validate his/her account.";
			commonXpath.statuscolumn.click();

			assertTrue(commonXpath.successmsg.getText().equals(messageText));

		} else if (moduleName.equals(CommonVariables.BLOGS)) {
			commonFunc.clickOnstatuscolumn("blog");
		} else if (moduleName.equals(CommonVariables.CATEGORIES)) {
			commonFunc.clickOnstatuscolumn("category");
		} else if (moduleName.equals(CommonVariables.PAGES)) {
			commonFunc.clickOnstatuscolumn("page");
		} else if (moduleName.equals(CommonVariables.GALLERIES)) {
			commonFunc.clickOnstatuscolumn("galleries");
		} else if (moduleName.equals(CommonVariables.SERVICES)) {
			commonFunc.clickOnstatuscolumn("service");
		}
	}

	@When("I enter all mandatory fields for {string} Blogs")
	public void enterAllMandatoryFieldsForAddBlogs(String formName) {
		commonFunc.checkElementAvailableWithAttributeCompare(commonXpath.elementList, commonXpath.element,
				CommonVariables.STYLE, CommonVariables.DISPLAY_NONE);
		ExcelHelper.readDataFromXLS(FilesPaths.TEMPLATE9WIREFRAME, CommonVariables.BLOGS);
		switch (formName) {
		case "add":
			String title = ExcelHelper.getData(1, 0);
			String blogCategory = ExcelHelper.getData(1, 1);
			String authorfirstName = ExcelHelper.getData(1, 2);
			String authorlastName = ExcelHelper.getData(1, 3);
			String image = ExcelHelper.getData(1, 4);
			String imageAlt = ExcelHelper.getData(1, 5);
			String status = ExcelHelper.getData(1, 7);
			String description = ExcelHelper.getData(1, 8);
			String metaTitle = ExcelHelper.getData(1, 9);
			String metaDescription = ExcelHelper.getData(1, 10);

			Frame.commonVariables.setTxtSearchCmnVar(title);
			Frame.commonVariables.setAdminauthor(authorfirstName + " " + authorlastName);

			blogs.enterTitle(title);
			if (!blogCategory.isEmpty()) {
				blogs.selectBlogCategory(blogCategory);
			}
			blogs.enterAuthorFirstName(authorfirstName);
			blogs.enterAuthorLastName(authorlastName);
			blogs.selectImage(image);
			blogs.enterImageAlt(imageAlt);
			blogs.enterPublishDate();
			blogs.selectStatus(status);
			blogs.enterDescription(description);
			blogs.enterMetaTitle(metaTitle);
			blogs.enterMetaDescription(metaDescription);

			break;

		case "edit":
			String updatetitle = ExcelHelper.getData(1, 11);

			blogs.enterTitle(updatetitle);
			Frame.commonVariables.setTxtSearchCmnVar(updatetitle);
			break;

		default:
			assertTrue(false);
			break;
		}
	}

	@When("Click on {string} Settings menu")
	public void clickOnSettingsMenu(String moduleName) throws InterruptedException {
		Thread.sleep(2000);
		if (moduleName.equals(CommonVariables.BLOGS)) {
			blogs.clickonSettingmenu();
		} else if (moduleName.equals(CommonVariables.CONTACTUS)) {
			commonXpath.contactSetting.click();
		} else if (moduleName.equals(CommonVariables.HOME_PAGE_SLIDERS)) {
			commonXpath.homeSettingMenu.click();
		} else {
			assertTrue(false);
		}
	}

	@When("Click on {string} left side menu")
	public void clickOnBlogCategoriesMenu(String moduleName) throws InterruptedException {
		Thread.sleep(2000);
		System.out.println("Blogs module");
		System.out.println("Module name" + moduleName);
		System.out.println("Commvar" + CommonVariables.BlogCategories);
		if (moduleName.equals(CommonVariables.BlogCategories)) {
			System.out.println("Blog categories click");
			blogs.clickonBlogCategoriesmenu();
		} else {
			assertTrue(false);
		}
	}

	@And("Get value of record per page on blog listing page")
	public void getValueOfRecordPerPageOnBlogListingPage() {
		Frame.commonVariables
				.setBlogRecordPage(driver.findElement(By.id("field_blog_per_page_display")).getAttribute("value"));
	}

	@When("I enter all mandatory fields for {string} Settings")
	public void enterAllMandatoryFieldsForBlogsSetting(String moduleName) throws InterruptedException {

		if (moduleName.equals(CommonVariables.BLOGS)) {

			blogs.checkValidationBlogDisplayField();

			ExcelHelper.readDataFromXLS(FilesPaths.TEMPLATE9WIREFRAME, CommonVariables.BLOGS);

			String fieldblog = ExcelHelper.getData(1, 12);

			blogs.enterFieldblog(fieldblog);
			Thread.sleep(2000);

		} else {
			assertTrue(false);
		}
	}

	@And("Click on {string} button in {string} Settings")
	public void clickOnSaveButtonInBlogsSettings(String buttonName, String moduleName) throws InterruptedException {
		Thread.sleep(2000);
		if (moduleName.equals(CommonVariables.BLOGS)) {
			blogs.clickonSettingsave();
		} else {
			assertTrue(false);
		}
	}

	@Then("I should get {string} message on {string} Settings")
	public void shouldGetSettingsHaveBeenSavedSuccessfullyMessageOnBlogsSettings(String successmessage,
			String moduleName) {
		assertTrue(commonXpath.successmsg.getText().equals(successmessage));
	}

	@When("I enter all mandatory fields for {string} Page")
	public void enterAllMandatoryFieldsForAddPage(String formName) throws InterruptedException {
		ExcelHelper.readDataFromXLS(FilesPaths.TEMPLATE9WIREFRAME, CommonVariables.PAGES);
		switch (formName) {
		case "add":
			String title = ExcelHelper.getData(1, 0);
			String status = ExcelHelper.getData(1, 1);
			String pageContent = ExcelHelper.getData(1, 2);
			String metaTitle = ExcelHelper.getData(1, 3);
			String metaDescription = ExcelHelper.getData(1, 4);

			Frame.commonVariables.setTxtSearchCmnVar(title);

			pages.enterTitle(title);
			pages.enterStatus(status);
			pages.enterPageContent(pageContent);
			pages.enterMetaTitle(metaTitle);
			pages.enterMetaDescription(metaDescription);

			break;

		case "edit":
			String updateTitle = ExcelHelper.getData(1, 6);

			pages.enterTitle(updateTitle);
			Frame.commonVariables.setTxtSearchCmnVar(updateTitle);

			break;

		default:
			assertTrue(false);
			break;
		}
	}

	@When("Data update and verify details for Admin {string} section")
	public void dataUpdateAndVerifyDetailsForAdminSettingsSection(String formName) throws InterruptedException {
		ExcelHelper.readDataFromXLS(FilesPaths.TEMPLATE9WIREFRAME, CommonVariables.SETTINGS);

		// Admin Settings
		String appName = ExcelHelper.getData(1, 1);
//		String noofRecordsperPage = ExcelHelper.getData(1, 2);
		String footerTitleforAdmin = ExcelHelper.getData(1, 3);

		settings.adminSettings(appName, footerTitleforAdmin);

		// Front End Settings
		String sitetitle = ExcelHelper.getData(3, 1);
		String tagline = ExcelHelper.getData(3, 2);
		String copyrightText = ExcelHelper.getData(3, 3);
		String metaDescription = ExcelHelper.getData(3, 4);

		settings.frontEndSettings(sitetitle, tagline, copyrightText, metaDescription);

		// Company Settings
		String companyName = ExcelHelper.getData(5, 1);
		String addressline1 = ExcelHelper.getData(5, 2);
		String addressline2 = ExcelHelper.getData(5, 3);
		String city = ExcelHelper.getData(5, 4);
		String state = ExcelHelper.getData(5, 5);
		String country = ExcelHelper.getData(5, 6);
		String zipcode = ExcelHelper.getData(5, 7);
		String phone = ExcelHelper.getData(5, 8);
		String email = ExcelHelper.getData(5, 9);

		settings.companySettings(companyName, addressline1, addressline2, city, state, country, zipcode);

		settings.companySettings(phone, email);

		// Email Settings
		String fromName = ExcelHelper.getData(7, 1);
		String fromEmail = ExcelHelper.getData(7, 2);
		String adminEmail = ExcelHelper.getData(7, 3);

		settings.emailSettings(fromName, fromEmail, adminEmail);

		// Password Settings

		String passwordStrength = ExcelHelper.getData(9, 1);
		String loginattempt = ExcelHelper.getData(9, 2);
		String minpwdlength = ExcelHelper.getData(9, 3);
		String userBlockTime = ExcelHelper.getData(9, 4);

		settings.passwordSettings(passwordStrength, loginattempt, minpwdlength, userBlockTime);

		// Social link Settings
		String facebook = ExcelHelper.getData(11, 1);
		String twitter = ExcelHelper.getData(11, 2);
		String linkedIn = ExcelHelper.getData(11, 3);
		settings.sociallinkSettings(facebook, twitter, linkedIn);

		// Google Captcha
		String googleRecaptchaSiteKey = ExcelHelper.getData(13, 1);
		String googleRecaptchaSecertKey = ExcelHelper.getData(13, 2);
		settings.googlecaptchaSettings(googleRecaptchaSiteKey, googleRecaptchaSecertKey);

		// SEO Settings
		String robotsMetaTag = ExcelHelper.getData(15, 1);
		String googleAnalyticsCode = ExcelHelper.getData(15, 2);
		settings.seoSettings(googleAnalyticsCode, robotsMetaTag);

	}

	@And("Verify validation message for {string}")
	public void verifyValidationMessageForUsers(String moduleName) throws InterruptedException {
		if (moduleName.equals(CommonVariables.USERS)) {
			users.validations();
		} else if (moduleName.equals(CommonVariables.PAGES)) {
			pages.validations();
		} else if (moduleName.equals(CommonVariables.SETTINGS)) {
			settings.validations();
		} else if (moduleName.equals(CommonVariables.CATEGORIES)) {
			categories.validations();
		} else if (moduleName.equals(CommonVariables.BLOGS)) {
			blogs.validations();
		} else if (moduleName.equals(CommonVariables.SERVICES)) {
			services.validations();
		}
	}

	@When("Change First Name and Last Name")
	public void changeFirstNameAndLastName() throws InterruptedException {
		ExcelHelper.readDataFromXLS(FilesPaths.TEMPLATE9WIREFRAME, CommonVariables.USERS);

		String changefirstname = ExcelHelper.getData(1, 5);
		String changelastName = ExcelHelper.getData(1, 6);

		users.enterchangeFirstName(changefirstname);
		users.enterchangeLastName(changelastName);

		Frame.commonVariables.setChangename(changefirstname + " " + changelastName);

		Frame.commonVariables
				.setTxtSearchCmnVar(driver.findElement(By.xpath("//*[@id='email']")).getAttribute("value"));

		commonXpath.settingSave.click();
	}

	@When("Verify in {string} Module")
	public void verifyInModule(String modulename) {

		commonFunc.clickMenuOption(modulename);

		commonFunc.searchRecord(Frame.commonVariables.getTxtSearchCmnVar(), Frame.commonVariables.getChangename(),
				commonXpath.tableRow.getText(), modulename, "");
	}

	@When("Set new password")
	public void setNewPassword() throws ConfigurationException, InterruptedException {
		readPropFile = new ReadPropFile();

		String password = readPropFile.readProp().getProperty("password");
		String newpassword = readPropFile.readProp().getProperty("newpassword");

		changepwd.entercurrentpwd(password);
		changepwd.enternewpwd(newpassword);
		changepwd.enterconfirmpwd(newpassword);

		commonXpath.settingSave.click();

		PropertiesConfiguration out = new PropertiesConfiguration(FilesPaths.CONFIG_PROPERTIES_FILE);
		out.setProperty("password", newpassword);
		out.save();
		Thread.sleep(5000);

	}

	@Then("Logged out the admin user")
	public void loggedOutTheAdminUser() throws InterruptedException {
		commonXpath.profileMenu.click();
		Thread.sleep(3000);
		commonXpath.logoutProfile.click();
	}

	@Then("Verify checkbox for login user {string} Module")
	public void verifyCheckboxForLoginUserStringModule(String modulename) {

		String username = driver.findElement(By.xpath("// *[@id='dropdownMenu2']/span/span[1]")).getText();
		String[] user = username.split(":");

		commonFunc.searchRecord(Frame.commonVariables.getEmail(), user[1], commonXpath.tableRow.getText(), modulename,
				"");

		List<WebElement> checkbox = driver.findElements(By.xpath("//input[@type='checkbox']"));
		int count = checkbox.size();

		if (count > 0) {
			assert true;
		}

	}

	@When("I enter all fields for {string} {string}")
	public void enterAllFieldsForModule(String sheetName, String moduleName) throws InterruptedException {
		switch (moduleName) {
		case CommonVariables.USERS:
			users.fillFormSubmitForm(moduleName);
			break;
		case CommonVariables.PAGES:
			pages.fillFormSubmitForm(moduleName);
			break;
		case CommonVariables.CATEGORIES:
			categories.fillFormSubmitForm(moduleName);
			break;
		case CommonVariables.BLOGS:
			blogs.fillFormSubmitForm(moduleName);
			break;
		default:
			break;
		}
	}

	@And("Select all record and Click on {string} button in {string}")
	public void selectAllRecordAndClickOnDeleteButton(String formName, String moduleName) throws InterruptedException {
		Thread.sleep(2000);
		commonXpath.selectAllCheckbox.click();
		Thread.sleep(2000);
		commonXpath.deletebutton.click();
		Thread.sleep(2000);
		commonXpath.confirmyesbutton.click();
		Thread.sleep(3000);
	}

	@And("value get in Settings Module")
	public void valueGetInSettingsModule() throws InterruptedException {
		Thread.sleep(2000);
		String setting = commonXpath.noOfRecordsPerPage.getAttribute("value");
		Frame.commonVariables.setNumRecord(setting);
	}

	@And("Verify Sorting record in {string}")
	public void verifySortingRecordInUsers(String moduleName) {

		switch (moduleName) {
		case CommonVariables.USERS:
			users.sortingFun();
			break;

		case CommonVariables.PAGES:
			pages.sortingFun();
			break;

		case CommonVariables.BLOGS:
			blogs.sortingFun();
			break;

		case CommonVariables.CATEGORIES:
			categories.sortingFun();
			break;

		case CommonVariables.CONTACTUS:
			contactus.sortingFun();
			break;

		case CommonVariables.IP_TRACKER:
			iptracker.sortingFun();
			break;

		default:
			break;
		}
	}

	@When("Verify data in Contact Us")
	public void verifyDataInContactUs() throws IOException, InterruptedException {
		File f = new File(FilesPaths.CONTACT_INQUIRIES_EXCEL);

		if (f.exists()) {
			try {
				java.nio.file.Files.delete(Paths.get(FilesPaths.CONTACT_INQUIRIES_EXCEL));
			} catch (IOException e) {
				log.error(e.toString());
			}
		}

		List<WebElement> tdList = driver.findElements(By.xpath("//*[@id='DataTables_Table_0']/tbody/tr/td[2]"));
		ArrayList<String> list = new ArrayList<>();
		for (int i = 0; i < tdList.size(); i++) {
			list.add(tdList.get(i).getText());
			Thread.sleep(1000);
		}
		Thread.sleep(2500);
		driver.findElement(By.xpath("//*[@id='DataTables_Table_0_wrapper']/div[1]/div[1]/div/button")).click();
		Thread.sleep(5000);

		ExcelHelperPOI.readExcel(FilesPaths.CONTACT_INQUIRIES_EXCEL, "Sheet1");

		int totalRow = ExcelHelperPOI.lastRowNum();

		int j = 0;
		for (int i = 2; i < totalRow; i++) {
			XSSFRow r = ExcelHelperPOI.getSheet().getRow(i);
			String ce = r.getCell(0).getStringCellValue();
			Assert.assertEquals(list.get(j), ce);
			j++;
		}
	}

	@And("Verify Pagination count in {string}")
	public void verifyPaginationCountInModule(String menuTitle) throws InterruptedException {
		driver.findElement(By.xpath("/html/body/nav/ul/li[1]/a")).click();

		commonFunc.clickMenuOption(menuTitle);

		commonFunc.checkElementAvailableWithAttributeCompare(commonXpath.elementList, commonXpath.element,
				CommonVariables.STYLE, CommonVariables.DISPLAY_NONE);
		commonFunc.verifypaginationcount();

		List<WebElement> pagec11 = driver.findElements(By.xpath("//*[@style='cursor:pointer']"));
		int countc1 = pagec11.size();
		for (int i = 1; i <= countc1; i++) {
			String paginationcount = driver
					.findElement(By.xpath("//*[@id='DataTables_Table_0_paginate']/ul/li[" + (i + 1) + "]/a")).getText();

			if (Integer.parseInt(paginationcount) > 1) {
				driver.findElement(By.xpath("//*[@id='DataTables_Table_0_paginate']/ul/li[" + (i + 1) + "]/a")).click();
				Thread.sleep(2000);
				commonFunc.verifypaginationcount();
			} else {
				assert true;
			}
		}
	}

	@And("Verify detail image for add and edit page")
	public void verifyDetailImageForAddAndEditPage() {
		String allowImg = ExcelHelper.getData(1, 5);
		String imgallowyes = "Yes";
		String imagallowno = "No";
		List<WebElement> imageTag = driver.findElements(By.xpath("//*[@id='temp_image']"));
		List<WebElement> imageAlt = driver.findElements(By.xpath("//*[@id='image_alt']"));
		if (imgallowyes.equalsIgnoreCase(allowImg)) {
			int imageTagsize = imageTag.size();
			int imageAltsize = imageAlt.size();
			if ((imageTagsize > 0) && (imageAltsize > 0)) {
				assert true;
			}
		} else if (imagallowno.equalsIgnoreCase(allowImg)) {
			int imageTagsize = imageTag.size();
			int imageAltsize = imageAlt.size();
			if ((imageTagsize <= 0) && (imageAltsize <= 0)) {
				assert true;
			} else {
				assertTrue(false);
			}
		}
	}

	@Then("Verify the front Contact Us")
	public void verifyTheFrontContactUs() {
		Assert.assertEquals(commonXpath.contactUsTitle.getText(), Frame.commonVariables.getContactUsTitleCMS());
	}

	@And("Verify record per page on blog listing page")
	public void verifyRecordPerPageOnBlogListingPage() {
		List<WebElement> bloglist = driver
				.findElements(By.xpath("/html/body/main/div/div/div[1]/div[1]/div/div/div[2]/h3/a"));
		String blogField = ExcelHelper.getData(1, 12);
		int bloglistCount = bloglist.size();
		assertTrue(blogField.equals(String.valueOf(bloglistCount)));
	}

	@When("I enter all mandatory fields for {string} Home Page Sliders")
	public void enterAllMandatoryFieldsForAddHomeslider(String formName) throws InterruptedException {
		ExcelHelper.readDataFromXLS(FilesPaths.TEMPLATE9WIREFRAME, CommonVariables.HOME_PAGE_SLIDERS);

		switch (formName) {
		case "add":
			String img = ExcelHelper.getData(1, 0);

			commonXpath.image.sendKeys(FilesPaths.EXTRA_FILES_FOLDER + img);
			commonFunc.checkElementAvailableWithAttributeCompare(commonXpath.elementList, commonXpath.element,
					CommonVariables.STYLE, CommonVariables.DISPLAY_NONE);
			commonXpath.uploadButton.click();
			Frame.commonVariables.setTxtSearchCmnVar("Slider");
			Thread.sleep(4000);

			break;

		case "edit":
			String title = ExcelHelper.getData(1, 1);
			String overlaytxt = ExcelHelper.getData(1, 2);
			String imagealt = ExcelHelper.getData(1, 3);

			homepagesliders.enterTitle(title);
			homepagesliders.enterOverlayText(overlaytxt);

			JavascriptExecutor js = (JavascriptExecutor) driver;

			WebElement element = driver.findElement(By.xpath("//*[@id='alt_image_text']"));

			js.executeScript(scrollIntoView, element);

			homepagesliders.enterImageAlt(imagealt);
			Frame.commonVariables.setTxtSearchCmnVar(title);

			break;

		default:
			assertTrue(false);
			break;
		}
	}

	@Then("Verify detail in {string}")
	public void verifyDetailInHomeSlider(String image) throws InterruptedException {
		Thread.sleep(2000);

		JavascriptExecutor js = (JavascriptExecutor) driver;

		WebElement element = driver.findElement(By.xpath("//*[@id='addimagebtn']"));

		js.executeScript(scrollIntoView, element);

		Thread.sleep(2000);
		ExcelHelper.readDataFromXLS(FilesPaths.TEMPLATE9WIREFRAME, CommonVariables.HOME_PAGE_SLIDERS);
		String text = ExcelHelper.getData(1, 1);
		String updatedTitle = ExcelHelper.getData(1, 2);

		Thread.sleep(2000);
		commonXpath.searchButton.click();
		commonXpath.sliderSearchTextBox.sendKeys(text);
		Thread.sleep(2000);
		commonXpath.sliderSearchButton.click();
		Thread.sleep(2000);

		List<WebElement> img = driver.findElements(By.xpath("//*[@id='main']/div[3]/form/div/div[2]/div/div/div"));
		int numofimg = img.size();

		for (int i = 1; i <= numofimg; i++) {

			if (numofimg > 0)

			{
				WebElement img1 = driver.findElement(By
						.xpath("//*[@id='main']/div[3]/form/div/div[2]/div[" + i + "]/div/div/div[2]/ul/li[2]/div/a"));
				img1.getText();
				if (img1.getText().equals(text)) {
					Thread.sleep(4000);
					Actions actions = new Actions(driver);
					WebElement menuOption = driver.findElement(
							By.xpath("//*[@id='main']/div[3]/form/div/div[2]/div[1]/div/div/div[2]/ul/li[2]/div/a"));
					actions.moveToElement(menuOption).perform();

				} else {
					assertTrue(img1.getText().equals(updatedTitle));
				}
			}
		}
	}

	@And("Click on Delete button in Home Page Sliders")
	public void clickOnDeleteButtonInHomePageSliders() throws InterruptedException {
		List<WebElement> img = driver.findElements(By.xpath("//*[@id='main']/div[3]//form/div/div[2]/div/div/div"));
		int sliderCount = img.size();

		WebElement menuOption = driver
				.findElement(By.xpath("//*[@id='main']/div[3]//form/div/div[2]/div[" + sliderCount + "]/div/div"));

		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript(scrollIntoView, menuOption);

		Actions actions = new Actions(driver);
		actions.moveToElement(menuOption).perform();
		Thread.sleep(1000);

		WebElement deleteButton = driver.findElement(By
				.xpath("//*[@id='sortable']//div[" + sliderCount + "]/div[1]/div[1]/div[1]/div[1]/a[@title='Delete']"));
		deleteButton.click();
		Thread.sleep(2000);
		commonXpath.sliderSubmit.click();
	}

	@And("Click on Delete button in Partner")
	public void clickOnDeleteButtonInParnter() throws InterruptedException {
		partners.deletePartner();
	}

	@And("Home page slider setting {string}")
	public void homeSliderSetting(String value) throws InterruptedException {
		homepagesliders.selectDisplaySlider(value);
		Thread.sleep(2000);
		commonXpath.displaySliderSave.click();

	}

	@When("I enter all mandatory fields for {string} Galleries")
	public void enterAllMandatoryFieldsForAddGalleries(String formName) throws InterruptedException {
		commonFunc.checkElementAvailableWithAttributeCompare(commonXpath.elementList, commonXpath.element,
				CommonVariables.STYLE, CommonVariables.DISPLAY_NONE);
		ExcelHelper.readDataFromXLS(FilesPaths.TEMPLATE9WIREFRAME, CommonVariables.GALLERIES);
		switch (formName) {
		case "add":
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript(scrollIntoView, driver.findElement(By.xpath("//input[@type='file']")));
			String image = ExcelHelper.getData(1, 1);
			galleries.selectImage(image);
			commonXpath.uploadAllButton.click();
			commonFunc.checkElementAvailableWithAttributeCompare(commonXpath.elementList, commonXpath.element,
					CommonVariables.STYLE, CommonVariables.DISPLAY_NONE);
			break;

		case "edit":
			String imageAlt = ExcelHelper.getData(1, 0);
			galleries.enterImageAlt(imageAlt);
			Frame.commonVariables.setTxtSearchCmnVar(imageAlt);
			break;

		default:
			assertTrue(false);
			break;
		}

	}

	@And("Click on Delete button in Galleries")
	public void clickOnDeleteButtonInGalleries() throws InterruptedException {
		List<WebElement> img = driver.findElements(By.xpath("//*[@id='main']/div[3]//form/div/div[2]/div/div/div"));
		int sliderCount = img.size();

		WebElement menuOption = driver
				.findElement(By.xpath("//*[@id='main']/div[3]//form/div/div[2]/div[" + sliderCount + "]/div/div"));

		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript(scrollIntoView, menuOption);

		Actions actions = new Actions(driver);
		actions.moveToElement(menuOption).perform();
		Thread.sleep(1000);

		WebElement deleteButton = driver.findElement(By
				.xpath("//*[@id='sortable']//div[" + sliderCount + "]/div[1]/div[1]/div[1]/div[1]/a[@title='Delete']"));
		deleteButton.click();
		Thread.sleep(2000);
		commonXpath.gallerySubmit.click();

		commonFunc.checkElementAvailableWithAttributeCompare(commonXpath.elementList, commonXpath.element,
				CommonVariables.STYLE, CommonVariables.DISPLAY_NONE);
	}

	@Then("Make Galleries status and verify {string}")
	public void makeGalleriesActiveAndVerifySuccessMessage(String message) throws InterruptedException {
		Thread.sleep(2000);
		commonXpath.selectCheckBox.click();
		Thread.sleep(2000);
		commonXpath.inactiveButton.click();
		Thread.sleep(500);
		commonXpath.submitButton.click();
		Thread.sleep(2000);
		String msg = "Selected gallery(s) have been inactivated successfully.";
		if (commonXpath.inactiveMsgGallery.getText().equals(msg)) {
			assert true;
		}
		commonXpath.selectCheckBox.click();
		Thread.sleep(2000);
		commonXpath.activeButton.click();
		Thread.sleep(500);
		commonXpath.submitButton.click();
		Thread.sleep(2000);
		String msg1 = "Selected gallery(s) have been activated successfully.";
		assertTrue(commonXpath.inactiveMsgGallery.getText().equals(msg1));
	}

	@Then("Make Partner status and verify {string}")
	public void makePartnerActiveAndVerifySuccessMessage(String message) throws InterruptedException {
		partners.inactivePartner("have been inactivated successfully.");
		partners.selectPartner();
		partners.activePartner("have been activated successfully.");
	}

	@When("Select Partner")
	public void selectPartner() throws InterruptedException {
		partners.selectPartner();
	}

	@And("Get CopyRight Content from Settings")
	public void getCopyRightContentFromSettings() {
		settings.frontEndAccordion.click();
		settings.enterCopyrighttext("abc");
		settings.clickonSavebutton();
		commonFunc.checkElementAvailableWithAttributeCompare(commonXpath.elementList, commonXpath.element,
				CommonVariables.STYLE, CommonVariables.DISPLAY_NONE);

		settings.frontEndAccordion.click();
		Frame.commonVariables.setCopyRightText(settings.getCopyrighttext());
	}

	@Then("Make Home Page Sliders status and verify {string}")
	public void makeHomePageSlidersActiveAndVerifySuccessMessage(String message) throws InterruptedException {
		Thread.sleep(2000);
		commonXpath.selectCheckBox.click();
		Thread.sleep(2000);
		commonXpath.inactiveButton.click();
		commonXpath.submitButton.click();
		Thread.sleep(2000);
		String msg = "Selected Slider(s) have been inactivated successfully.";
		if (commonXpath.inactiveMsgGallery.getText().equals(msg)) {
			assert true;
		}
		commonXpath.selectCheckBox.click();
		Thread.sleep(2000);
		commonXpath.activeButton.click();
		commonXpath.submitButton.click();
		Thread.sleep(2000);
		String msg1 = "Selected Slider(s) have been activated successfully.";
		assertTrue(commonXpath.inactiveMsgGallery.getText().equals(msg1));
	}

	@And("{string} values change and verify")
	public void adminSettingsValuesChangeAndVerify(String accordion)
			throws WriteException, IOException, InterruptedException {
		switch (accordion) {
		case "Admin Settings":
			settings.backupAdminSettings();
			settings.adminSettings("abc", "qwe");
			break;

		case "Front End Settings":
			settings.backupFrontEndSettings();
			settings.frontEndSettings("Dev Digital", "xyz", "copy text", "front meta desc");
			break;

		case "Company Details settings":
			settings.backupCompanyDetailsSettings();
			settings.companySettings("companyName", "addressline1", "addressline2", "city", "state", "country",
					"12345");
			settings.companySettings("1234567890", "abc@yopmail.com");
			break;

		case "Email settings":
			settings.backupEmailSettings();
			settings.emailSettings("DevDigital", "contact@laravelcms.com", "contact@laravelcms.com");
			break;

		case "SEO settings":
			settings.seoSettings();
			break;

		default:
			break;
		}
	}
}