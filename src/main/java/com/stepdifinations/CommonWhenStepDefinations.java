package com.stepdifinations;

import org.openqa.selenium.WebDriver;

import com.base.TestBase;
import com.utilities.CommonFunc;

public class CommonWhenStepDefinations {

	WebDriver driver;
	CommonFunc commonFunc;

	public CommonWhenStepDefinations() {
		driver = TestBase.getDriver();
		commonFunc = new CommonFunc(driver);
	}
}
