package com.stepdifinations;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Locale;

import org.apache.log4j.Logger;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.base.Frame;
import com.base.TestBase;
import com.basicactions.DropDownHelper;
import com.basicactions.ExcelHelper;
import com.basicactions.LogHelper;
import com.basicactions.WaitHelper;
import com.pages.adminpages.blogs.Blogs;
import com.pages.adminpages.settings.Settings;
import com.pages.frontpages.Frontpages;
import com.pages.frontpages.contactus.FrontContactUS;
import com.utilities.CommonFunc;
import com.utilities.CommonVariables;
import com.utilities.CommonXpath;
import com.utilities.FilesPaths;
import com.utilities.ReadPropFile;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class FrontStepDefination {
	WebDriver driver;

	ReadPropFile readPropFile;
	WaitHelper waitHelper;
	CommonFunc commonFunc;
	Frontpages frontpages;
	CommonXpath commonXpath;
	FrontContactUS frontContactUs;
	DateTimeFormatter dtf;
	LocalDateTime now;
	Settings settings;
	DropDownHelper dropDownHelper;
	Blogs blogs;
	static final String HOMEPAGETEXT = "Home Page";
	String scrollIntoViewText = "arguments[0].scrollIntoView();";
	String[] socialLink;
	CommonWhenStepDefinations commonWhenStepDefinations;
	String pageTitle;
	String metaTitle;
	String metaDescription;
	boolean altTextSet = false;
	static Logger log = LogHelper.getLogger(FrontStepDefination.class);

	public FrontStepDefination() {
		driver = TestBase.getDriver();
		PageFactory.initElements(driver, this);
		waitHelper = new WaitHelper(driver);
		commonFunc = new CommonFunc(driver);
		frontpages = new Frontpages(driver);
		commonXpath = new CommonXpath(driver);
		settings = new Settings(driver);
		dropDownHelper = new DropDownHelper(driver);
		blogs = new Blogs(driver);
		frontContactUs = new FrontContactUS(driver);
		commonWhenStepDefinations = new CommonWhenStepDefinations();
	}

	@And("Verify {string} banner text")
	public void verifyBannerText(String subPage) throws InterruptedException {
		Thread.sleep(1500);
		Assert.assertEquals(commonXpath.subPageBannerText.getText().trim(), subPage);
	}

	@And("Verify About Us content")
	public void verifyAboutUsContent() {
		int size = commonXpath.aboutUsContentList.size();
		assertTrue(size > 0);

		for (int pTag = 1; pTag <= size; pTag++) {
			assertTrue(!commonXpath.aboutUsContent.getText().isEmpty());
		}
	}

	@And("Verify About Us image")
	public void verifyAboutUsImage() {
		int size = commonXpath.aboutUsImageList.size();
		assertTrue(size > 0);

		assertTrue(!commonXpath.aboutUsImage.getAttribute("src").isEmpty());
	}

	@Then("Verify Blog in front side")
	public void verifyBlogInFrontSide() throws InterruptedException {
		blogs.verifyBlogTitle();
		blogs.verifyBlogAuthorName();
		blogs.verifyShareOption();
		blogs.verifyBackToBlogList();
	}

	@And("Verify About Us mission statement is {string}")
	public void verifyAboutUsMissionStatement(String availableStatus) {
		int missionTitleSize = commonXpath.aboutUsMissionTitleList.size();
		int missionContentSize = commonXpath.aboutUsMissionContentList.size();

		if (availableStatus.equalsIgnoreCase("available")) {
			assertTrue(missionTitleSize > 0);
			Assert.assertEquals(commonXpath.aboutUsMissionTitle.getText().trim(), "Mission");
			assertTrue(missionContentSize > 0);
			assertTrue(!commonXpath.aboutUsMissionContent.getText().isEmpty());
		} else {
			assertTrue(missionTitleSize <= 0);
			assertTrue(missionContentSize <= 0);
		}
	}

	@And("Open {string} accordion")
	public void openAboutUsSettingsAccordion(String accordionName) throws InterruptedException {
		int size = driver.findElements(By.xpath("//*[@id='site-config-accordion']/div")).size();
		for (int i = 1; i <= size; i++) {
			Thread.sleep(1000);
			if (driver.findElement(By.xpath("//*[@id='site-config-accordion']/div[" + i + "]/div[1]/h5[1]/a[1]"))
					.getText().trim().equals(accordionName)) {
				driver.findElement(By.xpath("//*[@id='site-config-accordion']/div[" + i + "]/div[1]/h5[1]/a[1]"))
						.click();
				if (accordionName.equals("About us settings")) {
					JavascriptExecutor js = (JavascriptExecutor) driver;
					js.executeScript(scrollIntoViewText, commonXpath.aboutUsMissionStatementTextbox);
					Thread.sleep(1000);
				}
			}
		}
	}

	@And("{string} mission statement text")
	public void addRemoveMissionStatementText(String type) {
		switch (type) {
		case "Add":
			commonXpath.aboutUsMissionStatementTextbox.clear();
			commonXpath.aboutUsMissionStatementTextbox.sendKeys("abc text data");
			break;

		case "Remove":
			commonXpath.aboutUsMissionStatementTextbox.clear();
			break;

		default:
			break;
		}
		commonXpath.settingSave.click();

	}

	@And("Verify the Front detail")
	public void verifytheFrontDetail() {
		int size = commonXpath.aboutImage.size();
		if (size > 0) {
			assert true;
		} else {
			assertTrue(false);
		}

		size = commonXpath.aboutus_Content.size();
		if (size > 0) {
			assert true;
		} else {
			assertTrue(false);
		}
	}

	@And("Data update and verify details for Admin Setting section")
	public void dataUpdateAndVerifyDetailsForAdminSettingsSection() throws InterruptedException {
		ExcelHelper.readDataFromXLS(FilesPaths.TEMPLATE9WIREFRAME, CommonVariables.SETTINGS);

		// About Us Settings
		String misssionstatement = ExcelHelper.getData(16, 2);

		settings.entermissionStatement(misssionstatement);
	}

	@And("Get count of Active Featured Services")
	public void getCountOfActiveFeaturedServices() throws InterruptedException {
		commonFunc.checkElementAvailableWithAttributeCompare(commonXpath.elementList, commonXpath.element,
				CommonVariables.STYLE, CommonVariables.DISPLAY_NONE);

		commonXpath.searchButton.click();

		dropDownHelper.selectByVaule(commonXpath.status, "1");
		Thread.sleep(1500);

		dropDownHelper.selectByVaule(commonXpath.featruedDropdown, "1");
		Thread.sleep(1500);

		commonXpath.sliderSearchButton.click();

		commonFunc.checkElementAvailableWithAttributeCompare(commonXpath.elementList, commonXpath.element,
				CommonVariables.STYLE, CommonVariables.DISPLAY_NONE);

		Frame.commonVariables.setNumRecord(
				String.valueOf(driver.findElements(By.xpath("//*[@id='DataTables_Table_0']/tbody/tr/td[4]/a")).size()));
	}

	@And("Get count of Active Home Page Sliders")
	public void getCountOfActiveHomePageSliders() {
		commonFunc.checkElementAvailableWithAttributeCompare(commonXpath.elementList, commonXpath.element,
				CommonVariables.STYLE, CommonVariables.DISPLAY_NONE);

		commonXpath.searchButton.click();

		dropDownHelper.selectByVaule(commonXpath.status, "1");

		commonXpath.sliderSearchButton.click();

		commonFunc.checkElementAvailableWithAttributeCompare(commonXpath.elementList, commonXpath.element,
				CommonVariables.STYLE, CommonVariables.DISPLAY_NONE);

		int allImagesCount = commonXpath.imagesCMS.size();

		Frame.commonVariables.setNumRecord(String.valueOf(allImagesCount));
	}

	@Then("Get count of Active Galleries")
	public void getCountOfActiveGalleries() throws InterruptedException {
		commonFunc.checkElementAvailableWithAttributeCompare(commonXpath.elementList, commonXpath.element,
				CommonVariables.STYLE, CommonVariables.DISPLAY_NONE);

		commonXpath.searchButton.click();

		dropDownHelper.selectByVaule(commonXpath.status, "1");

		commonXpath.sliderSearchButton.click();

		Thread.sleep(1500);

		commonFunc.checkElementAvailableWithAttributeCompare(commonXpath.elementList, commonXpath.element,
				CommonVariables.STYLE, CommonVariables.DISPLAY_NONE);

		Frame.commonVariables.setNumRecord(String.valueOf(commonXpath.imagesCMS.size()));
	}

	@Then("Get count of Active Partners")
	public void getCountOfActivePartners() {
		commonFunc.checkElementAvailableWithAttributeCompare(commonXpath.elementList, commonXpath.element,
				CommonVariables.STYLE, CommonVariables.DISPLAY_NONE);

		commonXpath.searchButton.click();

		dropDownHelper.selectByVaule(commonXpath.status, "1");

		commonXpath.sliderSearchButton.click();

		commonFunc.checkElementAvailableWithAttributeCompare(commonXpath.elementList, commonXpath.element,
				CommonVariables.STYLE, CommonVariables.DISPLAY_NONE);

		Frame.commonVariables.setNumRecord(String.valueOf(commonXpath.imagesCMS.size()));
	}

	@Then("Get count of Active {string}")
	public void getCountOfActiveModule(String moduleName) throws InterruptedException {
		commonFunc.checkElementAvailableWithAttributeCompare(commonXpath.elementList, commonXpath.element,
				CommonVariables.STYLE, CommonVariables.DISPLAY_NONE);

		commonXpath.searchButton.click();

		dropDownHelper.selectByVaule(commonXpath.status, "1");

		commonXpath.sliderSearchButton.click();

		commonFunc.checkElementAvailableWithAttributeCompare(commonXpath.elementList, commonXpath.element,
				CommonVariables.STYLE, CommonVariables.DISPLAY_NONE);

		Frame.commonVariables.setNumRecord(
				String.valueOf(driver.findElements(By.xpath("//*[@id='DataTables_Table_0']/tbody/tr/td[4]/a")).size()));

		Thread.sleep(1500);
		if (moduleName.equals("Blogs")) {
			Frame.commonVariables.setBlogCategoryCount(
					String.valueOf(driver.findElements(By.xpath("//*[@id='category']/option")).size() - 1));
		}
	}

	@And("Open Front site and scroll down {string} Module")
	public void openFrontSiteAndScrollDownModule(String moduleName) throws ParseException {
		commonFunc.urlGet();
		driver.get(Frame.commonVariables.getFrontURL());
		JavascriptExecutor js = (JavascriptExecutor) driver;

		switch (moduleName) {
		case "Services":
			String serviceImagesCountCMS = Frame.commonVariables.getNumRecord();
			if (serviceImagesCountCMS.equals("0")) {
				int size = commonXpath.servicesSectionList.size();
				assertTrue(size <= 0);
			} else {
				js.executeScript(scrollIntoViewText, commonXpath.servicesSection);
				Assert.assertEquals(commonXpath.servicesTitle.getText().trim(), moduleName);
			}
			break;

		case "Work Gallery":
			String galleryImagesCountCMS = Frame.commonVariables.getNumRecord();
			if (galleryImagesCountCMS.equals("0")) {
				int size = commonXpath.gallerySectionList.size();
				assertTrue(size <= 0);
			} else {
				js.executeScript(scrollIntoViewText, commonXpath.gallerySection);
				Assert.assertEquals(commonXpath.galleryTitle.getText().trim(), moduleName);
			}
			break;

		case "Partners":
			String partnerImagesCountCMS = Frame.commonVariables.getNumRecord();
			if (partnerImagesCountCMS.equals("0")) {
				int size = commonXpath.partnersSectionList.size();
				assertTrue(size <= 0);
			} else {
				js.executeScript(scrollIntoViewText, commonXpath.partnersSection);
				Assert.assertEquals(commonXpath.partnersTitle.getText().trim(), moduleName);
			}
			break;

		case "Contact Us":
			js.executeScript(scrollIntoViewText, commonXpath.contactUsSection);
			break;

		case "Footer":
			js.executeScript(scrollIntoViewText, commonXpath.footerSection);
			break;

		case "Blogs":
			js.executeScript(scrollIntoViewText, commonXpath.blogsSection);
			Assert.assertEquals(commonXpath.blogsTitle.getText().trim(), moduleName);
			break;

		default:
			break;
		}
	}

	@And("Remove {string} URL")
	public void removeSocialURL(String type) throws InterruptedException {
		switch (type) {
		case "Facebook":
			settings.enterFacebook("");
			break;

		case "Twitter":
			settings.enterTwitter("");
			break;

		case "LinkedIn":
			settings.enterLinkedIn("");
			break;

		default:
			break;
		}
		settings.clickonSavebutton();

		int size = driver.findElements(By.xpath("//*[@id='site-config-accordion']/div")).size();
		for (int i = 1; i <= size; i++) {
			Thread.sleep(1000);
			if (driver.findElement(By.xpath("//*[@id='site-config-accordion']/div[" + i + "]/div[1]/h5[1]/a[1]"))
					.getText().trim().equals("Social Networking Links")) {
				driver.findElement(By.xpath("//*[@id='site-config-accordion']/div[" + i + "]/div[1]/h5[1]/a[1]"))
						.click();
			}
		}

		if (settings.getFacebook().isEmpty() && settings.getTwitter().isEmpty() && settings.getLinkedIn().isEmpty()) {
			Frame.commonVariables.setSocialMediaLinksFront(false);
		}

	}

	@Then("Verify validation for maximum selection Social Networking Links")
	public void verifyMaximumSelectionSocialNetworkingLinks() throws InterruptedException {
		int socialLinksCount = driver.findElements(By.xpath("//*[@id='accordion-item-4']/div/div/div")).size();
		int count = 0;
		int i = 2;
		while (i <= socialLinksCount) {
			if (driver.findElement(By.xpath("//*[@id='accordion-item-4']/div/div/div[" + i + "]/div[1]/input[1]"))
					.getAttribute("checked") != null) {
				count++;
				assert true;
			}
			i++;
		}

		i = 2;
		if (count == 4) {
			while (i <= socialLinksCount) {
				if (driver.findElement(By.xpath("//*[@id='accordion-item-4']/div/div/div[" + i + "]/div[1]/input[1]"))
						.getAttribute("checked") == null) {
					driver.findElement(By.xpath("//*[@id='accordion-item-4']/div/div/div[" + i + "]/div[1]/input[1]"))
							.click();
					Thread.sleep(1500);
					assertTrue(driver.findElement(By.xpath("//*[@id='accordion-item-4']/div/div/div[1]")).getText()
							.trim().contains("Maximum four Social Networking Links allow to be active."));
					Thread.sleep(1500);
					driver.findElement(By.xpath("//*[@id='globalSocialMessage']/div/button")).click();
				}
				i++;
			}
		}
	}

	@And("Verify required validation message")
	public void verifyRequiredValidationMessage() throws InterruptedException {
		int socialLinksCount = driver.findElements(By.xpath("//*[@id='accordion-item-4']/div/div/div")).size();
		int i = 2;
		while (i <= socialLinksCount) {
			if (driver.findElement(By.xpath("//*[@id='accordion-item-4']/div/div/div[" + i + "]/div[1]/input[1]"))
					.getAttribute("checked") != null) {
				driver.findElement(By.xpath("//*[@id='accordion-item-4']/div/div/div[" + i + "]/div[1]/input[1]"))
						.click();
			}
			i++;
		}

		i = 2;
		while (i <= socialLinksCount) {
			String socialMedia = driver
					.findElement(By.xpath("//*[@id='accordion-item-4']/div/div/div[" + i + "]/div[1]/input[2]"))
					.getAttribute("id");

			switch (socialMedia) {
			case "facebook_link":
				validateSocialIcons(i, "The facebook link field is required.");
				break;

			case "instagram_link":
				validateSocialIcons(i, "The instagram link field is required.");
				break;

			case "linkedin_link":
				validateSocialIcons(i, "The linkedin link field is required.");
				break;

			case "pinterest_link":
				validateSocialIcons(i, "The pinterest link field is required.");
				break;

			case "snapchat_link":
				validateSocialIcons(i, "The snapchat link field is required.");
				break;

			case "tiktok_link":
				validateSocialIcons(i, "The tiktok link field is required.");
				break;

			case "twitter_link":
				validateSocialIcons(i, "The twitter link field is required.");
				break;

			case "yelp_link":
				validateSocialIcons(i, "The yelp link field is required.");
				break;

			case "youtube_link":
				validateSocialIcons(i, "The youtube link field is required.");
				break;

			default:
				break;
			}

			i++;
		}
	}

	public void validateSocialIcons(int i, String checkStr) throws InterruptedException {
		driver.findElement(By.xpath("//*[@id='accordion-item-4']/div/div/div[" + i + "]/div[1]/input[1]")).click();

		driver.findElement(By.xpath("//*[@id='accordion-item-4']/div/div/div[" + i + "]/div[1]/input[2]")).clear();
		driver.findElement(By.xpath("//*[@id='accordion-item-4']/div/div/div[" + i + "]/div[1]/input[2]"))
				.sendKeys(Keys.TAB);
		Thread.sleep(1000);

		assertTrue(driver.findElement(By.xpath("//*[@id='accordion-item-4']/div/div/div[" + i + "]/div[1]/div[1]"))
				.getText().equals(checkStr));

		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='accordion-item-4']/div/div/div[" + i + "]/div[1]/input[1]")).click();
	}

	@And("Set social media icons {string}")
	public void setSocialMediaIcons(String socialLinks) throws InterruptedException {
		socialLink = socialLinks.split(",");

		int socialLinksCount = driver.findElements(By.xpath("//*[@id='accordion-item-4']/div/div/div")).size();

		int i = 2;
		ArrayList<String> ar = new ArrayList<>();

		while (i <= socialLinksCount) {
			ar.add(driver.findElement(By.xpath("//*[@id='accordion-item-4']/div/div/div[" + i + "]/label[1]")).getText()
					.trim());
			i++;
		}

		// Set sequence of array
		int newPos = 0;
		i = 0;
		while (i < ar.size()) {
			int j = 0;
			while (j < socialLink.length) {
				if (socialLink[j].trim().equals(ar.get(i).trim())) {
					String temp = socialLink[j];
					socialLink[j] = socialLink[newPos];
					socialLink[newPos] = temp;
					newPos++;
				}
				j++;
			}
			i++;
		}

		i = 2;
		while (i <= socialLinksCount) {
			if (driver.findElement(By.xpath("//*[@id='accordion-item-4']/div/div/div[" + i + "]/div[1]/input[1]"))
					.getAttribute("checked") != null) {
				driver.findElement(By.xpath("//*[@id='accordion-item-4']/div/div/div[" + i + "]/div[1]/input[1]"))
						.click();
			}
			i++;
		}

		i = 0;
		while (i < socialLink.length) {
			switch (socialLink[i].trim()) {
			case "Facebook":
				socialMediaLinkSet("facebook_link");
				break;

			case "Instagram":
				socialMediaLinkSet("instagram_link");
				break;

			case "LinkedIn":
				socialMediaLinkSet("linkedin_link");
				break;

			case "Pinterest":
				socialMediaLinkSet("pinterest_link");
				break;

			case "Snapchat":
				socialMediaLinkSet("snapchat_link");
				break;

			case "Tiktok":
				socialMediaLinkSet("tiktok_link");
				break;

			case "Twitter":
				socialMediaLinkSet("twitter_link");
				break;

			case "Yelp":
				socialMediaLinkSet("yelp_link");
				break;

			case "Youtube":
				socialMediaLinkSet("youtube_link");
				break;

			default:
				break;
			}
			i++;
		}

		commonXpath.settingSave.click();
		Thread.sleep(1500);
	}

	public void socialMediaLinkSet(String socialType) throws InterruptedException {
		int socialLinksCount = driver.findElements(By.xpath("//*[@id='accordion-item-4']/div/div/div")).size();
		int i = 2;
		while (i <= socialLinksCount) {
			String socialMedia = driver
					.findElement(By.xpath("//*[@id='accordion-item-4']/div/div/div[" + i + "]/div[1]/input[2]"))
					.getAttribute("id");
			boolean urlEntered = false;
			if (socialType.equalsIgnoreCase(socialMedia)) {
				Thread.sleep(1000);
				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("arguments[0].click();", driver
						.findElement(By.xpath("//*[@id='accordion-item-4']/div/div/div[" + i + "]/div[1]/input[1]")));
				driver.findElement(By.xpath("//*[@id='accordion-item-4']/div/div/div[" + i + "]/div[1]/input[2]"))
						.clear();
				switch (socialMedia) {
				case "facebook_link":
					driver.findElement(By.xpath("//*[@id='accordion-item-4']/div/div/div[" + i + "]/div[1]/input[2]"))
							.sendKeys("https://www.facebook.com/DevDigital");
					urlEntered = true;
					break;

				case "instagram_link":
					driver.findElement(By.xpath("//*[@id='accordion-item-4']/div/div/div[" + i + "]/div[1]/input[2]"))
							.sendKeys("https://www.instagram.com/dev_digital/");
					break;

				case "linkedin_link":
					driver.findElement(By.xpath("//*[@id='accordion-item-4']/div/div/div[" + i + "]/div[1]/input[2]"))
							.sendKeys("https://www.linkedin.com/company/dev-digital-llc-");
					break;

				case "pinterest_link":
					driver.findElement(By.xpath("//*[@id='accordion-item-4']/div/div/div[" + i + "]/div[1]/input[2]"))
							.sendKeys("https://www.pinterest.com/");
					break;

				case "snapchat_link":
					driver.findElement(By.xpath("//*[@id='accordion-item-4']/div/div/div[" + i + "]/div[1]/input[2]"))
							.sendKeys("https://www.snapchat.com/");
					break;

				case "tiktok_link":
					driver.findElement(By.xpath("//*[@id='accordion-item-4']/div/div/div[" + i + "]/div[1]/input[2]"))
							.sendKeys("https://www.tiktok.com/");
					break;

				case "twitter_link":
					driver.findElement(By.xpath("//*[@id='accordion-item-4']/div/div/div[" + i + "]/div[1]/input[2]"))
							.sendKeys("https://twitter.com/dev_digital?lang=en");
					break;

				case "yelp_link":
					driver.findElement(By.xpath("//*[@id='accordion-item-4']/div/div/div[" + i + "]/div[1]/input[2]"))
							.sendKeys("https://www.yelp.com/");
					break;

				case "youtube_link":
					driver.findElement(By.xpath("//*[@id='accordion-item-4']/div/div/div[" + i + "]/div[1]/input[2]"))
							.sendKeys("https://www.youtube.com/");
					break;

				default:
					break;
				}

				if (urlEntered) {
					break;
				}
			}

			i++;
		}
	}

	@Then("Verify social media icons at front side")
	public void verifySocialMediaIconsAtFrontSide() {
		int liCount = driver.findElements(By.xpath("//*[@id=\"navbarNavAltMarkup\"]/ul/li")).size();

		int i = 1;
		while (i <= socialLink.length) {
			assertTrue(
					driver.findElement(By.xpath("//*[@id=\"navbarNavAltMarkup\"]/ul/li[" + liCount + "]/a[" + i + "]"))
							.getAttribute("title").equalsIgnoreCase(socialLink[i - 1].trim()));

			assertTrue(
					driver.findElement(By.xpath("//*[@id=\"navbarNavAltMarkup\"]/ul/li[" + liCount + "]/a[" + i + "]"))
							.getAttribute("href").toLowerCase(Locale.ENGLISH)
							.contains(socialLink[i - 1].trim().toLowerCase(Locale.ENGLISH)));
			i++;
		}
	}

	@Then("Verify the banner title for front if {string}")
	public void verifyTheBannerTitleForFront(String value) {
		int size = commonXpath.bannerFrontTitle.size();
		if (value.equals("Yes")) {
			assertTrue(size > 0);
		} else if (value.equals("No")) {
			assertTrue(size <= 0);
		}
	}

	@And("Verify sliders count as per CMS")
	public void verifySlidersCountAsPerCMS() {
		String bannerSliderCountCMS = Frame.commonVariables.getNumRecord();
		Frame.commonVariables.setNumRecord(
				String.valueOf(driver.findElements(By.xpath("//*[@id='carouselCaptions']/div[1]/div")).size()));
		String bannerSliderCountFront = Frame.commonVariables.getNumRecord();
		Assert.assertEquals(bannerSliderCountFront, bannerSliderCountCMS);
	}

	public void socialLinks(boolean socialLinks, int socialLinksCount, String menuXpathString, int count, String type) {
		if (socialLinks) {
			boolean socialLinkFound = false;
			for (int i = 1; i <= socialLinksCount; i++) {
				String menu = driver.findElement(By.xpath(menuXpathString + count + "]/a[" + i + "]"))
						.getAttribute("title").toLowerCase(Locale.ENGLISH);
				log.info("menu name = " + menu);
				switch (type) {
				case "Facebook Remove":
					assertTrue(!menu.contains("facebook"));
					break;

				case "Twitter Remove":
					assertTrue(!menu.contains("twitter"));
					break;

				case "LinkedIn Remove":
					assertTrue(!menu.contains("linkedin"));
					break;

				case "Facebook Add":
					assertTrue(menu.contains("facebook"));
					socialLinkFound = true;
					break;

				case "Twitter Add":
					assertTrue(menu.contains("twitter"));
					socialLinkFound = true;
					break;

				case "LinkedIn Add":
					assertTrue(menu.contains("linkedin"));
					socialLinkFound = true;
					break;

				default:
					break;
				}

				if (socialLinkFound) {
					break;
				}

			}
		}
	}

	public void checkSocialMediaLinks(String menuXpathString, int count) {
		int size = driver.findElements(By.xpath(menuXpathString + count + "]/a[1]")).size();
		if (size > 0) {
			assertTrue(false);
		}
	}

	@And("Verify the header details")
	public void verifyTheHeaderDetails() {
		ExcelHelper.readDataFromXLS(FilesPaths.TEMPLATE9WIREFRAME, CommonVariables.HEADER);

		for (int i = 1; i <= 6; i++) {
			String m1 = driver.findElement(By.xpath("//*[@id='navbarNavAltMarkup']/ul/li[" + i + "]/a")).getText();
			Assert.assertEquals(m1, ExcelHelper.getData(1, i));
		}
	}

	@And("Verify Service Listing on {string} as per CMS")
	public void verifyServiceListingAsPerCMS(String type) {
		String serviceListCountCMS;
		String serviceListCountFront;
		switch (type) {
		case HOMEPAGETEXT:
			serviceListCountCMS = Frame.commonVariables.getNumRecord();
			Frame.commonVariables.setNumRecord(String.valueOf(commonXpath.servicesListCountFront.size()));
			serviceListCountFront = Frame.commonVariables.getNumRecord();
			Assert.assertEquals(serviceListCountFront, serviceListCountCMS);
			break;

		case "Detail Page":
			serviceListCountCMS = Frame.commonVariables.getNumRecord();
			if (serviceListCountCMS.equals("0")) {
				Assert.assertEquals("No service(s) found",
						commonXpath.servicesDetailPageNoServicesFoundText.getText().trim());
			} else {
				Frame.commonVariables.setNumRecord(String.valueOf(commonXpath.servicesListingList.size()));
				serviceListCountFront = Frame.commonVariables.getNumRecord();
				Assert.assertEquals(serviceListCountFront, serviceListCountCMS);
			}
			break;

		default:
			break;
		}
	}

	@And("Verify Work Gallery on {string} as per CMS")
	public void verifyWorkGalleryAsPerCMS(String type) {
		String galleryImagesCountCMS;
		String galleryImagesCountFront;
		switch (type) {
		case HOMEPAGETEXT:
			galleryImagesCountCMS = Frame.commonVariables.getNumRecord();
			Frame.commonVariables.setNumRecord(String.valueOf(commonXpath.galleryImagesCount.size()));
			galleryImagesCountFront = Frame.commonVariables.getNumRecord();
			Assert.assertEquals(galleryImagesCountFront, galleryImagesCountCMS);
			break;

		case "Detail Page":
			galleryImagesCountCMS = Frame.commonVariables.getNumRecord();
			if (galleryImagesCountCMS.equals("0")) {
				Assert.assertEquals("No work gallery(s) found",
						commonXpath.galleryImagesDetailPageNoGalleryFoundText.getText().trim());
			} else {
				Frame.commonVariables.setNumRecord(String.valueOf(commonXpath.galleryImagesDetailPageCount.size()));
				galleryImagesCountFront = Frame.commonVariables.getNumRecord();
				Assert.assertEquals(galleryImagesCountFront, galleryImagesCountCMS);
			}
			break;

		default:
			break;
		}
	}

	@And("Verify Blogs on {string} as per CMS")
	public void verifyBlogsAsPerCMS(String type) throws InterruptedException {
		String blogsImagesCountCMS;
		String blogsImagesCountFront;

		String blogCategoryCountCMS;
		String blogCategoryCountFront;

		blogCategoryCountCMS = Frame.commonVariables.getBlogCategoryCount();
		Frame.commonVariables.setBlogCategoryCount(String.valueOf(
				driver.findElements(By.xpath("//h5[contains(text(),'Category')]//following::div[1]/ul[1]/li")).size()));

		blogCategoryCountFront = Frame.commonVariables.getBlogCategoryCount();
		Assert.assertEquals(blogCategoryCountFront, blogCategoryCountCMS);

		Thread.sleep(1500);

		blogsImagesCountCMS = Frame.commonVariables.getNumRecord();
		Thread.sleep(2000);

		if (Integer.parseInt(blogsImagesCountCMS) > Integer.parseInt(Frame.commonVariables.getBlogRecordPage())) {
			while (true) {
				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("arguments[0].scrollIntoView();", commonXpath.blogsLoadMore);
				Thread.sleep(1500);

				if (commonXpath.blogsLoadMore.getAttribute("style").isEmpty()
						|| commonXpath.blogsLoadMore.getAttribute("style") == null
						|| !commonXpath.blogsLoadMore.getAttribute("style").contains("display: none;")) {
					commonXpath.blogsLoadMore.click();
					Thread.sleep(1000);
				} else {
					break;
				}
			}
		}

		Thread.sleep(2000);
		Frame.commonVariables.setNumRecord(String.valueOf(commonXpath.blogsDetailPageCount.size()));
		blogsImagesCountFront = Frame.commonVariables.getNumRecord();
		Assert.assertEquals(blogsImagesCountFront, blogsImagesCountCMS);

	}

	@And("Verify Blogs Content")
	public void verifyBlogsContent() throws InterruptedException {
		// Search Textbox
		int size = commonXpath.blogsSearchTextBox.size();
		assertTrue(size > 0);

		if (Integer.parseInt(Frame.commonVariables.getNumRecord()) > 0) {
			commonXpath.blogsDetailPageFirstBlog.click();
			Thread.sleep(2500);

			// Blog Name
			size = commonXpath.blogsDetailPageTitle.size();
			assertTrue(size > 0);

			// Share Links
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].scrollIntoView();", commonXpath.blogsDetailPageSocialMediaSection);
			size = commonXpath.blogsDetailPageSocialMediaSectionList.size();
			assertTrue(size > 0);

			size = commonXpath.blogsDetailPageSocialMediaLinks.size();
			assertTrue(size > 0);

			// Back to Blog List button
			size = commonXpath.blogsDetailPageBackToBlogListButtonList.size();
			assertTrue(size > 0);

			Assert.assertEquals(commonXpath.blogsDetailPageBackToBlogListButton.getAttribute("href"),
					Frame.commonVariables.getFrontURL() + "blogs");

			// Next Blog link
			size = commonXpath.blogsDetailPagePrevBlogLinkList.size();
			assertTrue(size <= 0);

			size = commonXpath.blogsDetailPageNextBlogLinkList.size();
			assertTrue(size > 0);

			commonXpath.blogsDetailPageNextBlogLink.click();
			Thread.sleep(2500);

			// Previous Blog link
			size = commonXpath.blogsDetailPagePrevBlogLinkList.size();
			assertTrue(size > 0);

			size = commonXpath.blogsDetailPageNextBlogLinkList.size();
			if (Integer.parseInt(Frame.commonVariables.getNumRecord()) > 2) {
				assertTrue(size > 0);
			} else {
				assertTrue(size <= 0);
			}
		}
	}

	@And("Verify Partners on {string} as per CMS")
	public void verifyPartnersAsPerCMS(String type) {
		String partnerImagesCountCMS;
		String partnerImagesCountFront;
		if (type.equals(HOMEPAGETEXT)) {
			partnerImagesCountCMS = Frame.commonVariables.getNumRecord();
			Frame.commonVariables.setNumRecord(String.valueOf(commonXpath.partnersImagesCount.size()));
			partnerImagesCountFront = Frame.commonVariables.getNumRecord();
			Assert.assertEquals(partnerImagesCountFront, partnerImagesCountCMS);
		}
	}

	@And("Verify Blogs on Home Page")
	public void verifyBlogsOnHomePage() throws InterruptedException {
		int size = commonXpath.blogsListHomePage.size();
		if (size < 2) {
			assertTrue(false);
		}
		Thread.sleep(1000);

		size = commonXpath.blogsHomePageViewMore.size();
		if (size <= 0) {
			assertTrue(false);
		}
	}

	@And("Verify content of footer")
	public void verifyContentOfFooter() {

		// CopyRight text
		String frontCopyRightText = commonXpath.frontCopyRightText.getText().trim().toLowerCase();
		String cmsCopyRightText = Frame.commonVariables.getCopyRightText().toLowerCase();

		dtf = DateTimeFormatter.ofPattern("yyyy");
		now = LocalDateTime.now();

		String currentYear = dtf.format(now);

		Assert.assertEquals(frontCopyRightText, "� " + currentYear + " " + cmsCopyRightText);

		// Home / Site Map Links
		Assert.assertEquals(commonXpath.frontFooterHomeLink.getText().trim(), "Home");
		Assert.assertEquals(commonXpath.frontFooterHomeLink.getAttribute("href"), driver.getCurrentUrl());

		Assert.assertEquals(commonXpath.frontFooterSitemapLink.getText().trim(), "Sitemap");
		Assert.assertEquals(commonXpath.frontFooterSitemapLink.getAttribute("href"),
				driver.getCurrentUrl() + "site-map");

		// Powered By Text
		Assert.assertEquals(commonXpath.frontFooterPowerdByText.getText().trim(),
				"Powered By DevDigital: Nashville Software Development");
		Assert.assertEquals(commonXpath.frontFooterPowerdByLink.getAttribute("href"), "https://www.devdigital.com/");
		Assert.assertEquals(commonXpath.frontFooterPowerdByLink.getAttribute("target"), "_blank");

	}

	@And("Verify the element in contact us")
	public void verifyTheElementInHomeContactUs() {
		frontContactUs.verifyElements();
	}

	@When("Changes for {string} menu as {string}")
	public void changesForMenuActiveInactive(String menu, String type) throws InterruptedException {
		if (menu.equalsIgnoreCase("About")) {
			driver.findElement(By.xpath("//*[@id=\"DataTables_Table_0\"]/tbody/tr[1]/td[4]/a")).click();
		} else if (menu.equalsIgnoreCase("Services")) {
			driver.findElement(By.xpath("//*[@id=\"DataTables_Table_0\"]/tbody/tr[2]/td[4]/a")).click();
		} else if (menu.equalsIgnoreCase("Gallery")) {
			driver.findElement(By.xpath("//*[@id=\"DataTables_Table_0\"]/tbody/tr[3]/td[4]/a")).click();
		} else if (menu.equalsIgnoreCase("Blog")) {
			driver.findElement(By.xpath("//*[@id=\"DataTables_Table_0\"]/tbody/tr[4]/td[4]/a")).click();
		} else if (menu.equalsIgnoreCase("Contact")) {
			driver.findElement(By.xpath("//*[@id=\"DataTables_Table_0\"]/tbody/tr[5]/td[4]/a")).click();
		}

		Thread.sleep(1500);

		pageTitle = driver.findElement(By.xpath("//*[@id='title']")).getAttribute("value");
		metaTitle = driver.findElement(By.xpath("//*[@id='meta_title']")).getAttribute("value");
		metaDescription = driver.findElement(By.xpath("//*[@id='meta_desc']")).getAttribute("value");

		Select select = new Select(driver.findElement(By.xpath("//*[@id='status']")));
		if (type.contains("Inactive")) {
			Frame.commonVariables.setStatus("Inactive");
			select.selectByValue("0");
		} else {
			Frame.commonVariables.setStatus("Active");
			select.selectByValue("1");

			if (type.contains("without Banner Image")) {
				if (driver.findElement(By.xpath("//*[@id='has_banner']")).getAttribute("checked") != null) {
					commonFunc.clickJSButton(driver.findElement(By.xpath("//*[@id='has_banner']//parent::label[1]")));
				}
			} else {
				if (driver.findElement(By.xpath("//*[@id='has_banner']")).getAttribute("checked") == null) {
					driver.findElement(By.xpath("//*[@id='has_banner']//parent::label[1]")).click();
					driver.findElement(By.xpath("//*[@id='temp_banner']"))
							.sendKeys(FilesPaths.EXTRA_FILES_FOLDER + "Banner.jpg");
					commonFunc.checkElementAvailableWithAttributeCompare(commonXpath.elementList, commonXpath.element,
							"style", "display: none;");
				}
				driver.findElement(By.xpath("//*[@id='banner_alt']")).clear();
				altTextSet = false;
			}
		}
	}

	@Then("Verify at front side for {string}")
	public void verifyAtFrontSideFor(String menu) throws InterruptedException {
		int liCount = driver.findElements(By.xpath("//*[@id=\"navbarNavAltMarkup\"]/ul/li")).size();
		int i = 2;
		while (i < liCount) {
			if (Frame.commonVariables.getStatus().equals("Active")) {
				if (driver.findElement(By.xpath("//*[@id=\"navbarNavAltMarkup\"]/ul/li[" + i + "]/a")).getText().trim()
						.equalsIgnoreCase(pageTitle)) {
					driver.findElement(By.xpath("//*[@id=\"navbarNavAltMarkup\"]/ul/li[" + i + "]/a")).click();
					break;
				}
			} else {
				assertTrue(!driver.findElement(By.xpath("//*[@id=\"navbarNavAltMarkup\"]/ul/li[" + i + "]/a")).getText()
						.trim().equalsIgnoreCase(pageTitle));
			}
			i++;
		}

		Thread.sleep(1500);

		if (Frame.commonVariables.getStatus().equals("Active")) {
			if (menu.contains("without Banner Image")) {
				assertTrue(driver.findElement(By.xpath("//main[@role='main']/section[1]/div[1]/div[1]/picture/img"))
						.getAttribute("alt").equals("inner banner"));

				assertTrue(driver.findElement(By.xpath("//main[@role='main']/section[1]/div[1]/div[1]/picture/img"))
						.getAttribute("src").contains("inner-banner"));
			} else {
				assertTrue(!driver.findElement(By.xpath("//main[@role='main']/section[1]/div[1]/div[1]/picture/img"))
						.getAttribute("alt").equals("inner banner"));

				if (altTextSet) {
					assertTrue(driver.findElement(By.xpath("//main[@role='main']/section[1]/div[1]/div[1]/picture/img"))
							.getAttribute("alt").equals("alt text 123"));
				} else {
					assertTrue(driver.findElement(By.xpath("//main[@role='main']/section[1]/div[1]/div[1]/picture/img"))
							.getAttribute("alt").equals(pageTitle));
				}
				assertTrue(!driver.findElement(By.xpath("//main[@role='main']/section[1]/div[1]/div[1]/picture/img"))
						.getAttribute("src").contains("inner-banner"));
			}

			assertTrue(driver.findElement(By.xpath("//meta[@property='og:title']")).getAttribute("content")
					.contains(metaTitle));
			assertEquals(driver.findElement(By.xpath("//meta[@name='description']")).getAttribute("content"),
					metaDescription);

			if (menu.contains("Blog")) {
				driver.findElement(By.xpath("//*[@id='blog_listing']/div/div/a")).click();

				assertTrue(!driver.findElement(By.xpath("//meta[@property='og:title']")).getAttribute("content")
						.contains(metaTitle));
				assertTrue(!driver.findElement(By.xpath("//meta[@name='description']")).getAttribute("content")
						.equals(metaDescription));

				if (menu.contains("without Banner Image")) {
					assertTrue(driver.findElement(By.xpath("//main[@role='main']/section[1]/div[1]/div[1]/picture/img"))
							.getAttribute("alt").equals("inner banner"));

					assertTrue(driver.findElement(By.xpath("//main[@role='main']/section[1]/div[1]/div[1]/picture/img"))
							.getAttribute("src").contains("inner-banner"));
				} else {
					assertTrue(
							!driver.findElement(By.xpath("//main[@role='main']/section[1]/div[1]/div[1]/picture/img"))
									.getAttribute("alt").equals("inner banner"));

					if (altTextSet) {
						assertTrue(driver
								.findElement(By.xpath("//main[@role='main']/section[1]/div[1]/div[1]/picture/img"))
								.getAttribute("alt").equals("alt text 123"));
					} else {
						assertTrue(driver
								.findElement(By.xpath("//main[@role='main']/section[1]/div[1]/div[1]/picture/img"))
								.getAttribute("alt").equals(pageTitle));
					}
					assertTrue(
							!driver.findElement(By.xpath("//main[@role='main']/section[1]/div[1]/div[1]/picture/img"))
									.getAttribute("src").contains("inner-banner"));
				}
			}
		} else {
			if (menu.contains("Blog")) {
				commonFunc.scroll(driver.findElement(By.xpath("//h2[contains(text(),'Latest Blogs')]")));

				assertTrue(driver.findElements(By.xpath("//*[@id='blog_more_btn']")).size() > 0);
			} else if (menu.contains("Contact")) {
				commonFunc.scroll(driver.findElement(By.xpath("//h2[contains(text(),'Contact')]")));

				assertTrue(driver.findElement(By.xpath("//h2[contains(text(),'Contact')]")).getText().trim()
						.equalsIgnoreCase("contact"));
			} else if (menu.contains("Gallery")) {
				commonFunc.scroll(driver.findElement(By.xpath("//h2[contains(text(),'Work Gallery')]")));

				assertTrue(driver.findElement(By.xpath("//h2[contains(text(),'Work Gallery')]")).getText().trim()
						.equalsIgnoreCase("work gallery"));
			} else if (menu.contains("Services")) {
				commonFunc.scroll(driver.findElement(By.xpath("//h2[contains(text(),'Services')]")));

				assertTrue(driver.findElement(By.xpath("//h2[contains(text(),'Services')]")).getText().trim()
						.equalsIgnoreCase("services"));
			} else if (menu.contains("About")) {
				commonFunc.scroll(driver.findElement(By.xpath("//h2[contains(text(),'About')]")));

				assertTrue(driver.findElement(By.xpath("//h2[contains(text(),'About')]")).getText().trim()
						.equalsIgnoreCase("about"));
			}
		}
	}

	@And("{string} Banner image alt text")
	public void setBannerImageAltText(String type) {
		altTextSet = false;
		driver.findElement(By.xpath("//*[@id='banner_alt']")).clear();
		if (type.equalsIgnoreCase("set")) {
			altTextSet = true;
			driver.findElement(By.xpath("//*[@id='banner_alt']")).sendKeys("alt text 123");
		}
	}
}